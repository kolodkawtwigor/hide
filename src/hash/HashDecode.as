package hash
{
	import Connect.Jsons.JSONConnect;
	import Connect.Jsons.JSONMain;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;

	public class HashDecode
	{
		static private var __instance:HashDecode;
		private var __encode:String;
		private var __decode:String;
		private var __key:String = 'bB0LocQn2y4t6Td4ZQlB';
		private var __func:Function;
		
		private var __timer:Timer = new Timer(500);
		
		private var __obj:Object;
		
		private var __objCache:Object;
		
		public function HashDecode():void {
			__instance = this;
		}
		
		public static function getInstance():HashDecode {
			return __instance ? __instance : new HashDecode();
		}
		
		public function getDecode(crypted:String, func:Function, objCache:Object = null):void {
			__func = func;
			__objCache = objCache;
			
			__decode = Xor64.decode(crypted, __key);
			
//			__timer.addEventListener(TimerEvent.TIMER, onTimerDecode);
//			__timer.start();
			onTimerDecode(null);
		}
		
		private function onTimerDecode(e:TimerEvent):void {
//			__timer.stop();
//			__timer.removeEventListener(TimerEvent.TIMER, onTimerDecode);
			
//			__obj = JSONMain.decode(__decode);
			__obj = JSONConnect.decode(__decode);
			
//			__timer.addEventListener(TimerEvent.TIMER, onTimerRes);
//			__timer.start();
			onTimerRes(null);
		}
		
		private function onTimerRes(e:TimerEvent):void {
//			__timer.stop();
//			__timer.removeEventListener(TimerEvent.TIMER, onTimerRes);
			__func.call(null, __obj);
		}
	}
}