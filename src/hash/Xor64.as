﻿package hash {
	
	import flash.utils.ByteArray;
	
	public class Xor64 {
		
		public static function xor(str:String, key:String):String {
			
			var result:String = new String();
			
			for (var i:Number = 0; i < str.length; i++) {
				if (i > (key.length - 1)) {
					key += key;
				}
				result += String.fromCharCode(str.charCodeAt(i) ^ key.charCodeAt(i));
			}
			
			return result;
		}
		
		public static function encode(str:String, key:String):String {
			
			var source:ByteArray = new ByteArray();
			source.writeUTFBytes(xor(str, key));
			return Base64.encode(source);
		}
		
		public static function decode(str:String, key:String):String {
			
			return xor(Base64.decode(str).toString(), key);
		}
	}
}