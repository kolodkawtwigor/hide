package ServiceGame.ServiceControllers
{
	import ServiceGame.Service;
	
	import ServiceGame.ServiceModels.AppInfoModel;

	public class AppInfoController
	{
		static private var _init:AppInfoController;
		static public function get instance():AppInfoController
		{
			if(!_init) _init = new AppInfoController();
			return _init;
		}
		
		public function init(o:*):void
		{
			var appInfoModel:AppInfoModel 	= Service.instance.appInfoModel;
				appInfoModel.cacheUrl	    = o.cacheUrl;
				appInfoModel.serverTime 	= o.serverTime;
				appInfoModel.staticUrl 		= o.staticUrl;
		}	
	}
}