package ServiceGame.ServiceControllers
{
	import Connect.service;
	
	import ServiceGame.ServiceModels.BonusModel;
	
	public class BonusController
	{
		static private var _init:BonusController;
		static public function get instance():BonusController
		{
			if(!_init) _init = new BonusController();
			return _init; 
		}
		
		public function init(o:*):void
		{
			var bonusModel:BonusModel 		= service().bonusModel;
			bonusModel.isAvailable 			= o.isAvailable;
			bonusModel.timeLeft 			= o.timeLeft;
		}
	}
}