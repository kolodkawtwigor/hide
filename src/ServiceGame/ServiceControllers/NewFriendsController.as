package ServiceGame.ServiceControllers
{
	import ServiceGame.ServiceModels.FriendsModel;
	import ServiceGame.ServiceModels.NewFriendsModel;

	public class NewFriendsController
	{
		static private var _init:NewFriendsController
		private var __friendsModel:Vector.<FriendsModel> = Service.instance.friendsModel;
		
		static public function get instance():NewFriendsController
		{
			if(!_init) _init = new NewFriendsController();
			return _init; 
		}
	
		public function init(o:*):void
		{
			var newFriendsModel:NewFriendsModel = new NewFriendsModel();
			
			for (var j:int = 0; j < o.length; j++) 
			{
				for (var i:int = 0; i < __friendsModel.length; i++) 
				{
					if (o[j] == __friendsModel[i].playerUid) 
					{
						newFriendsModel.firstName 			= __friendsModel[i].nameFirst;
						newFriendsModel.lastName 			= __friendsModel[i].nameLast;
						newFriendsModel.picture 			= __friendsModel[i].image;
						
						newFriendsModel.playerUid 			= __friendsModel[i].playerUid;
						
						newFriendsModel.playerId 			= __friendsModel[i].playerId;
						Service.instance.newfriendsModel.push(newFriendsModel);
					}
				}		
			}
		}
	}
}