package ServiceGame.ServiceControllers
{
	import ServiceGame.Service;
	
	import ServiceGame.ServiceModels.GameDataItemsModel;
	
	public class GameDataItemsController
	{
		static private var _init:GameDataItemsController;
		static public function get instance():GameDataItemsController
		{
			if(!_init) _init = new GameDataItemsController();
			return _init;
		}
		
		public function init(o:*):void
		{
			var lnth:int = o.length;
			for (var i:int = 0; i < lnth; i++) 
			{
				var gameDataItemsModel:GameDataItemsModel 			= new GameDataItemsModel(); 
				gameDataItemsModel.id 								= o[i].id;
				gameDataItemsModel.name 							= o[i].name;
				gameDataItemsModel.typeId 							= o[i].typeId;
				
				Service.instance.gameDataItems.push(gameDataItemsModel);
			}
		}	
	}
}