package ServiceGame.ServiceControllers
{
	import Connect.service;
	
	import ServiceGame.ServiceModels.LotteryModel;

	public class LotteryController
	{
		static private var __init:LotteryController
		
		static public function get instance():LotteryController
		{
			if(!__init) __init = new LotteryController();
			return __init;
		}
		public function init(o:*):void
		{
			var lnth:int = o.length;
			
			for (var i:int = 0; i < lnth; i++) 
			{
				var loteryModel:LotteryModel					= new LotteryModel();
				
				loteryModel.count 								= o[i].count;
				loteryModel.itemId 								= o[i].itemId;
				loteryModel.percent 							= o[i].percent;
				service().lotteryContainerModel.push(loteryModel);	
			}
		}
	}
}