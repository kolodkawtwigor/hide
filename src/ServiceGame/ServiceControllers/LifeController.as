package ServiceGame.ServiceControllers
{
	import Connect.service;
	
	import ServiceGame.ServiceModels.LifeModel;
	

	public class LifeController
	{
		static private var __init:LifeController;
		
		static public function get instance():LifeController
		{
			if(!__init) __init = new LifeController();
			return __init; 
		}
		
		
		public function init(o:*):void
		{
			var lifeModel:LifeModel 		= service().lifeModel
			lifeModel.init_count 			= o.count;
			lifeModel.resp_count 			= o.countRespawn;
			lifeModel.max_count 			= o.max;
			lifeModel.timeLeft 				= o.timeLeft;
			lifeModel.resp_period 			= o.timePeriod;
		}
	}
}