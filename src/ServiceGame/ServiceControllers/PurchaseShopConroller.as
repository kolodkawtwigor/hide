
package ServiceGame.ServiceControllers
{
	import ServiceGame.Service;
	
	import ServiceGame.ServiceModels.PurchaseShopModel;

	public class PurchaseShopConroller
	{
		static private var _init:PurchaseShopConroller
		static public function get instance():PurchaseShopConroller
		{
			if(!_init) _init = new PurchaseShopConroller();
			return _init;
		}
		public function init(o:*):void
		{
			var lnth:int = o.length;
			for (var i:int = 0; i < lnth; i++) 
			{
				var purchaseShopModel:PurchaseShopModel 	=  new PurchaseShopModel(); 
				purchaseShopModel.count  						=  o[i].count;
				purchaseShopModel.name  						=  o[i].name;
				purchaseShopModel.id  							=  o[i].id;
				purchaseShopModel.priceHard  					=  o[i].priceHard;
				purchaseShopModel.priceSoft  					=  o[i].priceSoft;
				purchaseShopModel.itemId  						=  o[i].itemId;
				
				Service.instance.purchasesShopModel.push(purchaseShopModel);
			}
			
		}	
	}
}