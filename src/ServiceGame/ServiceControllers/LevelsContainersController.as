package ServiceGame.ServiceControllers
{
	import Connect.service;
	
	import ServiceGame.ServiceModels.LevelModel;
	import ServiceGame.ServiceModels.Levels;
	import ServiceGame.ServiceModels.LevelsContainersModel;

	public class LevelsContainersController
	{
		static private var _init:LevelsContainersController
		static public function get instance():LevelsContainersController
		{
			if(!_init) _init = new LevelsContainersController();
			return _init;
		}
		
		public function init(o:*):void
		{
			var lnth:int = o.length;
			var len:int  = service().levelsModelContainer.length
			var lvlContainer:Vector.<LevelModel> = service().levelsModelContainer
			var lvlComplete:Array = service().playerModel.containers
				
			for (var i:int = 0; i < lnth; i++)
			{
				var levelModel:LevelsContainersModel		= new LevelsContainersModel();
				levelModel.id 								= o[i].id;
				levelModel.name 							= o[i].name;
				levelModel.image 							= o[i].image;
				
				levelModel.levels 							= new Vector.<LevelModel>();
				var lnth2:int = o[i].levels.length;
				
				
				for (var j:int = 0; j < lnth2; j++) 
				{
					
//					var offCheck:Boolean = true;
//					if(lvlComplete.length == 0)
//					{
						createNoneLvl(o[i].levels[j]);
//						offCheck = false;
//					}
//					
//					for (var k:int = 0; k < lvlComplete.length &&  offCheck; k++) 
//					{
//						if ( o[i].levels[j] == lvlComplete[k].id)
//						{
//							levelModel.levels.push(lvlComplete[k])
//							offCheck = false 
//						}
//						
//						if(offCheck && k ==  lvlComplete.length - 1)
//						{
//							createNoneLvl(o[i].levels[j]);
//							offCheck = false;
//						}
//					}
					
					function createNoneLvl(param:String):void
					{
						for (var i2:int = 0; i2 < len; i2++) 
						{
							if(param == lvlContainer[i2].id)
							{
								levelModel.levels.push(lvlContainer[i2]);
								return;
							}
						}
					}
				}
				
				service().levelsContainerModelContainer.push(levelModel);	
			}			
		}
		
		
	}
}