package ServiceGame.ServiceControllers
{
	import Connect.service;
	import ServiceGame.ServiceModels.BonusLevelModel;

	public class BonusLevelsController
	{
			static private var _init:BonusLevelsController
			static public function get instance():BonusLevelsController
			{
				if(!_init) _init = new BonusLevelsController();
				return _init;
			}
			
		public function init(o:*):void
		{
			var lnth:int = o.length;
			for (var i:int = 0; i < lnth; i++) 
			{
				var bonuslevelModel:BonusLevelModel				= new BonusLevelModel();
				bonuslevelModel.id 								= o[i].id;
				bonuslevelModel.name 							= o[i].name;
				bonuslevelModel.position 						= o[i].position;
				
				bonuslevelModel.words 							= new Vector.<String>();
				
				var lnth2:int = o[i].words.length;
				
				for (var j:int = 0; j < lnth2; j++) 
				{
					
					bonuslevelModel.words.push(o[i].words[j]);
				}
				
				service().bonuslevelModelContainer.push(bonuslevelModel);	
			}			
		}
	}
}