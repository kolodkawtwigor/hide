package ServiceGame.ServiceControllers
{
	import Connect.service;
	
	import ServiceGame.ServiceModels.FriendsModel;

	public class FriendsController
	{
		static private var _init:FriendsController
		
		static public function get instance():FriendsController
		{
			if(!_init) _init = new FriendsController();
			return _init; 
		}
		
		public function init(o:*):void
		{
			var lnth:int = o.length;
			for (var i:int = 0; i < lnth; i++) 
			{
					var friendsModel:FriendsModel 		= new FriendsModel;
				
					friendsModel.playerId 			= o[i].id;
					friendsModel.image				= o[i].image;
					friendsModel.isNew				= o[i].isNew;
					friendsModel.levels				= o[i].levels;
					friendsModel.nameFirst 			= o[i].nameFirst;
					friendsModel.nameLast 			= o[i].nameLast;
					friendsModel.stars				= o[i].points;
					friendsModel.platformId			= o[i].platformId;
					friendsModel.platformType		= o[i].platformType;
						
					service().friendsModel.push(friendsModel);
			}
		}
	}
}