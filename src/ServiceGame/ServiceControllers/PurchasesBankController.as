package ServiceGame.ServiceControllers
{
	
	import Connect.service;
	
	import ServiceGame.ServiceModels.PurchasesBankModel;

	public class PurchasesBankController
	{
		static private var _init:PurchasesBankController
		static public function get instance():PurchasesBankController
		{
			if(!_init) _init = new PurchasesBankController();
			return _init;
		}
		public function init(o:*):void
		{
			var lnth:int = o.length;
			for (var i:int = 0; i < lnth; i++) 
			{
				var purchaseBankModel:PurchasesBankModel 			= new PurchasesBankModel(); 
				purchaseBankModel.bonus 							=  o[i].bonus;
				purchaseBankModel.count  						    =  o[i].count;
				purchaseBankModel.id  								=  o[i].id;
				purchaseBankModel.priceHard  						=  o[i].priceHard;
				purchaseBankModel.total 							=  o[i].total;
				purchaseBankModel.typeId  							=  o[i].typeId ;
				
				service().purchaseBankModel.push(purchaseBankModel);
			}
			
		}	
	}
}