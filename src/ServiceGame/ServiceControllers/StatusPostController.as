package ServiceGame.ServiceControllers
{
	import ServiceGame.Service;
	
	import ServiceGame.ServiceModels.StatusPostModel;
	
	public class StatusPostController
	{
		static private var _init:StatusPostController
		static public function get instance():StatusPostController
		{
			if(!_init) _init = new StatusPostController();
			return _init; 
		}
		
		public function init(o:*):void
		{
			var statusPostModel:StatusPostModel = Service.instance.statusPostModel;
			statusPostModel.needStatusPost 		= o.status.isAvailable;
		}
	}
}