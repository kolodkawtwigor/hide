package ServiceGame.ServiceControllers
{
	import Connect.service;
	
	import ServiceGame.Service;
	import ServiceGame.ServiceModels.WordsModel;

	public class WordsController
	{
		
		static private var __init:WordsController
		
		static public function get instance():WordsController
		{
			if(!__init) __init = new WordsController();
			return __init;
		}
		public function init(o:*):void
		{
			var lnth:int = o.length;
			
			for (var i:int = 0; i < lnth; i++) 
			{
				var wordsModel:WordsModel		= new WordsModel();
				
				wordsModel.id 								= o[i].id;
				wordsModel.image 							= o[i].image;
				wordsModel.name 							= o[i].word;
				service().wordsContainerModel.push(wordsModel);	
			}
		}
	}
}

