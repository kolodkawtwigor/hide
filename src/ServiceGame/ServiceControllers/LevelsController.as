package ServiceGame.ServiceControllers
{
	import Connect.service;
	
	import ServiceGame.ServiceModels.LevelModel;

	public class LevelsController
	{
		static private var _init:LevelsController
		static public function get instance():LevelsController
		{
			if(!_init) _init = new LevelsController();
			return _init;
		}
		
		public function init(o:*):void
		{
			var lnth:int = o.length;
			for (var i:int = 0; i < lnth; i++)
			{
				var levelModel:LevelModel					= new LevelModel();
				
				levelModel.id 								= o[i].id;
				levelModel.name 							= o[i].name;
				levelModel.position 						= o[i].position;
				levelModel.typeId 							= o[i].typeId;
				
				levelModel.words = new Vector.<String>();
				for (var j:int = 0; j < o[i].words.length; j++) 
				{
					levelModel.words.push(o[i].words[j]);
				}
				
				service().levelsModelContainer.push(levelModel);	
			}			
		}
	}
}