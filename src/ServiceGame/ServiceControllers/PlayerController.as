package ServiceGame.ServiceControllers
{
	import ServiceGame.Service;
	import ServiceGame.ServiceModels.PlayerModel;

	public class PlayerController
	{
		static private var __init:PlayerController;
		static public function get instance():PlayerController
		{
			if(!__init) __init = new PlayerController();
			return __init;
		}
		
		public function init(o:*):void
		{
			var playerModel:PlayerModel 			= Service.instance.playerModel;
			
				playerModel.currencySoft 			= o.currencySoft;
				playerModel.currentLevelId 			= o.currentLevelId;
				playerModel.id			    		= o.id;
				playerModel.image 					= o.image;
				playerModel.nameFirst 				= o.nameFirst;
				playerModel.nameLast 				= o.nameLast;
				playerModel.session 				= o.session;
				playerModel.points 					= o.points;
				playerModel.lifeCount 				= o.energy.count;
				playerModel.lifeMax 				= o.energy.max;
				playerModel.lifeTime 				= o.energy.time;
		}
	}
}