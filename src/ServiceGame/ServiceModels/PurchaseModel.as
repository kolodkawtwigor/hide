package ServiceGame.ServiceModels
{
	import Events.ServiceEvent;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;

	public class PurchaseModel extends EventDispatcher
	{
		private var __count:String;
		private var __id:String;
		private var __priceHard:String;
		
		public function PurchaseModel(target:IEventDispatcher = null)
		{
			super(target);
		}
		
		public function get priceHard():String
		{
			return __priceHard;
		}

		public function set priceHard(value:String):void
		{
			__priceHard = value;
		}

		public function get id():String
		{
			return __id;
		}

		public function set id(value:String):void
		{
			__id = value;
		}

		public function get count():String
		{
			return __count;
		}

		public function set count(value:String):void
		{
			__count = value;
		}

	}
}