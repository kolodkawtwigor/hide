package ServiceGame.ServiceModels
{
	import flash.events.EventDispatcher;

	public class GiftsReceived extends EventDispatcher
	{
		private var __count:int;
		private var __playerId:String;
		private var __time:int;
		
		public function GiftsReceived()
		{
		}

		public function get playerId():String
		{
			return __playerId;
		}

		public function set playerId(value:String):void
		{
			__playerId = value;
		}

		public function get time():int
		{
			return __time;
		}

		public function set time(value:int):void
		{
			__time = value;
		}

		public function get count():int
		{
			return __count;
		}

		public function set count(value:int):void
		{
			__count = value;
		}

	}
}