package ServiceGame.ServiceModels
{
	import Events.ServiceEvent;
	
	import starling.events.EventDispatcher;
	
	public class AppInfoModel extends EventDispatcher
	{
		private var __serverTime:Number;
		private var __cacheUrl:String;
		private var __staticUrl:String;
		
		public function AppInfoModel():void
		{
		
		}

		public function get staticUrl():String
		{
			return __staticUrl;
		}

		public function set staticUrl(value:String):void
		{
			__staticUrl = value;
		}

		public function get cacheUrl():String
		{
			return __cacheUrl;
		}

		public function set cacheUrl(value:String):void
		{
			__cacheUrl = value;
		}

		public function get serverTime():Number
		{
			return __serverTime;
		}

		public function set serverTime(value:Number):void
		{
			__serverTime = value;
		}
	}
}