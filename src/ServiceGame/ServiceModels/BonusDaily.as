package ServiceGame.ServiceModels
{
	import starling.events.EventDispatcher;
	
	public class BonusDaily extends EventDispatcher
	{
		private var __day:int;
		private var __isCurrent:Boolean;
		private var __currencySoft:int;
		
		public function BonusDaily()
		{
			
		}

		public function get currencySoft():int
		{
			return __currencySoft;
		}

		public function set currencySoft(value:int):void
		{
			__currencySoft = value;
		}

		public function get isCurrent():Boolean
		{
			return __isCurrent;
		}

		public function set isCurrent(value:Boolean):void
		{
			__isCurrent = value;
		}

		public function get day():int
		{
			return __day;
		}

		public function set day(value:int):void
		{
			__day = value;
		}

	}
}