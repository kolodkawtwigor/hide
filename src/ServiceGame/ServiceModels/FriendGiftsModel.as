package ServiceGame.ServiceModels
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import Events.ServiceEvent;

	public class FriendGiftsModel extends EventDispatcher
	{
		private var __gift_content_count:int;
		private var __received_gifts:Object;
		private var __total_content_count:int;
		private var __players:Array;
		
		public function FriendGiftsModel():void
		{
			
		}

		public function get gift_content_count():int
		{
			return __gift_content_count;
		}

		public function set gift_content_count(value:int):void
		{
			__gift_content_count = value;
		}

		public function get received_gifts():Object
		{
			return __received_gifts;
		}

		public function set received_gifts(value:Object):void
		{
			__received_gifts = value;
		}

		public function get total_content_count():int
		{
			return __total_content_count;
		}

		public function set total_content_count(value:int):void
		{
			__total_content_count = value;
		}

		public function get players():Array
		{
			return __players;
		}

		public function set players(value:Array):void
		{
			__players = value;
		}


	}
}