package ServiceGame.ServiceModels
{LevelsContainersModel
	import starling.events.EventDispatcher;
	
	public class LevelsContainersModel extends EventDispatcher
	{
		private var __id:String;
		private var __name:String;
		private var __levels:Vector.<LevelModel>;
		private var __image:String;
		
		public function LevelsContainersModel()
		{
			super();
		}
		
		

		public function get image():String
		{
			return __image;
		}

		public function set image(value:String):void
		{
			__image = value;
		}

		public function get levels():Vector.<LevelModel>
		{
			return __levels;
		}

		public function set levels(value:Vector.<LevelModel>):void
		{
			__levels = value;
		}

		public function get name():String
		{
			return __name;
		}
		
		public function set name(value:String):void
		{
			__name = value;
		}
		
		public function get id():String
		{
			return __id;
		}
		
		public function set id(value:String):void
		{
			__id = value;
		}
	}
}