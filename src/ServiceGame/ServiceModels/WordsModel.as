package ServiceGame.ServiceModels
{
	import starling.events.EventDispatcher;
	
	public class WordsModel extends EventDispatcher
	{
		private var __id:String;
		private var __image:String;
		private var __name:String;
		public function WordsModel()
		{
			
		}

		public function get name():String
		{
			return __name;
		}

		public function set name(value:String):void
		{
			__name = value;
		}

		public function get image():String
		{
			return __image;
		}

		public function set image(value:String):void
		{
			__image = value;
		}

		public function get id():String
		{
			return __id;
		}

		public function set id(value:String):void
		{
			__id = value;
		}

	}
}