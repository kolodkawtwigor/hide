package ServiceGame.ServiceModels
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	public class GameDataItemsModel extends EventDispatcher
	{
		private var __description:String;
		private var __id:String;
		private var __image:String;
		private var __name:String;
		private var __typeId:String;
		
		public function GameDataItemsModel()
		{
			
		}

		public function get typeId():String
		{
			return __typeId;
		}

		public function set typeId(value:String):void
		{
			__typeId = value;
		}

		public function get name():String
		{
			return __name;
		}

		public function set name(value:String):void
		{
			__name = value;
		}

		public function get image():String
		{
			return __image;
		}

		public function set image(value:String):void
		{
			__image = value;
		}

		public function get id():String
		{
			return __id;
		}

		public function set id(value:String):void
		{
			__id = value;
		}

		public function get description():String
		{
			return __description;
		}

		public function set description(value:String):void
		{
			__description = value;
		}

	}
}