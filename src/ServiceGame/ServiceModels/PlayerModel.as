package ServiceGame.ServiceModels
{
	import Events.ServiceEvent;
	
	import starling.events.Event;
	import starling.events.EventDispatcher;
	
	

	
	public class PlayerModel extends EventDispatcher
	{
		private var __playerBonusLevels:Vector.<PlayerBonusLevels>;
		private var __currencySoft:int;
		private var __id:String;
		private var __image:String;
		private var __levels:Vector.<Levels>;
		private var __nameFirst:String;
		private var __nameLast:String;
		private var __session:String;
		private var __points:int;
		private var __lifeCount:int;
		private var __lifeMax:int;
		private var __lifeTime:int;
		private var __containers:Array;
		private var __currentCotainer:String;
		private var __currentLevelId:String;
		
		
		public function PlayerModel()
		{
			
		}

		public function get currentLevelId():String
		{
			return __currentLevelId;
		}

		public function set currentLevelId(value:String):void
		{
			__currentLevelId = value;
		}

		public function get containers():Array
		{
			return __containers;
		}

		public function set containers(value:Array):void
		{
			__containers = value;
			dispatchEvent( new Event(ServiceEvent.LEVEL_COUNT));
		}

		public function get currentCotainer():String
		{
			return __currentCotainer;
		}

		public function set currentCotainer(value:String):void
		{
			__currentCotainer = value;
		}

		public function get lifeTime():int
		{
			return __lifeTime;
		}

		public function set lifeTime(value:int):void
		{
			__lifeTime = value;
		}

		public function get lifeMax():int
		{
			return __lifeMax;
		}

		public function set lifeMax(value:int):void
		{
			__lifeMax = value;
		}

		public function get lifeCount():int
		{
			return __lifeCount;
		}

		public function set lifeCount(value:int):void
		{
			__lifeCount = value;
			dispatchEvent( new Event(ServiceEvent.LIFE_COUNT));
		}

		public function get points():int
		{
			return __points;
		}

		public function set points(value:int):void
		{
			__points = value;
			
		}

		public function get session():String
		{
			return __session;
		}

		public function set session(value:String):void
		{
			__session = value;
		}

		public function get nameLast():String
		{
			return __nameLast;
		}

		public function set nameLast(value:String):void
		{
			__nameLast = value;
		}

		public function get nameFirst():String
		{
			return __nameFirst;
		}

		public function set nameFirst(value:String):void
		{
			__nameFirst = value;
		}

		public function get levels():Vector.<Levels>
		{
			return __levels;
		}

		public function set levels(value:Vector.<Levels>):void
		{
			__levels = value;
		
		}

		public function get image():String
		{
			return __image;
		}

		public function set image(value:String):void
		{
			__image = value;
		}

		public function get id():String
		{
			return __id;
		}

		public function set id(value:String):void
		{
			__id = value;
		}

		public function get currencySoft():int
		{
			return __currencySoft;
		}

		public function set currencySoft(value:int):void
		{
			__currencySoft = value;
			dispatchEvent( new Event(ServiceEvent.CURRENCY_SOFT));
		}

		public function get playerBonusLevels():Vector.<PlayerBonusLevels>
		{
			return __playerBonusLevels;
		}

		public function set playerBonusLevels(value:Vector.<PlayerBonusLevels>):void
		{
			__playerBonusLevels = value;
		}

	}
}