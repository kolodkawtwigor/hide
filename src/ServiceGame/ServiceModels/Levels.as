package ServiceGame.ServiceModels
{
	import starling.events.Event;
	import starling.events.EventDispatcher;
	
	
	public class Levels extends LevelModel
	{
		private var __id:String;
		private var __completed:Boolean;
		private var __progress:Array;
		
		public function Levels()
		{
			
		}

		public function get progress():Array
		{
			return __progress;
		}

		public function set progress(value:Array):void
		{
			__progress = value;
		}

		public function get completed():Boolean
		{
			return __completed;
		}

		public function set completed(value:Boolean):void
		{
			__completed = value;
		}

		override public function get id():String
		{
			return __id;
		}

		override public function set id(value:String):void
		{
			__id = value;
		}

	}
}