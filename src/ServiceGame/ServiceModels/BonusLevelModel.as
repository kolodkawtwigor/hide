package ServiceGame.ServiceModels
{
	import starling.events.EventDispatcher;
	
	public class BonusLevelModel extends EventDispatcher
	{
		private var __id:String;
		private var __name:String;
		private var __position:int;
		private var __words:Vector.<String>;
		public function BonusLevelModel()
		{
			super();
		}

		public function get words():Vector.<String>
		{
			return __words;
		}

		public function set words(value:Vector.<String>):void
		{
			__words = value;
		}

		public function get position():int
		{
			return __position;
		}

		public function set position(value:int):void
		{
			__position = value;
		}

		public function get name():String
		{
			return __name;
		}

		public function set name(value:String):void
		{
			__name = value;
		}

		public function get id():String
		{
			return __id;
		}

		public function set id(value:String):void
		{
			__id = value;
		}

	}
}