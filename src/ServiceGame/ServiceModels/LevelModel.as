package ServiceGame.ServiceModels
{
	import flash.events.EventDispatcher;

	public class LevelModel extends EventDispatcher
	{
		private var __id:String;
		private var __name:uint;
		private var __position:uint;
		private var __typeId:uint;
		private var __words:Vector.<String>;
		
		public function LevelModel()
		{
			
		}

		public function get words():Vector.<String>
		{
			return __words;
		}

		public function set words(value:Vector.<String>):void
		{
			__words = value;
		}

		public function get typeId():uint
		{
			return __typeId;
		}

		public function set typeId(value:uint):void
		{
			__typeId = value;
		}

		public function get position():uint
		{
			return __position;
		}

		public function set position(value:uint):void
		{
			__position = value;
		}

		public function get name():uint
		{
			return __name;
		}

		public function set name(value:uint):void
		{
			__name = value;
		}

		public function get id():String
		{
			return __id;
		}

		public function set id(value:String):void
		{
			__id = value;
		}

	}
}