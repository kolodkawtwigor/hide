package ServiceGame.ServiceModels
{
	import flash.events.EventDispatcher;

	public class FriendsModel extends EventDispatcher
	{
		private var __playerId:String;
		private var __levels:int;
		private var __playerUid:String;
		private var __nameFirst:String;
		private var __nameLast:String;
		private var __image:String;
		private var __position:String;
		private var __stars:String;
		private var __platformId:String;
		private var __platformType:String;
		private var __place:int;
		
		private var __canGift:Boolean;
		private var __isNew:Boolean;
		
		public function FriendsModel():void
		{
		
		}

		public function get levels():int
		{
			return __levels;
		}

		public function set levels(value:int):void
		{
			__levels = value;
		}

		public function get position():String
		{
			return __position;
		}

		public function set position(value:String):void
		{
			__position = value;
		}

		public function get canGift():Boolean
		{
			return __canGift;
		}

		public function set canGift(value:Boolean):void
		{
			__canGift = value;
		}

		public function get playerUid():String
		{
			return __playerUid;
		}

		public function set playerUid(value:String):void
		{
			__playerUid = value;
		}

		public function get stars():String
		{
			return __stars;
		}

		public function set stars(value:String):void
		{
			__stars = value;
		}

		public function get platformType():String
		{
			return __platformType;
		}

		public function set platformType(value:String):void
		{
			__platformType = value;
		}

		public function get platformId():String
		{
			return __platformId;
		}

		public function set platformId(value:String):void
		{
			__platformId = value;
		}

		public function get isNew():Boolean
		{
			return __isNew;
		}

		public function set isNew(value:Boolean):void
		{
			__isNew = value;
		}

		public function get image():String
		{
			return __image;
		}

		public function set image(value:String):void
		{
			__image = value;
		}

		public function get nameLast():String
		{
			return __nameLast;
		}

		public function set nameLast(value:String):void
		{
			__nameLast = value;
		}

		public function get nameFirst():String
		{
			return __nameFirst;
		}

		public function set nameFirst(value:String):void
		{
			__nameFirst = value;
		}

		public function get playerId():String
		{
			return __playerId;
		}

		public function set playerId(value:String):void
		{
			__playerId = value;
		}

	}
	
}
