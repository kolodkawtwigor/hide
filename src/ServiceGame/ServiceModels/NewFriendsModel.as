package ServiceGame.ServiceModels
{
	import Events.ServiceEvent;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;

	public class NewFriendsModel extends EventDispatcher
	{
		private var __firstName:String;
		private var __lastName:String;
		private var __picture:String;
		private var __playerUid:String;
		private var __playerId:String;
		
		public function NewFriendsModel():void
		{		
		}

		public function get playerId():String
		{
			return __playerId;
		}

		public function set playerId(value:String):void
		{
			__playerId = value;
		}

		public function get playerUid():String
		{
			return __playerUid;
		}

		public function set playerUid(value:String):void
		{
			__playerUid = value;
		}

		public function get picture():String
		{
			return __picture;
		}

		public function set picture(value:String):void
		{
			__picture = value;
		}

		public function get lastName():String
		{
			return __lastName;
		}

		public function set lastName(value:String):void
		{
			__lastName = value;
		}

		public function get firstName():String
		{
			return __firstName;
		}

		public function set firstName(value:String):void
		{
			__firstName = value;
		}

	}
}