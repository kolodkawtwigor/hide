package ServiceGame.ServiceModels
{
	import flash.events.EventDispatcher;
	
	public class FriendsInSocialModel extends EventDispatcher
	{
		private var __uid:String;
		private var __user_id:String;
		private var __avatar:String;
		private var __lastName:String;
		private var __firstName:String;
		
		public function FriendsInSocialModel()
		{
			
		}
		
		public function get firstName():String
		{
			return __firstName;
		}
		
		public function set firstName(value:String):void
		{
			__firstName = value;
		}
		
		public function get lastName():String
		{
			return __lastName;
		}
		
		public function set lastName(value:String):void
		{
			__lastName = value;
		}
		
		public function get avatar():String
		{
			return __avatar;
		}
		
		public function set avatar(value:String):void
		{
			__avatar = value;
		}
		
		public function get user_id():String
		{
			return __user_id;
		}
		
		public function set user_id(value:String):void
		{
			__user_id = value;
		}
		
	}
}