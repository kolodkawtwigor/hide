package ServiceGame.ServiceModels
{
	import flash.events.EventDispatcher;
	
	public class StatusPostModel extends EventDispatcher
	{
		
		private var __need__status__post:Boolean;
		private var __reward__content__count:int;
		
		public function StatusPostModel()
		{
			
		}
		
		public function get needStatusPost():Boolean
		{
			return __need__status__post;
		}
		
		public function set needStatusPost(value:Boolean):void
		{
			__need__status__post = value;
		}
	}
}