package ServiceGame.ServiceModels
{
	
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	public class PurchaseShopModel extends EventDispatcher
	{
		private var __itemId:String;
		private var __count:String;
		private var __id:String;
		private var __priceHard:String;
		private var __priceSoft:String;
		private var __name:String;
		
		public function PurchaseShopModel(target:IEventDispatcher=null)
		{
			super(target);
		}
		
		public function get name():String
		{
			return __name;
		}

		public function set name(value:String):void
		{
			__name = value;
		}

		public function get priceSoft():String
		{
			return __priceSoft;
		}

		public function set priceSoft(value:String):void
		{
			__priceSoft = value;
		}

		public function get priceHard():String
		{
			return __priceHard;
		}
		
		public function set priceHard(value:String):void
		{
			__priceHard = value;
		}
		
		public function get id():String
		{
			return __id;
		}
		
		public function set id(value:String):void
		{
			__id = value;
		}
		
		public function get count():String
		{
			return __count;
		}
		
		public function set count(value:String):void
		{
			__count = value;
		}
		
		public function get itemId():String
		{
			return __itemId;
		}

		public function set itemId(value:String):void
		{
			__itemId = value;
		}

	}
}