package ServiceGame.ServiceModels
{
	import starling.events.EventDispatcher;
	
	public class PlayerBonusLevels extends EventDispatcher
	{
		private var __id:String;
		private var __completed:Boolean;
		private var __endTime:int;
		private var __progress:Array;
		
		public function PlayerBonusLevels()
		{
			super();
		}

		

		public function get completed():Boolean
		{
			return __completed;
		}

		public function set completed(value:Boolean):void
		{
			__completed = value;
		}

		public function get progress():Array
		{
			return __progress;
		}

		public function set progress(value:Array):void
		{
			__progress = value;
		}

		public function get endTime():int
		{
			return __endTime;
		}

		public function set endTime(value:int):void
		{
			__endTime = value;
		}

	

		public function get id():String
		{
			return __id;
		}

		public function set id(value:String):void
		{
			__id = value;
		}

	}
}