package ServiceGame.ServiceModels
{
	public class LotteryModel
	{
		private var __count:uint;
		private var __itemId:String;
		private var __percent:uint;
		public function LotteryModel()
		{
		}

		public function get percent():uint
		{
			return __percent;
		}

		public function set percent(value:uint):void
		{
			__percent = value;
		}

		public function get itemId():String
		{
			return __itemId;
		}

		public function set itemId(value:String):void
		{
			__itemId = value;
		}

		public function get count():uint
		{
			return __count;
		}

		public function set count(value:uint):void
		{
			__count = value;
		}

	}
}