package ServiceGame.ServiceModels
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	public class PurchasesBankModel extends EventDispatcher
	{
		private var __bonus:String;
		private var __total:String;
		private var __typeId:String;
		private var __count:String;
		private var __id:String;
		private var __priceHard:String;
		
		public function PurchasesBankModel()
		{
			
		}
		
		public function get priceHard():String
		{
			return __priceHard;
		}
		
		public function set priceHard(value:String):void
		{
			__priceHard = value;
		}
		
		public function get id():String
		{
			return __id;
		}
		
		public function set id(value:String):void
		{
			__id = value;
		}
		
		public function get count():String
		{
			return __count;
		}
		
		public function set count(value:String):void
		{
			__count = value;
		}
		public function get typeId():String
		{
			return __typeId;
		}

		public function set typeId(value:String):void
		{
			__typeId = value;
		}

		public function get total():String
		{
			return __total;
		}

		public function set total(value:String):void
		{
			__total = value;
		}

		public function get bonus():String
		{
			return __bonus;
		}

		public function set bonus(value:String):void
		{
			__bonus = value;
		}

	}
}