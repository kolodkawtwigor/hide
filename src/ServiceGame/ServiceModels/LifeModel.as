package ServiceGame.ServiceModels
{
	import flash.events.EventDispatcher;

	public class LifeModel extends EventDispatcher
	{
		private var __init_count:int;
		private var __resp_period:int;
		private var __max_count:int;
		private var __timeLeft:int;
		private var __resp_count:int;
	
		public function LifeModel()
		{
			
		}

		public function get timeLeft():int
		{
			return __timeLeft;
		}

		public function set timeLeft(value:int):void
		{
			__timeLeft = value;
		}

		public function get init_count():int
		{
			return __init_count;
		}

		public function set init_count(value:int):void
		{
			__init_count = value;
		}

		public function get max_count():int
		{
			return __max_count;
		}

		public function set max_count(value:int):void
		{
			__max_count = value;
		}

		public function get resp_count():int
		{
			return __resp_count;
		}

		public function set resp_count(value:int):void
		{
			__resp_count = value;
		}

		public function get resp_period():int
		{
			return __resp_period;
		}

		public function set resp_period(value:int):void
		{
			__resp_period = value;
		}


	}
}