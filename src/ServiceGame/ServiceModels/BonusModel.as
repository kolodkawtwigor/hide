package ServiceGame.ServiceModels
{
	import flash.events.EventDispatcher;
	
	public class BonusModel extends EventDispatcher
	{
		private var __isAvailable:Boolean;
		private var __timeLeft:Number;
		
		public function BonusModel():void
		{
			
		}

		
		public function get timeLeft():Number
		{
			return __timeLeft;
		}

		public function set timeLeft(value:Number):void
		{
			__timeLeft = value;
		}

		public function get isAvailable():Boolean
		{
			return __isAvailable;
		}

		public function set isAvailable(value:Boolean):void
		{
			__isAvailable = value;
		}

	}
}