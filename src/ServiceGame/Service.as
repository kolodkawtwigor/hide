package ServiceGame
{
	import ServiceGame.ServiceModels.AppInfoModel;
	import ServiceGame.ServiceModels.BonusLevelModel;
	import ServiceGame.ServiceModels.BonusModel;
	import ServiceGame.ServiceModels.FriendsInSocialModel;
	import ServiceGame.ServiceModels.FriendsModel;
	import ServiceGame.ServiceModels.GameDataItemsModel;
	import ServiceGame.ServiceModels.LevelModel;
	import ServiceGame.ServiceModels.LevelsContainersModel;
	import ServiceGame.ServiceModels.LifeModel;
	import ServiceGame.ServiceModels.LotteryModel;
	import ServiceGame.ServiceModels.PlayerModel;
	import ServiceGame.ServiceModels.PurchaseShopModel;
	import ServiceGame.ServiceModels.PurchasesBankModel;
	import ServiceGame.ServiceModels.StatusPostModel;
	import ServiceGame.ServiceModels.WordsModel;
	
	public class Service
	{
		//
		static private var _init:Service;


		static public function get instance():Service
		{
			if(!_init) _init = new Service();
			return _init; 
		}
		
		private var __appInfoModel			:AppInfoModel;
		private var __friendsModel			:Vector.<FriendsModel>;
		private var __friendInSocialModel   :Vector.<FriendsInSocialModel>;
		private var __gameDataItemsModel	:Vector.<GameDataItemsModel>;
		private var __levelModel			:Vector.<LevelModel>;
		private var __purchasesBankModel	:Vector.<PurchasesBankModel>;
		private var __purchasesShopModel	:Vector.<PurchaseShopModel>;
		private var __wordsContainerModel	:Vector.<WordsModel>;
		private var __lotteryContainerModel	:Vector.<LotteryModel>;
		
		private var __lifeModel				:LifeModel;
		
		private var __bonusModel			:BonusModel;
		private var __playerModel			:PlayerModel;
		private var __statusPostModel		:StatusPostModel;
		private var __bonuslevelModelContainer :Vector.<BonusLevelModel>;
		private var __levelsContainerModelContainer :Vector.<LevelsContainersModel>;
		
		
		
		
		public function get lotteryContainerModel():Vector.<LotteryModel>
		{
			if(!__lotteryContainerModel) __lotteryContainerModel = new Vector.<LotteryModel>();
			return __lotteryContainerModel;
		}
		
		public function set lotteryContainerModel(value:Vector.<LotteryModel>):void
		{
			__lotteryContainerModel = value;
		}
		
		public function get wordsContainerModel():Vector.<WordsModel>
		{
			if(!__wordsContainerModel) __wordsContainerModel = new Vector.<WordsModel>();
			return __wordsContainerModel;
		}
		
		public function set wordsContainerModel(value:Vector.<WordsModel>):void
		{
			__wordsContainerModel = value;
		}
		
		public function get friendInSocialModel():Vector.<FriendsInSocialModel>
		{
			if(!__friendInSocialModel) __friendInSocialModel = new Vector.<FriendsInSocialModel>();
			return __friendInSocialModel;
		}
		
		public function set friendInSocialModel(value:Vector.<FriendsInSocialModel>):void
		{
			__friendInSocialModel = value;
		}
		
		public function get levelsContainerModelContainer():Vector.<LevelsContainersModel>
		{
			if(!__levelsContainerModelContainer) __levelsContainerModelContainer = new Vector.<LevelsContainersModel>();
			return __levelsContainerModelContainer;
		}
		
		public function set levelsContainerModelContainer(value:Vector.<LevelsContainersModel>):void
		{
			__levelsContainerModelContainer = value;
		}
		
		
		
		public function get bonuslevelModelContainer():Vector.<BonusLevelModel>
		{
			if(!__bonuslevelModelContainer) __bonuslevelModelContainer = new Vector.<BonusLevelModel>();
			return __bonuslevelModelContainer;
		}
		
		public function set bonuslevelModelContainer(value:Vector.<BonusLevelModel>):void
		{
			__bonuslevelModelContainer = value;
		}
		
		public function get purchasesShopModel():Vector.<PurchaseShopModel>
		{
			if(!__purchasesShopModel) __purchasesShopModel = new Vector.<PurchaseShopModel>();
			return __purchasesShopModel;
		}
		
		public function set bonusModel(value:BonusModel):void
		{
			__bonusModel = value;
		}
		
		public function get bonusModel():BonusModel
		{
			if(!__bonusModel) __bonusModel = new BonusModel();
			return __bonusModel;
		}
		
		public function set purchasesShopModel(value:Vector.<PurchaseShopModel>):void
		{
			__purchasesShopModel = value;
		}
		
		public function get statusPostModel():StatusPostModel
		{
			if(!__statusPostModel) __statusPostModel = new StatusPostModel();
			return __statusPostModel;
		}
		
		public function set statusPostModel(value:StatusPostModel):void
		{
			__statusPostModel = value;
		}
		
		public function get playerModel():PlayerModel
		{
			if(!__playerModel) __playerModel = new PlayerModel();
			return __playerModel;
		}
		public function set playerModel(value:PlayerModel):void
		{
			__playerModel = value;
		}
		
		public function get gameDataItems():Vector.<GameDataItemsModel>
		{
			if(!__gameDataItemsModel) __gameDataItemsModel = new Vector.<GameDataItemsModel>();
			return __gameDataItemsModel;
		}
		
		public function set gameDataItems(value:Vector.<GameDataItemsModel>):void
		{
			__gameDataItemsModel = value;
		}
		
		public function get friendsModel():Vector.<FriendsModel> 
		{
			if(!__friendsModel) __friendsModel = new Vector.<FriendsModel>();
			return __friendsModel;
		}
		public function set friendsModel(value:Vector.<FriendsModel>):void
		{
			__friendsModel = value;
		}
		
		
		public function get purchaseBankModel():Vector.<PurchasesBankModel>
		{
			if(!__purchasesBankModel) __purchasesBankModel = new Vector.<PurchasesBankModel>();
			return __purchasesBankModel;
		}
		public function set purchaseBankModel(value:Vector.<PurchasesBankModel>):void
		{
			__purchasesBankModel = value;
		}
		public function get lifeModel():LifeModel 
		{
			if(!__lifeModel) __lifeModel = new LifeModel();
			return __lifeModel;
		}
		public function set lifeModel(value:LifeModel):void
		{
			__lifeModel = value;
		}
		public function get appInfoModel():AppInfoModel 
		{
			if(!__appInfoModel) __appInfoModel = new AppInfoModel();
			return __appInfoModel;
		}
		public function set appInfoModel(value:AppInfoModel):void
		{
			__appInfoModel = value;
		}
		
		public function get levelsModelContainer():Vector.<LevelModel>
		{
			if(!__levelModel) 
			{
				__levelModel = new Vector.<LevelModel>();
			}
			return __levelModel;
		}
		public function set levelsModelContainer(value:Vector.<LevelModel>):void
		{
			__levelModel = value;
		}
	}
}