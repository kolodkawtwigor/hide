package GraphicsArchitecture.ButtonsElements.SystemButtons
{
	import GraphicsArchitecture.MImage;
	
	
	import starling.display.Button;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.filters.ColorMatrixFilter;
	
	public class SoundBtn extends Sprite
	{
		private var buttonName:String;
		private var _callBack:Function;
		private var textField:Image;
		private var _secondItem:MImage;
		private var button:Button;
		private var _on:Boolean;
		public function SoundBtn(callBack:Function, on:Boolean)
		{
			_callBack = callBack;
			_on = on;
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			createButton();
		}
		
		private function createButton():void
		{
			if(_on)button = new Button(MAssetsManager.assetManager.getTexture("sound_on_BTN"))
			else button = new Button(MAssetsManager.assetManager.getTexture("sound_off_BTN"))
				
			button.scaleWhenDown = .9;
			addChild(button);
			button.alignPivot();
			
			button.addEventListener(TouchEvent.TOUCH , onGameKeyClick);
		}
		
	
	
	private function onGameKeyClick(e:TouchEvent):void
	{
		var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
		for each (var touch:Touch in touches) 
		{
			if (touch.phase == TouchPhase.ENDED)
			{
				_callBack();
			}
			if (touch.phase == TouchPhase.BEGAN)
			{
//				SoundGameManager.playFX(SoundGameManager.OTHER, 1);
			}
		}
	}
	
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);
			button.removeEventListener(TouchEvent.TOUCH , onGameKeyClick);
		}
	}
}

