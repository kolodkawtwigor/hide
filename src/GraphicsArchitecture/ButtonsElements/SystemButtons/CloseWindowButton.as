package GraphicsArchitecture.ButtonsElements.SystemButtons
{
	
	import Supporting.SoundGameManager;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.TouchEvent;
	
	public class CloseWindowButton extends Sprite
	{
		private var buttonName:String;
		private var _callBack:Function;
		private var textField:Image;
		private var button:Button;
		public function CloseWindowButton(callBack:Function)
		{
			_callBack = callBack;
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			buttonName = "close_BTN";
			createButton();
		}
		
		
		private function createButton():void
		{
			button = new Button(MAssetsManager.assetManager.getTexture(buttonName))
			button.scaleWhenDown = .9;
			addChild(button);
			button.alignPivot();
			button.addEventListener(Event.TRIGGERED, onGameKeyClick);
		}
		
		private function onGameKeyClick(e:Event):void
		{
//			SoundGameManager.playFX(SoundGameManager.OTHER, 1);
			_callBack();
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
			button.removeEventListener(TouchEvent.TOUCH , onGameKeyClick);
		}
	}
}

