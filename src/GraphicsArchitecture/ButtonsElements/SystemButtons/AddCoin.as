package GraphicsArchitecture.ButtonsElements.SystemButtons
{
	import com.greensock.TweenLite;
	
	import starling.display.Button;
	import starling.display.Canvas;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class AddCoin extends Sprite
	{
		private var buttonName:String;
		private var _callBack:Function;
		private var _secondItem:Image;
		private var button:Button;
		public function AddCoin(callBack:Function)
		{
			_callBack = callBack;
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			buttonName = "addCoin_BTN";
			createButton();
			drawBackGround();
			this.useHandCursor = true;
			
		}
		
		private function drawBackGround():void
		{
			var canvas:Canvas = new Canvas();
			canvas.drawCircle(30,30,25);
			canvas.beginFill();
			addChild(canvas); 
			canvas.alpha = 0;
			canvas.addEventListener(TouchEvent.TOUCH , onGameKeyClick);
		}
		
		private function createButton():void
		{
			button = new Button(MAssetsManager.assetManager.getTexture(buttonName))
			button.scaleWhenDown = .9;
			addChild(button);
			button.touchable = false;
		}
		
		private function onGameKeyClick(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			if( touches.length == 0) 
			{
				tween2Big();
			}
			for each (var touch:Touch in touches) 
			{
				if (touch.phase == TouchPhase.ENDED)
				{
					tween2Big();
					_callBack();
				}
				if (touch.phase == TouchPhase.BEGAN)
				{
					//			SoundGameManager.playFX(SoundGameManager.OTHER, 1);
					tween2Small();
				}
			}
		}
		
		private function tween2Big():void
		{
			TweenLite.to( button, 0.01, { scaleX : 1, scaleY : 1, x:0, y:0 });
		}
		
		private function tween2Small():void
		{
			TweenLite.to( button, 0.01, { scaleX : .9, scaleY : .9, x:3, y:3});
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
			button.removeEventListener(TouchEvent.TOUCH , onGameKeyClick);
		}
	}
}