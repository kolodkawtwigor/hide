package  GraphicsArchitecture.ButtonsElements.MapScreenButtons
{
	import com.greensock.TweenLite;
	
	import GraphicsArchitecture.MImage;
	
	import starling.display.Button;
	import starling.display.Canvas;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class BonusBtn extends Sprite
	{
		private var buttonName:String;
		private var _callBack:Function;
		private var _secondItem:Image;
		private var __rect:Canvas;
		private var button:Button;
		public function BonusBtn(callBack:Function)
		{
			_callBack = callBack;
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			buttonName = "bonus_BTN";
			createButton();
			createSecondItem()
			this.alignPivot();
			drawBackGround();
			this.useHandCursor = true;
		}
		
		private function drawBackGround():void
		{
			var canvas:Canvas = new Canvas();
			canvas.drawCircle(-30,-20,50);
			canvas.beginFill();
			addChild(canvas); 
			canvas.alpha = 0;
			canvas.addEventListener(TouchEvent.TOUCH , onGameKeyClick);
		}
		
		private function createSecondItem():void
		{
			_secondItem = new MImage(MAssetsManager.assetManager.getTexture("bonus_BTN_over"));
			addChild(_secondItem); _secondItem.alignPivot();
			_secondItem.touchable = false;    _secondItem.x = 70; _secondItem.y = -10; _secondItem.visible = false;
		}
		
		private function createButton():void
		{
			button = new Button(MAssetsManager.assetManager.getTexture(buttonName))
			button.scaleWhenDown = .9;
			addChild(button);
			button.alignPivot();
			button.touchable = false;
		}
		
		private function onGameKeyClick(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			if( touches.length == 0) 
			{
				_secondItem.visible = false;
				tween2Big();
			}
			for each (var touch:Touch in touches) 
			{
				if (touch.phase == TouchPhase.ENDED)
				{
					tween2Big();
					_callBack();
				}
				if (touch.phase == TouchPhase.BEGAN)
				{
					//			SoundGameManager.playFX(SoundGameManager.OTHER, 1);
					tween2Small();
					_secondItem.visible = false;
				}
				if (touch.phase == TouchPhase.HOVER)
				{
					
					_secondItem.visible = true;
				}
			}
		}
		
		private function tween2Big():void
		{
			TweenLite.to( button, 0.01, { scaleX : 1, scaleY : 1 });
		}
		
		private function tween2Small():void
		{
			TweenLite.to( button, 0.01, { scaleX : .9, scaleY : .9});
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}

