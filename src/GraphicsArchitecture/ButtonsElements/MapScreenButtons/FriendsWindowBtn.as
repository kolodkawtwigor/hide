package GraphicsArchitecture.ButtonsElements.MapScreenButtons
{
	import GraphicsArchitecture.MImage;
	
	import starling.display.Button;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class FriendsWindowBtn extends Sprite
	{
		private var buttonName:String;
		private var _callBack:Function;
		private var textField:Image;
		private var _secondItem:MImage;
		private var button:Button;
		public function FriendsWindowBtn(callBack:Function)
		{
			_callBack = callBack;
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			buttonName = "gameBtn_1";
			createButton();
			createSecondItem();
		}
		
		private function createSecondItem():void
		{
			_secondItem = new MImage("icons","ico_friends");
			addChild(_secondItem); _secondItem.alignPivot(); _secondItem.touchable = false;
		}
		
		private function createButton():void
		{
			button = new Button(MAssetsManager.getAtlasUniversal("buttons").getTexture(buttonName) )
			var buttonNameDown:String = buttonName + "_down"
			button.downState = 	MAssetsManager.getAtlasUniversal("buttons").getTexture(buttonNameDown);
			button.scaleWhenDown = .9;
			addChild(button);
			button.alignPivot();
			button.scaleX = button.scaleX*InitializationStarling.GlobalSize;
			button.scaleY = button.scaleY*InitializationStarling.GlobalSize;
			
			button.addEventListener(TouchEvent.TOUCH , onGameKeyClick);
		}
		
		private function onGameKeyClick(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			for each (var touch:Touch in touches) 
			{
				if (touch.phase == TouchPhase.ENDED)
				{
					//			SoundGameManager.playFX(SoundGameManager.OTHER, 1);
					_callBack();
					_secondItem.scaleX = _secondItem.scaleX*1.1;_secondItem.scaleY = _secondItem.scaleY*1.1;
				}
				if (touch.phase == TouchPhase.BEGAN)
				{
					_secondItem.scaleX = _secondItem.scaleX *.9;_secondItem.scaleY = _secondItem.scaleY*.9;
				}
			}
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}

