package GraphicsArchitecture.ButtonsElements.MapScreenButtons
{
	import com.greensock.TweenLite;
	
	import GameLogic.GameHelpingService.MTextField;
	
	import GraphicsArchitecture.FontsManager;
	import GraphicsArchitecture.MImage;
	
	import Supporting.SoundGameManager;
	
	import starling.display.Button;
	import starling.display.Canvas;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.filters.BlurFilter;
	
	public class LevelButton extends Sprite
	{
		private var buttonName:String;
		private var _callBack:Function;
		private var textField:Image;
		private var _secondItem:MImage;
		private var button:Button;
//		private var _secondItem2:Image;
		private var _secondItem2:MTextField;
		private var _position:String;
		public function LevelButton(callBack:Function, position:int)
		{
			_callBack = callBack;
			_position = String(position);
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			buttonName = "done_lvl_BTN";
			createButton();
			createSecondItem();
			drawBackGround();
		}
		
		private function drawBackGround():void
		{
			var canvas:Canvas = new Canvas();
			canvas.drawCircle(-30,-20,50);
			canvas.beginFill();
			addChild(canvas); 
			canvas.alpha = 0;
			canvas.addEventListener(TouchEvent.TOUCH , onGameKeyClick);
		}
		
		private function createSecondItem():void
		{
			_secondItem2 = new MTextField(70, 70, _position , MAssetsManager.ARISTA, 60, 0xFFFFFF);
			addChild(_secondItem2);_secondItem2.x = -35; _secondItem2.y = -28;_secondItem2.alignPivot();
			_secondItem2.filter = BlurFilter.createDropShadow(2,1,0x0,.4,2,2);
			
			
			
			
//			_secondItem2 = FontsManager.dropShadowTextField(_position, MAssetsManager.ARISTA,60,0xFFFFFF, 0xF49A09,2,6);
//			addChild(_secondItem2); 
//			_secondItem2.alignPivot();_secondItem2.touchable = false;
//			_secondItem2.x = -35;
//			_secondItem2.y = -28;
		}
		
		private function createButton():void
		{
			button = new Button(MAssetsManager.assetManager.getTexture(buttonName) );
			button.scaleWhenDown = .9;
			addChild(button);
			button.alignPivot();
			button.touchable = false;
//			button.addEventListener(TouchEvent.TOUCH , onGameKeyClick);
		}
		
		
		private function onGameKeyClick(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			
			if( touches.length == 0) 
			{
				tween2Big();
			}
			
			for each (var touch:Touch in touches) 
			{
				if (touch.phase == TouchPhase.ENDED)
				{
					tween2Big();
					_callBack();
				}
				if (touch.phase == TouchPhase.BEGAN)
				{
//					SoundGameManager.playFX(SoundGameManager.OTHER, 1);
					tween2Small();
				}
			}
		}
		
		private function tween2Big():void
		{
			TweenLite.to( _secondItem2, 0.01, { scaleX : 1, scaleY : 1 });
			TweenLite.to( button, 0.01, { scaleX : 1, scaleY : 1 });
		}
		
		private function tween2Small():void
		{
			TweenLite.to( _secondItem2, 0.01, { scaleX : .9, scaleY : .9});
			TweenLite.to( button, 0.01, { scaleX : .9, scaleY : .9});
		}
		
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
			button.removeEventListener(TouchEvent.TOUCH , onGameKeyClick);
//			_secondItem2.texture.dispose();
//			_secondItem2.dispose();
		}
	}
}


