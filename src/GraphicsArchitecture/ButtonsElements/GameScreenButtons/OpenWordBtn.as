package GraphicsArchitecture.ButtonsElements.GameScreenButtons
{
	import com.greensock.TweenLite;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Point;
	
	import GameLogic.GameHelpingService.MTextField;
	
	import GraphicsArchitecture.MImage;
	
	import Supporting.InvertedMask;
	
	import starling.display.Button;
	import starling.display.Canvas;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.filters.BlurFilter;
	import starling.text.TextField;
	import starling.textures.Texture;
	
	public class OpenWordBtn extends starling.display.Sprite
	{
		private var buttonName:String;
		private var _callBack:Function;
		private var _secondItem:Image;
		private var __rect:Canvas;
		private var button:Button;
		private var _background:flash.display.Sprite;
		private var _ellipse:flash.display.Sprite;
		private var _figure:flash.display.Sprite;
		private var _invertedMask:InvertedMask;
		private var _starlingImage:Image;
		private var _globalPoint:Point;
		private var _rectangleWidth:Number = 90;
		private var _distance:Number;
		private var _location:Point;
		public var inventory:TextField;
		
		
		public function OpenWordBtn(callBack:Function )
		{
			_callBack = callBack;
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			buttonName = "open_letter_inv_BTN";
			createButton();
			createSecondItem();
			
			drawBackGround();
			
			createSuportItem();
			this.alignPivot();
			this.useHandCursor = true;
		}
		
		private function createSuportItem():void
		{
			inventory = new MTextField(40, 40, "2" , MAssetsManager.ARISTA, 34, 0xFFFFFF);
			addChild(inventory);inventory.x = 6; inventory.y = 18;inventory.alignPivot();
			inventory.filter = BlurFilter.createDropShadow(1,1,0x0,.5,1,1);
		}
		
		private function drawBackGround():void
		{
			var canvas:Canvas = new Canvas();
			canvas.drawCircle(-30,-20,50);
			canvas.beginFill();
			addChild(canvas); 
			canvas.alpha = 0;
			canvas.addEventListener(TouchEvent.TOUCH , onGameKeyClick);
		}
		
		private function createSecondItem():void
		{
			_secondItem = new MImage(MAssetsManager.assetManager.getTexture("open_letter_inv_BTN_over"));
			addChild(_secondItem); _secondItem.alignPivot();
			_secondItem.touchable = false;   _secondItem.x = 125; _secondItem.y = -20;
			_secondItem.visible = false;
		}
		
		private function createButton():void
		{
			button = new Button(MAssetsManager.assetManager.getTexture(buttonName))
			button.scaleWhenDown = .9;
			addChild(button);
			button.alignPivot();
			button.touchable = false;
		}
		
		private function onGameKeyClick(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			if( touches.length == 0) 
			{
				_secondItem.visible = false;
				tween2Big();
			}
			for each (var touch:Touch in touches) 
			{
					if (touch.phase == TouchPhase.ENDED)
					{
						tween2Big();
						_callBack();
					}
					if (touch.phase == TouchPhase.BEGAN)
					{
						//			SoundGameManager.playFX(SoundGameManager.OTHER, 1);
						tween2Small();
						_secondItem.visible = false;
					}
					if (touch.phase == TouchPhase.HOVER)
					{
						
						_secondItem.visible = true;
					}
			}
		}
		
		private function tween2Big():void
		{
			TweenLite.to( button, 0.01, { scaleX : 1, scaleY : 1 });
		}
		
		private function tween2Small():void
		{
			TweenLite.to( button, 0.01, { scaleX : .9, scaleY : .9});
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}

