package GraphicsArchitecture.ButtonsElements.GameScreenButtons
{
	import starling.display.Button;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class ToMapBtn extends Sprite
	{
		private var buttonName:String;
		private var _callBack:Function;
		private var _secondItem:Image;
		private var button:Button;
		public function ToMapBtn(callBack:Function)
		{
			_callBack = callBack;
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			buttonName = "toMap_BTN";
			createButton();
		}
		
		private function createButton():void
		{
			button = new Button(MAssetsManager.assetManager.getTexture(buttonName))
			button.scaleWhenDown = .9;
			addChild(button);
						button.alignPivot();
			button.addEventListener(TouchEvent.TOUCH , onGameKeyClick);
		}
		
		private function onGameKeyClick(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			for each (var touch:Touch in touches) 
			{
				if (touch.phase == TouchPhase.ENDED)
				{
					_callBack();
				}
				if (touch.phase == TouchPhase.BEGAN)
				{
					//					SoundGameManager.playFX(SoundGameManager.OTHER, 1);
				}
			}
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
			button.removeEventListener(TouchEvent.TOUCH , onGameKeyClick);
		}
	}
}