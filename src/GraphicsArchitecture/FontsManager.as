package GraphicsArchitecture
{
	
	import flash.display.BitmapData;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	import starling.utils.AssetManager;

	public class FontsManager extends Sprite
	{
		static public var __assetsContent:starling.utils.AssetManager = new starling.utils.AssetManager(0, false);
		private static var img:Image;
		private static var texture:Texture;
		
		public function FontsManager()
		{
		}
		
		public static function dropShadowTextField(text:String, fontName:String, textSize:uint, textColor:uint, shadowColor:uint = 0x000000,  shadowThickness:int = 2, strength:int = 1 ,bold:Boolean = false, align:String = TextFormatAlign.CENTER, width:Number = 0, height:Number = 100):Image
		{
				var textfield:TextField = new TextField();
				textfield.text = text;
				textfield.multiline = false;
				if(width == 0) textfield.autoSize = TextFieldAutoSize.LEFT;
				else textfield.width = width;
				textfield.height = height;
				textfield.wordWrap = false;
				
				var format:TextFormat = new TextFormat();
				format.font = fontName;
				format.size = textSize;
				format.color = textColor;
				format.align = align;
				format.bold = bold;
				format.letterSpacing = .2
				
				textfield.setTextFormat(format);
				textfield.embedFonts = true;
				
				addOutline(textfield, shadowColor, shadowThickness, strength);
				
				var bmd:BitmapData = new BitmapData (textfield.width, textfield.height, true, 0);
				
				bmd.draw(textfield,  null, null, null, null, true);
				texture = Texture.fromBitmapData(bmd, false, false)
				var img:Image = new Image(texture);
				textfield.text = '';
				textfield = null;
				
				bmd.dispose();
				bmd = null;
				
			
			return img;
		}
		
		public static function addOutline(obj:TextField, color:uint, thickness:int = 2, strength:int = 1):void 
		{
			var outline:GlowFilter = new GlowFilter();
			outline.blurX = outline.blurY = thickness;
			outline.color = color;
			outline.strength = strength;
			outline.quality = BitmapFilterQuality.HIGH;
			
			obj.filters = [outline];
			
			
			
		}
	}
}