package GraphicsArchitecture.ButtonsGraphics
{
	import starling.events.Event;
	
	public class SimpleButton extends MButton
	{
		public function SimpleButton(params:Object)
		{
			super(params);
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		override protected function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			super.init();
		}
		
		override protected function destroy(e:Event):void
		{
			super.destroy(e);
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
	}
}