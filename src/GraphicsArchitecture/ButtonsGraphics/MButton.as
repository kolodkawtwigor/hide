package GraphicsArchitecture.ButtonsGraphics
{
	import flash.geom.Point;
	
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import GraphicsArchitecture.MImage;
	
	public class MButton extends Sprite
	{
		protected var _defaultIcon:MImage;
		protected var _hoverIcon:MImage;
		protected var _buttonTexture:String;
		protected var _params:Object;
		protected var _buttonName:String;
		protected var _buttonPos:Point;
		
		public function MButton(params:Object)
		{
			//параметры кнопки;
			_params = params;
			_buttonName = _params["buttonName"];
			_buttonTexture = _params["buttonTexture"];
			_buttonPos = _params["buttonPos"];
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		protected function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			addDefaultIcon();
			addHoverIcon();
			
			this.name = _buttonName;
			this.x = _buttonPos.x;
			this.y = _buttonPos.y;
			
			addEventListener(TouchEvent.TOUCH, changeIcons);
			this.alignPivot();
		}
		
		private function addDefaultIcon():void
		{
//			_defaultIcon = new MImage(MAssetsManager.BtnUp, 0, 0, true);
//			_defaultIcon.alignPivot();
//			addChild(_defaultIcon);
		}
		
		private function addHoverIcon():void
		{
//			_hoverIcon = new MImage(MAssetsManager.BtnOver, 0, 0, true);
//			_hoverIcon.alignPivot();
//			_hoverIcon.visible = false;
//			addChild(_hoverIcon);
		}
		
		protected function changeIcons(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			
			if (touches.length == 0)
			{
				_hoverIcon.visible = false;
			}
			
			for each (var touch:Touch in touches) 
			{
				if (touch.phase == TouchPhase.BEGAN)
				{
					this._hoverIcon.visible = false;
				}
				
				if (touch.phase == TouchPhase.ENDED)
				{
					this._hoverIcon.visible = false;
				}
				
				if (touch.phase == TouchPhase.HOVER)
				{
					this._hoverIcon.visible = true;
				}
			}
		}		
		
		protected function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);
			_defaultIcon.texture.dispose();
			_hoverIcon.texture.dispose();
			removeEventListener(TouchEvent.TOUCH, changeIcons);
		}
	}
}