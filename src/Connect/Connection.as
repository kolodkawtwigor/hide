package Connect
{
	import Connect.Local.URLConnector;
	
	import ServiceGame.ServiceModels.GameDataItemsModel;
	import ServiceGame.ServiceModels.PurchaseShopModel;
	
	public class Connection
	{
		static public var gateWayPath:String;
		public var nc:URLConnector;
		
		static public var SOCIAL_NAME:String = "OK";
		static public var user_id:String = "1";
		static public var CURRENCY_NAME:String = "OK";
		static public var APP_URL_GAME:String;
		
		protected var _firstName:String = "Black";
		static public var AVATAR:String;
		protected var _lastName:String = "Jack";
		protected var _socialFriendIDs:Array;
		
		protected var __handler:ServiceHandler;
		protected var _friends_social_ids:Array = new Array();
		protected var _friends_social_in:Array = new Array;
		protected var __onGetAllFriendInfo:Function;
		protected var __onCompleteStatusPost:Function;
		
		public function Connection()
		{
			
		}
		
		public function initConnection():void
		{
			
		}
		
		public function purchase(bonusOrder:String, callBack:Function, type:String):void { new Error("Нужно реализовать эту функцию"); }
		public function purchaseBonus(callBack:Function, bonusItem:PurchaseShopModel):void { new Error("Нужно реализовать эту функцию"); }
		public function purchaseComplete(callBack:Function):void { new Error("Нужно реализовать эту функцию"); }
		public function inviteFriends(message:String, params:String, selected_uids:String, callBack:Function = null):void { new Error("Нужно реализовать эту функцию"); }
		public function setStatusPost(callBack:Function, text:String, url:String):void{ new Error("Нужно реализовать эту функцию"); }
		public function setSocialName():void{ SOCIAL_NAME = "OK"; }
		public function wallPost(callBack:Function, message:String):void{ new Error("Нужно реализовать эту функцию"); }
		public function askLife(callBack:Function, message:String):void{ new Error("Нужно реализовать эту функцию"); }
		public function sayThankU(callBack:Function, text : String, params : String = null, selectedUids:String = null):void{ new Error("Нужно реализовать эту функцию"); }
		public function goToGroup():void{ new Error("Нужно реализовать эту функцию"); }
		
		public function wallPostBitmap(callBack:Function, bitmap:*, message:String = null, uid:String = null, helloCountryName:String = null):void{ new Error("Нужно реализовать эту функцию"); }
		/**ИНФОРМАЦИЯ О ПОЛЬЗОВАТЕЛЕ**/
		public function userInfo(target:*, fun:Function):void
		{
			var param:Object = { 
				"player":{
					"platformId": user_id,
					"platformType":SOCIAL_NAME,
					"nameFirst":_firstName,
					"nameLast":_lastName,
					"image":AVATAR,
					"friends":_friends_social_ids
				}
			};
			ServiceHandler.instance.netConnection(gateWayPath + "game/init", param, target, fun)
		}
		
		private function getFriends( data: Object ):void
		{		
			for (var i:int = 0; i < data.uids.length; i++) 
			{
				_friends_social_ids.push( data.uids[i] );
			}	
		}
		
		public function initServer():void
		{
			nc = new URLConnector();
			
			this.__handler = new ServiceHandler(nc, gateWayPath);
			userInfo(this, ServerConnect.instance.userLoad);
		}
		
		protected function onFault(fault:Object):void { trace(String(fault.description));	}
	}
}
