package Connect.Purchases
{
	import Connect.currentConnection;
	
	import ServiceGame.ServiceModels.PurchaseModel;
	import ServiceGame.ServiceModels.PurchasesBankModel;
	
	import flash.events.EventDispatcher;
	

	public class PurchaseManager extends EventDispatcher
	{
		protected var __onPurchaseComplete:Function;
		
		public function PurchaseManager(purchaseType:String, callBack:Function)
		{
			__onPurchaseComplete = callBack;
		}
		
		protected function makePurchase(bonusOrder:int):void
		{
			new Error("Следует реализовать метод для покупки");
		}
		
		protected function handleApiEvent(event:*):void
		{
			trace(event.method)
			__onPurchaseComplete.call();
			currentConnection().purchaseComplete(handleApiEvent);
		}
		
		static public function purchaseModel(type:String):PurchasesBankModel
		{
			var _pm:Vector.<PurchasesBankModel> = Service.instance.purchaseBankModel;
			for (var i:int = 0; i < _pm.length; i++) 
			{
				if(_pm[i].id == type) return _pm[i];
			}	
			return null;
		}
	}
}