package Connect.Purchases
{
	import Connect.Jsons.JSON;
	import Connect.OK.ApiCallbackEvent;
	import Connect.OK.ForticomAPI;
	import Connect.Purchases.PurchaseManager;
	
	import ServiceGame.ServiceModels.PurchaseModel;
	import ServiceGame.ServiceModels.PurchasesBankModel;
	
	public class PurchaseOK extends PurchaseManager
	{
		
		public function PurchaseOK(purchaseType:String, callBack:Function)
		{
			ForticomAPI.removeEventListener(ApiCallbackEvent.CALL_BACK, handleApiEvent);
			super(purchaseType, callBack);
			
			var _pm:Vector.<PurchasesBankModel> = Service.instance.purchaseBankModel;
			for (var i:int = 0; i < _pm.length; i++) 
			{
				if(_pm[i].id == purchaseType) makePurchase(i);
			}
		}
		
		override protected function makePurchase(bonusOrder:int):void
		{
			ForticomAPI.addEventListener(ApiCallbackEvent.CALL_BACK, handleApiEvent);
			
			var obj:Object = {
				"type" : "BANK",
				"id" : Service.instance.purchaseBankModel[bonusOrder].id
			};
			var purchase:String = Connect.Jsons.JSON.encode(obj);
			
			var tipeOfWord:String;
			var serviceType:int = int(Service.instance.purchaseBankModel[bonusOrder].total);
			
			if (serviceType == 1)  tipeOfWord = "жизнь";
			else tipeOfWord = "жизней";
			
			var tipeOfLife:String = Service.instance.purchaseBankModel[bonusOrder].total+ " " + tipeOfWord; 
				
				
			ForticomAPI.showPayment(tipeOfLife,
				Service.instance.purchaseBankModel[bonusOrder].typeId,
				purchase,
				int(Service.instance.purchaseBankModel[bonusOrder].priceHard), null, null, null, 'true');
			
		}
	}
}