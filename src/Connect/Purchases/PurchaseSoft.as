package Connect.Purchases
{
	import Connect.ServiceHandler;
	import Connect.service;
	
	import ServiceModels.Inventory;
	
	import ServiceGame.ServiceModels.GameDataItemsModel;
	import ServiceGame.ServiceModels.PurchaseModel;
	
	import flash.events.Event;

	public class PurchaseSoft extends PurchaseManager
	{
		public function PurchaseSoft(purchaseType:String, callBack:Function)
		{
			super(purchaseType, callBack);
			
			var _pm:Vector.<Inventory> = service().playerModel.inventory;
			for (var i:int = 0; i < _pm.length; i++) 
			{
				if(_pm[i].id == purchaseType) makePurchase(i);
			}
		}
		
		override protected function makePurchase(bonusOrder:int):void
		{
			ServiceHandler.instance.itemUse (this, function (res:*):void
			{
				if(res.response.message != "Not enough currency soft")
				{
					__onPurchaseComplete.call();
				}
			}
				, service().playerModel.inventory[bonusOrder].id);	
		};
	}
}