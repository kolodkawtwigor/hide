package Connect.Purchases
{
	import Connect.Jsons.JSON;
	import Connect.OK.ApiCallbackEvent;
	import Connect.OK.ForticomAPI;
	import Connect.Purchases.PurchaseManager;
	
	import ServiceGame.ServiceModels.GameDataItemsModel;
	import ServiceGame.ServiceModels.PurchaseShopModel;
	
	
	public class PurchaseOKShop extends PurchaseManager
	{
		private var __bonusItem:GameDataItemsModel;
		
		public function PurchaseOKShop(purchaseType:String, callBack:Function, bonusItem)
		{
			ForticomAPI.removeEventListener(ApiCallbackEvent.CALL_BACK, handleApiEvent);
			super(purchaseType, callBack);
			__bonusItem = bonusItem;
			var _pm:Vector.<PurchaseShopModel> = Service.instance.purchasesShopModel;
			for (var i:int = 0; i < _pm.length; i++) 
			{
				if(_pm[i].itemId == purchaseType) 
				{
					makePurchase(i);
				}
			}
		}
		
		override protected function makePurchase(bonusOrder:int):void
		{
			ForticomAPI.addEventListener(ApiCallbackEvent.CALL_BACK, handleApiEvent);
			
			var obj:Object = {
				"type" : "SHOP",
				"id" : Service.instance.purchasesShopModel[bonusOrder].id
			};
			
			var purchase:String = Connect.Jsons.JSON.encode(obj);
			
			ForticomAPI.showPayment(__bonusItem.name,
				__bonusItem.description,
				purchase,
				int(Service.instance.purchasesShopModel[bonusOrder].priceHard), null, null, null, 'true');
		}
	}
}