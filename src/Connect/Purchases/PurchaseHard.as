package Connect.Purchases
{
	import Connect.OK.ApiCallbackEvent;
	import Connect.OK.ForticomAPI;
	import Connect.currentConnection;
	import Connect.service;
	
	import ServiceGame.ServiceModels.PurchaseModel;

	public class PurchaseHard extends PurchaseManager
	{
		
		public function PurchaseHard(purchaseType:String, callBack:Function)
		{
			super(purchaseType, callBack);
			
			var _pm:Vector.<PurchaseModel> = service().purchaseModel;
			for (var i:int = 0; i < _pm.length; i++) 
			{
				if(_pm[i].purchase_id == purchaseType) makePurchase(i);
			}
		}
		
		override protected function makePurchase(bonusOrder:int):void
		{
			currentConnection().purchase(bonusOrder, handleApiEvent);
		}
	}
}