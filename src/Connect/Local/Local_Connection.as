package Connect.Local
{
	import Connect.Connection;
	import Connect.ServerConnect;
	import Connect.ServiceHandler;
	
	import flash.display.Sprite;

	public class Local_Connection extends Connection
	{
		static private var _init:Local_Connection;
		static public function instance():Local_Connection { return _init; }
		
		public function Local_Connection()
		{
			super();
			_init = this;
			SOCIAL_NAME = "LOCAL";
		}
		
		override public function initConnection():void
		{
			nc = new URLConnector();
			
			this.__handler = new ServiceHandler(nc, gateWayPath);
			userInfo(this, ServerConnect.instance.userLoad);
		}
		
		override public function setSocialName():void
		{
			SOCIAL_NAME = "LOCAL";
		}
		
		/**ЗАПРОС О ПОЛЬЗОВАТЕЛЕ*/
		override public function userInfo(target:*, fun:Function):void
		{
			var param:Object = { 
				"player":{
					"platformId":ServerConnect.SOCIAL_ID,
						"platformType":SOCIAL_NAME,
						"nameFirst":_firstName,
						"nameLast":_lastName,
						"image":AVATAR,
						"friends":_friends_social_ids
				}
			};
			
			ServiceHandler.instance.netConnection(gateWayPath + "game/init", param, target, fun)	
		}
	}
}