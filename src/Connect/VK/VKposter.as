package Connect.VK
{
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.system.LoaderContext;
	import flash.utils.ByteArray;
	
	import Connect.ServerConnect;
	import Connect.Jsons.JSON;
	import Connect.OK.adobe.images.MultipartData;
	
	import Events.GameModelEvents;
	
	import api.com.adobe.images.PNGEncoder;
	
	
	public class VKposter extends EventDispatcher
	{
		private var flashVars:Object;
		private var uid:String;
		private var helloCountryName:String = '';
		private var image:*;
		private var urlLoader:URLLoader = new URLLoader();
		private var gameName:String = "''";
		//		private var gameUrl:String = Connection.APP_URL_GAME;
		private var gameUrl:String = ServerConnect.STATIC_PATH;
		private var albumDescription:String;
		private var albumTitle:String;
		private var photoDescription:String;
		private var _aid:String;
		
		static public var VK:APIConnection;
		
		public function VKposter(_image:*, _uid:String, _helloCountryName:String, _vk:APIConnection):void 
		{
			VK = _vk;
			flashVars = hide.flashVars;
			image = _image;
			if(_uid) uid = _uid;
			albumDescription = "Альбом из игры " + gameName + " " + gameUrl;
			albumTitle = "Альбом из игры " + gameName; 
			photoDescription =_helloCountryName;
			
			if(_image is Bitmap) getUserAlbums(); else loaderBitmap();
		}
		
		private function loaderBitmap():void
		{
			var _loader:Loader = new Loader();
//			_loader.load(new URLRequest(image));
			
			var pictureURLReq:URLRequest = new URLRequest(image);
			
			var context:LoaderContext = new LoaderContext(); 
			context.checkPolicyFile = true;
			
			_loader.load(pictureURLReq, context); 
			
			
			_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onColpleteLoad);
			_loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, infoIOErrorEvent);
			function onColpleteLoad(e:Event):void{
				image = e.currentTarget.content;
				getUserAlbums();
			}
		}
		
		protected function infoIOErrorEvent(event:IOErrorEvent):void
		{
			// TODO Auto-generated method stub
			
		}
		
		private function getUserAlbums():void 
		{
//			ServerConnect.BONUS_PUBLISH = true;
			VK.api('photos.getAlbums', { owner_id: uid }, onGetAlbumsHandler, onApiRequestFail);
		}
		
		/**---------------Error-------------*/
		private function onApiRequestFail( data: Object ): void
		{
//			ServerConnect.BONUS_PUBLISH = true;
			trace("Error: "+data.error_msg+"\n");
		}
		
		private function onGetAlbumsHandler(response:*):void 
		{
			for (var i:int = 0; i < response.length; i++) 
			{
				if (response[i].title == albumTitle)
				{
					if(!uid) uid = response[i].owner_id;
					getUploadUrl(response[i].aid);
					return;
				}
			}
			createAlbum();
		}
		
		private function createAlbum():void 
		{
			VK.api('photos.createAlbum', {title: albumTitle, description: albumDescription}, onCreateAlbumHandler, onApiRequestFail);
		}
		
		private function onCreateAlbumHandler(response:*):void 
		{
			getUploadUrl(response.aid);
		}
		
		private function getUploadUrl(aid:*):void 
		{
			_aid = aid;
			
			VK.api('photos.getUploadServer', {album_id:_aid, aid: _aid }, onGetUploadUrlHandler, onApiRequestFail);
		}
		
		private function onGetUploadUrlHandler(response:*):void 
		{
			var jpgStream:ByteArray = PNGEncoder.encode(image.bitmapData);
			var url_request:URLRequest = new URLRequest(response.upload_url);
			url_request.method = URLRequestMethod.POST;
			url_request.requestHeaders.push(new URLRequestHeader("Content-type", "multipart/form-data; boundary=" + MultipartData.BOUNDARY));
			var urlloader:URLLoader = new URLLoader();
			var data:MultipartData = new MultipartData();
			data.addFile(jpgStream, "photo", "photo.jpg");
			url_request.data = data.data;
			urlloader.addEventListener(Event.COMPLETE, albumSavePhoto);
			urlloader.load(url_request);
		}
		
		private function albumSavePhoto(e:Event):void 
		{
			var data:Object = Connect.Jsons.JSON.decode(e.target.data as String);
			
			VK.api('photos.save', data, wallPost, onApiRequestFail);
		}
		
		private function wallPost(data:Object):void
		{
			var _photo:String = '';
			var request : Object = { owner_id: uid, message: photoDescription, attachments: data[0].id };
			VK.api("wall.post", request, streamPost, onApiRequestFail);
		}
		
		private function streamPost(response:*):void 
		{			
			dispatchEvent( new Event(GameModelEvents.WALL_UPLOAD_POST));
		}
		
		public function removed():void 
		{
			
		}
		
		private function onEndPosting(response:*):void 
		{
			
		}
	}
	
}