package Connect.VK
{
	import flash.events.Event;
	import flash.net.URLRequest;
	
	import Connect.Connection;
	import Connect.ServerConnect;
	import Connect.ServiceHandler;
	import Connect.service;
	import Connect.OK.ApiCallbackEvent;
	import Connect.OK.ForticomAPI;
	import Connect.VK.events.CustomEvent;
	
	import Events.GameModelEvents;
	
	import ServiceGame.Service;
	import ServiceGame.ServiceModels.FriendsInSocialModel;
	import ServiceGame.ServiceModels.GameDataItemsModel;
	import ServiceGame.ServiceModels.PurchaseShopModel;
	import ServiceGame.ServiceModels.PurchasesBankModel;
	
//	import managers.ServiceTools;
	
	
	public class VK_Connection extends Connection
	{
		static private var _init:VK_Connection;
		static public function instance():VK_Connection { return _init; }
		private var __onPurchaseComplete:Function;
		private var VK:APIConnection;
		
		private var flashVars:Object;
		
		private var __fields:String = 'id, photo_50, photo_100, last_name, first_name';
		
		private var __OKposter:VKposter;
		
		private var __callBackOrder:Function;
		private var __purchaseCallBack:Function;
		public function VK_Connection():void
		{
			super();
			_init = this;
			SOCIAL_NAME = "VK";
		}
		
		override public function initConnection():void
		{
//			FOR CHANGE
			flashVars = hide.flashVars;
			
			VK = new APIConnection(flashVars);
			
			VK.addEventListener('onConnectionInit', onConnect);
			VK.addEventListener('OI_ERROR', onErrorConnection);
			
			VK.addEventListener('onOrderCancel', cancelBack);
			VK.addEventListener('onOrderFail', cancelBack);       		
			VK.addEventListener('onOrderSuccess', onOrderSuccess);
		}
		
		/**ЗАПРОС ИНФОРМАЦИИ О СЕБЕ В ОК*/
		private function onConnect(e:CustomEvent): void
		{
			VK.api('users.get', { user_ids: flashVars['viewer_id'], fields: __fields }, _getUsersInfo, onApiRequestFail);
		}
		
		/**---------------Error-------------*/
		private function onApiRequestFail( data: Object ): void
		{
		
			trace("Error: "+data.error_msg+"\n");
		}
		
		/**ИНФОРМАЦИЯ О СЕБЕ В VK*/
		private function _getUsersInfo( data: Object ): void
		{	
			user_id = data[0].uid;
			
			//			if(user_id == "26122179") KungFu.instance.addChild( new CheatConsole );
				
			AVATAR = data[0].photo_100;
			_lastName = data[0].last_name;
			_firstName = data[0].first_name;
			
			VK.api('friends.get', { user_id: flashVars['viewer_id'], fields: __fields,count:200 }, _getFriends, onApiRequestFail);
		}
		
		
		
		/**СПИСОК ДРУЗЕЙ В VK*/
		private function _getFriends( data: Object ):void
		{		
//			//Loader.instance.show(90, "Соединяемся с сервером");
			for (var i:int = 0; i < data.length; i++) 
			{
				_friends_social_in.push( data[i].toString() );
			}
			_saveFriend(data);
		}
		
		private function _saveFriend(data: Object):void
		{
			if(data.length == 0) return;
			
			for (var i:int = 0; i < data.length; i++) 
			{
				var friend:FriendsInSocialModel = new FriendsInSocialModel();
				friend.user_id = data[i].uid;
				friend.avatar = data[i].photo_100;
				friend.lastName = data[i].last_name;
				friend.firstName = data[i].first_name;
				service().friendInSocialModel.push(friend);
			}
			VK.api('friends.getAppUsers', { user_id: flashVars['viewer_id'], fields: __fields,count:100 }, _getFriendsInGame, onApiRequestFail);
		}
		
		private function _getFriendsInGame(data: Object):void
		{
			for (var i:int = 0; i < data.length; i++) 
			{
				_friends_social_ids.push( data[i].toString() );
			}
			initServer();
		}	
		
		override public function setSocialName():void
		{
			SOCIAL_NAME = "VK";
		}
		
		/**РАССЫЛКА*/
		override public function sayThankU(callback:Function, message:String, params:String = null, ids:String = null ):void
		{
			VK.callMethod('showRequestBox', int(ids), message);
			VK.addEventListener('onRequestSuccess', sendNotification);
			
			function sendNotification(data:Object):void
			{
				var _param:Object = {player: {id: service().playerModel.id}, user_ids: ids.toString(), message: message};
//				__handler.notificationVK(this, null, _param);
				
				if(callback) 
				{
					callback();
				}
					VK.removeEventListener('onRequestSuccess', sendNotification);
			}
		}
		
		var eventFlag:Boolean = false;
		
		/**ПОКУПКА ЗА СОЦ ВАЛЮТУ*/
		
		
		override public function purchase(id:String, callBack:Function, type:String):void
		{
//			StatusStage.active();
			__purchaseCallBack = callBack;
			
			var _pm:Vector.<PurchasesBankModel> = Service.instance.purchaseBankModel;
			var obj:String = "BANK:" + id;
			VK.callMethod('showOrderBox', { type: 'item', item: obj }, callBack, onApiRequestFail);
		}
		
		protected function onOrderSuccess(event:Event):void
		{
			__purchaseCallBack(null);
			VK.removeEventListener('onOrderSuccess', __purchaseCallBack);
		}		
		
		
		private function cancelBack(data:* = null):void
		{
			trace('cancelBack');
			
		}
		
		override public function purchaseComplete(callBack:Function):void
		{
			ForticomAPI.removeEventListener(ApiCallbackEvent.CALL_BACK, callBack);
		}
		
		//******************************************************************************************************
		/**Покупка энергии за соц валюту**/
		override public function purchaseBonus(callBack:Function, bonusItem:PurchaseShopModel):void
		{
//			StatusStage.active();
			__purchaseCallBack = callBack;
			var obj:String = "SHOP:" + bonusItem.id;
			VK.callMethod('showOrderBox', { type: 'item', item: obj }, callBack, onApiRequestFail);
		}
		
		override public function goToGroup():void
		{
			var loc:* = new URLRequest("http://vk.com/club94584683");
			flash.net.navigateToURL(loc);
		}
		
		private function cancelBackSoc(data:* = null):void
		{
			trace('cancelBack');
		}
		
		public function purchaseSocComplete(callBack:Function):void
		{
			//			ForticomAPI.removeEventListener(ApiCallbackEvent.CALL_BACK, callBack);
		}
		
		/**Покупка промо за соц валюту**/
		public function purchaseSocPromo(id:String, key:int, price:int, purchaseName:String, callBack:Function):void
		{
			//			stanica.instance.stage.displayState = StageDisplayState.NORMAL;
			
			var obj:String = "PROMOTION:" + id + ":" + key;
			
			VK.callMethod('showOrderBox', { type: 'item', item: obj }, callBack, onApiRequestFail);
			
			VK.addEventListener('onOrderCancel', cancelBackSocPromo);
			VK.addEventListener('onOrderSuccess', callBack);
			VK.addEventListener('onOrderFail', cancelBackSocPromo);
		}
		
		private function cancelBackSocPromo(data:* = null):void
		{
			trace('cancelBack');
		}
		
		public function purchaseSocCompletePromo(callBack:Function):void
		{
			//ForticomAPI.removeEventListener(ApiCallbackEvent.CALL_BACK, callBack);
		}
		
		
		/**ПРИГЛАСИТЬ ДРУГА*/
		override public function inviteFriends( message:String, params:String, selected_uids:String, callBack:Function = null):void
		{
			VK.callMethod('showInviteBox');
		}
		
		
		/**ПОКАЗАТЬ ВСЕХ ДРУЗЕЙ*/
		public function getAllFriends(callBack:Function):void
		{
			__onGetAllFriendInfo = callBack;
			VK.api('friends.get', { user_id: flashVars['viewer_id'] }, _getAllFriends, onApiRequestFail);
		}
		
		private function _getAllFriends(res:*):void
		{
			if(res.length == 0) 
			{
				__onGetAllFriendInfo.call();
				return;
			}
			
			var idsList:String = "";
			var len:Number = res.length;
			if(res.length > 100) len = 101;
			for (var j:int = 0; j < len; j++) 
			{
				var isConsist:Boolean = false;
				for (var i = 0; i < _friends_social_ids.length; i++)
				{
					if(_friends_social_ids[i] == res[j])
					{
						isConsist = true;
						break;
					}
				}
				trace(res[j], " in getAllFrends");
				if(!isConsist) idsList = idsList + res[j] + ",";
			}
			
			VK.api('users.get', { uids: idsList , fields: __fields }, __onGetFriendInfo, onApiRequestFail);
		}
		
		private function __onGetFriendInfo(res:*):void
		{
			if(res)
			{
				for (var i:int = 0; i < res.length; i++) 
				{
//					if(res[i].photo_50.search('gif') == -1)
//					{
//						var __fam:FriendsAllModel = new FriendsAllModel();
//						__fam.photo = res[i].photo_50;
//						__fam.firstName = res[i].first_name;
//						__fam.lastName = res[i].last_name;
//						__fam.uid = res[i].uid;
//						service().friendAllModel.push( __fam );
//					}
				}
			}
			
			__onGetAllFriendInfo.call();
		}
		
		/**УСТАНОВИТЬ СТАТУС ПРИЛОЖЕНИЯ*/
		override public function setStatusPost(callBack:Function, text:String, url:String):void
		{
			//			stanica.instance.stage.displayState = StageDisplayState.NORMAL;
			//			if(callBack) __onCompleteStatusPost = callBack;
			
			var stat:String = text;
			
			publishPost(callBack, stat, {param: {url: url, desc: ''}});
		}
		
		/**ОПУБЛИКОВАТЬ Bitmap*/
		override public function wallPostBitmap(callBack:Function, bitmap:*, message:String = null, uid:String = null, helloCountryName:String = null):void
		{
			if(__OKposter){
				__OKposter.removed();
				__OKposter.removeEventListener(GameModelEvents.WALL_UPLOAD_POST, onPostComplete);
				__OKposter = null;
			}
			
			onWallPost();
			
			function onWallPost():void 
			{
				__OKposter = new VKposter(bitmap, flashVars['viewer_id'], message, VK);
				__OKposter.addEventListener(GameModelEvents.WALL_UPLOAD_POST, onPostComplete);
			}
			
			function onPostComplete(e:Event):void 
			{ 
				if(callBack) callBack(); 
				
				__OKposter.removeEventListener(GameModelEvents.WALL_UPLOAD_POST, onPostComplete);
//				trace(ServerConnect.BONUS_PUBLISH)
//				if(ServerConnect.BONUS_PUBLISH == false) 
//				{
//					service().playerModel.currencySoft += 150;
//					ServiceHandler.instance.setPublishStatus(this, addLife, Service.instance.playerModel.id);
//					ServerConnect.BONUS_PUBLISH = true;
//				}
			}
		}
		
		private function addLife(res:*):void
		{
			
		}
		
		/**Публикация на стену картинки с описанием*/
		public function publishPost(callBack:Function, msg:*, o:Object = null):void
		{
			var _photo:String = '';
			
			if(o){
				if(o['param']['desc']) msg += ' ' + o['param']['desc'];
				if(o['param']['url']) _photo += /*ServerConnect.STATIC_PATH + */o['param']['url'];
			}
			
			wallPostBitmap(callBack, _photo ? _photo : null, msg, flashVars['viewer_id'], null);
		}
		
		/**ДЛЯ ЛОКАЛЬНОГО СОЕДИНЕНИЯ*/
		protected function onErrorConnection(event:Event ):void
		{
			//			if(KungFu.RELEASE)
			//			{
			//				ErrorConnection({status: 'error', response: {code: '0x111'}});
			//				return;	
			//			}
			initServer();
		}
	}
}