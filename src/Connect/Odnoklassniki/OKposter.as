package Connect.Odnoklassniki
{
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.utils.ByteArray;
	
	import Connect.Connection;
	import Connect.currentConnection;
	import Connect.services;
	import Connect.Jsons.JSONMain;
	import Connect.OK.adobe.images.JPGEncoder;
	import Connect.OK.adobe.images.MultipartData;
	
	import Tools.UI.UITools;
	
	import api.com.adobe.json.JSON;
	import api.com.odnoklassniki.Odnoklassniki;
	import api.com.odnoklassniki.events.ApiCallbackEvent;
	import api.com.odnoklassniki.events.ApiServerEvent;

	/**
	 * ...
	 * @author Alex
	 */
	public class OKposter 
	{
		private var uid:String = services().playerModel.social_id;
		private var helloCountryName:String = '';
		private var image:Bitmap;
		private var urlLoader:URLLoader = new URLLoader();
		private var gameName:String = "'Пока станица спит'";
		private var gameUrl:String = Connection.APP_URL_GAME;
		private var albumDescription:String;
		private var albumTitle:String;
		private var photoDescription:String;
		private var postType:String;
		
		
		public function OKposter(_image:Bitmap, _uid:String, message:String, state:String="recipe"):void 
		{
			image = _image;
			if(_uid) uid = _uid;
			
			albumDescription = "Альбом из игры " + gameName + " " + gameUrl + currentConnection().statisticPostCode + currentConnection().postType;
			albumTitle = "Альбом из игры " + gameName; 
			photoDescription = message + ' ' + gameUrl + currentConnection().statisticPostCode + currentConnection().postType; 
			getUserAlbums();
		}
		private function getUserAlbums():void 
		{
			Odnoklassniki.callRestApi("photos.getAlbums", onGetAlbumsHandler, { }, "JSON", "POST");
		}
		private function onGetAlbumsHandler(response:*):void 
		{
			for (var i:int = 0; i < response.albums.length; i++) 
			{
				if (response.albums[i].title == albumTitle) 
				{
					if(!uid) uid = response.albums[i].user_id;
					getUploadUrl(response.albums[i].aid);
					return;
				}
			}
			
			createAlbum();
		}
		private function createAlbum():void 
		{
			Odnoklassniki.callRestApi("photos.createAlbum", onCreateAlbumHandler, {title:albumTitle, description:albumDescription,type:"public" }, "JSON", "POST");
		}
		private function onCreateAlbumHandler(response:*):void 
		{
			getUploadUrl(response);
		}
		private function getUploadUrl(aid:String):void 
		{
			Odnoklassniki.callRestApi("photosV2.getUploadUrl", onGetUploadUrlHandler, {uid:uid, aid:aid }, "JSON", "POST");
		}
		
		private function onGetUploadUrlHandler(response:*):void 
		{
			var jpgEncoder:JPGEncoder = new JPGEncoder(80);
			var jpgStream:ByteArray = jpgEncoder.encode(image.bitmapData);
			var urlRequest:URLRequest = new URLRequest(response.upload_url);
				urlRequest.method = URLRequestMethod.POST;
				urlRequest.requestHeaders.push(new URLRequestHeader("Content-type", "multipart/form-data; boundary=" + MultipartData.BOUNDARY));
			var mdata:MultipartData = new MultipartData();
			mdata.addFile(jpgStream, "pic1");
			urlRequest.data = mdata.data;
			urlLoader.addEventListener(Event.COMPLETE, albumSavePhoto);
			urlLoader.load(urlRequest);
		}
		private function albumSavePhoto(e:Event):void 
		{
			var data:Object = Connect.Jsons.JSONMain.decode(e.target.data as String);
			data = data.photos;
						
			var photo_id:String;
			var token:String;
			for (var key:String in data) 
			{
				photo_id = key;
				token = data[key].token;
				
			}
			
			Odnoklassniki.callRestApi("photosV2.commit", onEndPosting, { photo_id:photo_id, token:token, comment:photoDescription }, "JSON", "POST");
		}
		private function onEndPosting(response:*):void 
		{
			
		}
	}
	
}