package Connect.Odnoklassniki
{
	import flash.display.StageDisplayState;
	
	import Connect.Connection;
	import Connect.ErrorConnection;
	import Connect.currentConnection;
	import Connect.services;
	import Connect.Jsons.JSONMain;
	import Connect.Services.Bank.BankModel;
	import Connect.Services.Friends.FriendsAllModel;
	import Connect.Services.Shop.ShopItem;
	
	import Game.GameHandler;
	import Game.Pause;
	
	import Tools.Other.ServicesTools;
	import Tools.UI.UITools;
	
	import api.com.odnoklassniki.Odnoklassniki;
	import api.com.odnoklassniki.events.ApiServerEvent;


	public class OK_Connection extends Connection
	{
		static private var _init:OK_Connection;
		static public function instance():OK_Connection { return _init; }
		
		static private var API_SECRET_KEY:String;

		public function OK_Connection()
		{
			switch(stanica.DEV_VERSION)
			{
				case 1:
					API_SECRET_KEY = "1B67625DA7009538484C984F";
					break;
				case 2:
					API_SECRET_KEY = "04BFAACBFAF25266544BF01C";
					break;
				case 3:
					API_SECRET_KEY = "B2BC4F47E937103B2F5D5303";
					break;
			}
			_init = this;
			SOCIAL_NAME = "OK";
		}
		
		override public function initConnection():void
		{		
			stanica.instance.console( "initConnection() - start OK connection");
			Odnoklassniki.addEventListener(ApiServerEvent.CONNECTED, onConnect);
			Odnoklassniki.addEventListener(ApiServerEvent.CONNECTION_ERROR, onErrorConnection);
			Odnoklassniki.addEventListener(ApiServerEvent.PROXY_NOT_RESPONDING, onErrorConnection);
			Odnoklassniki.addEventListener(ApiServerEvent.NOT_YET_CONNECTED, onErrorConnection);
			Odnoklassniki.initialize(stanica.instance.stage, API_SECRET_KEY);
			
			
			statisticPostCode = '?wallpost:';
			//Preloader.instance.show(60);
		}
		
		/**ЗАПРОС ИНФОРМАЦИИ О СЕБЕ В ОК*/
		private function onConnect( e: ApiServerEvent ): void
		{
			stanica.instance.console( "onConnect() - connect to OK complete");
			var uids:* = Odnoklassniki.session.uid;
			var fields:String = "uid, first_name, last_name, pic_3, gender, age";
			Odnoklassniki.callRestApi( "users.getInfo" , _getUsersInfo, { uids:uids , fields:fields }, "JSON", "POST" );
			
			ForticomAPI.connection = stanica.instance.loaderInfo.parameters["apiconnection"];
			//Preloader.instance.show(75);
		}
		
		/**ИНФОРМАЦИЯ О СЕБЕ В ОК*/
		private function _getUsersInfo( data: Object ): void
		{	
			stanica.instance.console( "getUsersInfo()");
			stanica.instance.console( "user_id : ", user_id);
			stanica.instance.console( "Social _lastName: ", _lastName);
			stanica.instance.console( "Social _firstName: ", _firstName);
			//Loader.instance.show(50, "Запрашиваем информацию о друзьях");/
			user_id = data[0].uid
			_avatar = data[0].pic_3;
			_lastName = data[0].last_name;
			_firstName = data[0].first_name;
			
			Connection.age = data[0].age;
			Connection.gender = data[0].gender;
		
			var uids:* = Odnoklassniki.session.uid;
			Odnoklassniki.callRestApi( "friends.getAppUsers" , _getFriends, { uids:uids}, "JSON", "POST" );	
			//Preloader.instance.show(80);
		}
		
		/**СПИСОК ДРУЗЕЙ В ОК*/
		private function _getFriends( data: Object ):void
		{		
			//Loader.instance.show(90, "Соединяемся с сервером");
			for (var i:int = 0; i < data.uids.length; i++) 
			{
				_friends_social_ids.push( data.uids[i] );
			}	
			
			initServer();
		}
		
		override public function setSocialName():void
		{
			//SOCIAL_NAME = "OK";
		}
		
		/**РАССЫЛКА*/
		override public function sendNotification(message:String, params:String = null, ids:String = null, callback:Function = null):void
		{
			stanica.instance.stage.displayState = StageDisplayState.NORMAL;
			ForticomAPI.showNotification(message, params, ids);
			ForticomAPI.addEventListener(ApiCallbackEvent.CALL_BACK, callback);
		}
		
		/**ПОКУПКА ЗА СОЦ ВАЛЮТУ*/
		override public function purchase(id:String, callBack:Function):void
		{
			stanica.instance.stage.displayState = StageDisplayState.NORMAL;
			var purchase:BankModel = ServicesTools.purchaseById( id );
			
			if( purchase.currencyType == "CURRENCY_PREMIUM") var purchaseName:String = "Кристалы";
			else if( purchase.currencyType == "CURRENCY_SOFT") purchaseName = "Монеты";
			
			var obj:Object = {
				"type":"BANK",
				"id": purchase.id
			};
			
			if(services().playerModel.social_id == "558528498432")
			{
				purchase.priceHard = 1;
			}
			
			var product:String = JSONMain.encode(obj);
			
			ForticomAPI.showPayment(purchaseName,
									"",
									product,
									purchase.priceHard, null, null, null, 'true');
			
			ForticomAPI.removeEventListener(ApiCallbackEvent.CALL_BACK_PURCHASE, callBack);
			ForticomAPI.addEventListener(ApiCallbackEvent.CALL_BACK_PURCHASE, callBack);
		}
		
		override public function purchaseComplete(callBack:Function):void
		{
			ForticomAPI.removeEventListener(ApiCallbackEvent.CALL_BACK_PURCHASE, callBack);
		}
		
		//**************************************************
		/**Покупка энергии за соц валюту**/	
		override public function purchaseSoc(id:String, callBack:Function):void
		{
			stanica.instance.stage.displayState = StageDisplayState.NORMAL;
			
			var purchase:ShopItem = ServicesTools.inventoryIdInShop( id );
			
			var purchaseName:String = purchase.name;
			
			var obj:Object = {
				"type":"SHOP",
				"id": purchase.id
			};
			
			if(services().playerModel.social_id == "558528498432")
			{
				purchase.priceHardOK = 1;
			}
			
			var product:String = JSONMain.encode(obj);
			
			ForticomAPI.showPayment(purchaseName,
				"",
				product,
				purchase.priceHardOK, null, null, null, 'true');
	
			ForticomAPI.removeEventListener(ApiCallbackEvent.CALL_BACK_PURCHASE, callBack);
			ForticomAPI.addEventListener(ApiCallbackEvent.CALL_BACK_PURCHASE, callBack);
		}
		
		override public function purchaseSocComplete(callBack:Function):void
		{
			ForticomAPI.removeEventListener(ApiCallbackEvent.CALL_BACK_PURCHASE, callBack);
		}
		
		//***************************************************
		
		/**Покупка промо за соц валюту**/	
		override public function purchaseSocPromo(id:String, key:int, price:int, purchaseName:String, callBack:Function):void
		{
			stanica.instance.stage.displayState = StageDisplayState.NORMAL;
			
			var obj:Object = {
				"type":"PROMOTION",
				"id": id,
				"key": key
			};
			
			if(services().playerModel.social_id == "558528498432")
			{
				price = 1;
			}
			
			var product:String = JSONMain.encode(obj);
			
			ForticomAPI.showPayment(purchaseName,
				"",
				product,
				price, null, null, null, 'true');
			
			ForticomAPI.removeEventListener(ApiCallbackEvent.CALL_BACK_PURCHASE, callBack);
			ForticomAPI.addEventListener(ApiCallbackEvent.CALL_BACK_PURCHASE, callBack);
		}
		
		override public function purchaseSocCompletePromo(callBack:Function):void
		{
			ForticomAPI.removeEventListener(ApiCallbackEvent.CALL_BACK_PURCHASE, callBack);
		}
		
		//***************************************************
		
		/**УСТАНОВИТЬ СТАТУС ПРИЛОЖЕНИЯ*/
		override public function setStatusPost(callBack:Function = null):void
		{
			stanica.instance.stage.displayState = StageDisplayState.NORMAL;
			publishPost(callBack, 'Очень интересная игра! Играю, не могу оторваться!', {param: {url:'Icons/postImg.jpg', desc: 'Вам она понравится!'}});
			/*if(callBack) __onCompleteStatusPost = callBack;
			Odnoklassniki.callRestApi("users.hasAppPermission" , requestStatusPermission, { ext_perm:'SET STATUS' }, "JSON", "POST" )*/
		}
		
		/**ПРИГЛАСИТЬ ДРУГА*/
		override public function inviteFriends(message:String, ids:Array,  callBack:Function = null):void
		{
			stanica.instance.stage.displayState = StageDisplayState.NORMAL;
			var uid2:String = "";
			for (var i:int = 0; i < ids.length; i++) 
			{
				uid2 = uid2 + ids[i] + ";";
			}
			var __message:String = "Отличная игра! Заходи и будем играть вместе!";
			ForticomAPI.showInvite(__message, "", uid2);
		}
		
		
		/**ПОКАЗАТЬ ВСЕХ ДРУЗЕЙ*/
		override public function getAllFriends(callBack:Function):void
		{
			__onGetAllFriendInfo = callBack;
			Odnoklassniki.callRestApi( "friends.get" , _getAllFriends, { uids:Odnoklassniki.session.uid}, "JSON", "POST" );
		}
		
		private function _getAllFriends(res:*):void
		{
			if(res.length == 0) 
			{
				__onGetAllFriendInfo.call();
				return;
			}
			var fields:String = "uid, first_name, last_name, pic_1";
			var idsList:String = "";
			var len:Number = res.length;
			if(res.length > 100) len = 101;
			for (var j:int = 0; j < len; j++) 
			{
				var isConsist:Boolean = false;
				for (var i = 0; i < _friends_social_ids.length; i++)
				{
					if(_friends_social_ids[i] == res[j])
					{
						isConsist = true;
						break;
					}
				}
				trace(res[j], " in getAllFrends");
				if(!isConsist) idsList = idsList + res[j] + ",";
			}
			
			Odnoklassniki.callRestApi( "users.getInfo" , __onGetFriendInfo, { uids:idsList , fields:fields }, "JSON", "POST" );
		}
		
		private function __onGetFriendInfo(res:*):void
		{
			if(res)
			{
				for (var i:int = 0; i < res.length; i++) 
				{
					var __fam:FriendsAllModel = new FriendsAllModel();
						__fam.photo = res[i].pic_1;
						__fam.firstName = res[i].first_name;
						__fam.lastName = res[i].last_name;
						__fam.uid = res[i].uid;
						services().friendAllModel.push( __fam );
				}
			}
			
			__onGetAllFriendInfo.call();
		}
		
		/**ОПУБЛИКОВАТЬ*/
		override public function wallPost(callBack:Function, message:String):void
		{
			stanica.instance.stage.displayState = StageDisplayState.NORMAL;
			var request : Object = {method : "stream.publish", uid : 0, message : message};
			request = SignUtil.signRequest(request, true);
			
			ForticomAPI.showConfirmation("stream.publish", message, request["sig"]);
			callBack.call();
		}
		
		/**ОПУБЛИКОВАТЬ Bitmap*/
		override public function wallPostBitmap(callBack:Function, bitmap:*, message:String = null, uid:String = null, helloCountryName:String = null):void
		{
			
			stanica.instance.stage.displayState = StageDisplayState.NORMAL;
			Odnoklassniki.callRestApi("users.hasAppPermission" , requestWallPost, { ext_perm:'PHOTO_CONTENT' }, "JSON", "POST" );
			
			function requestWallPost(response:*):void 
			{
				UITools.removeCursore();
				if(Pause.STATUS && !Pause.ON_GAME) Pause.instance.play(); 
				if(response){
					onWallPost();
				}else {
					Odnoklassniki.showPermissions("PHOTO_CONTENT");
					ForticomAPI.addEventListener(ApiCallbackEvent.CALL_BACK, resolutionWallPost);
				}
			}
			
			function resolutionWallPost(response:*):void 
			{
				ForticomAPI.removeEventListener(ApiCallbackEvent.CALL_BACK, resolutionWallPost);
				if(response.result == 'ok') onWallPost();
			}
			
			function onWallPost():void 
			{
				new OKposter(bitmap, uid, message);
				callBack();
			}
		}
		
		/**Публикация на стену картинки с опиванием*/
		override public function publishPost(callBack:Function, msg:*, o:Object = null):void
		{
			msg += Connection.APP_URL_GAME + statisticPostCode + postType;
			stanica.instance.stage.displayState = StageDisplayState.NORMAL;
			Odnoklassniki.callRestApi("users.hasAppPermission" , requestTapePost, { ext_perm:'PUBLISH_TO_STREAM' }, "JSON", "POST" );
			
			var request : Object = { "media": [] };
			
			textPost(msg, request['media']);
			
			function textPost(o:*, _request:Array):void {
				var param:Array = _request;
				
				if(o is Array || o is Vector.<String>){
					for (var i:int = 0; i < o.length; i++)
					{
						param.push({ "type": "text", "text": o[i] });
					}
				}else {
					param.push({ "type": "text", "text": o });
				}
			}
			
			if(o){
				switch(o['type'])
				{
					case 'photo':
						request['media'].push(photoPost(o['param']));
						break;
					
					case 'app':
							appPost(o['param'], request['media']);
						break;
						
					default: appPost(o['param'], request['media']); break;
				}
			}
			
			trace(JSON.stringify(request));
			
			function appPost(o:*, _request:Array):Array{
				var param:Array = _request;
				
				for (var i:int = 0; i < o.length; i++) 
				{
					param.push({ 	"type": "app",
									"text": o[i]['desc'],
									"images": [ { 	"url": Connection.STATIC_PATH + o[i]['url'],
													"mark": o[i]['id'],
													"title": o[i]['descPic'] } ] });
				}
				
				if(!(o is Array)) param.push({ "type": "app",
												"text": o['desc'],
												"images": [ { 	"url": Connection.STATIC_PATH + o['url'],
																"mark": o['id'],
																"title": o['descPic'] } ] });
				
				return param;
			}
			
			function photoPost(o:Object):Object{
				var param:Object = {"type": "photo", "list": []};
				
				for (var names:String in o) 
				{
					param['list'].push({id: o['idOk']});
				}
				
				return param;
			}
			
			function requestTapePost(response:*):void 
			{
				if(response){
					onTapePost();
				}else {
					Odnoklassniki.showPermissions("PUBLISH_TO_STREAM");
					ForticomAPI.addEventListener(ApiCallbackEvent.CALL_BACK, resolutionTapePost);
				}
			}
			
			function resolutionTapePost(response:*):void 
			{
				ForticomAPI.removeEventListener(ApiCallbackEvent.CALL_BACK, resolutionTapePost);
				if(response.result == 'ok') onTapePost();
//				onTapePost();
			}
			
			function onTapePost():void 
			{
				ForticomAPI.postMediatopic(JSON.stringify(request), false);
				if(GameHandler.instance.setStatusPost == true) GameHandler.instance.sendRecommendGame();
			}
		}
		
		private function requestStatusPermission(response:*):void 
		{
			stanica.instance.stage.displayState = StageDisplayState.NORMAL;
			if (response) 
			{
				changeStatus();
			}
			else 
			{ 
				Odnoklassniki.showPermissions("SET STATUS");
				ForticomAPI.addEventListener(ApiCallbackEvent.CALL_BACK, onStatusPermissionGranted);
			}
		}
		private function onStatusPermissionGranted(response:*):void 
		{
			if (response.result=="ok" && response.method == "showPermissions") 
			{
				changeStatus();
			}
		}
		
		/*private function changeStatus():void 
		{		
			stanica.instance.stage.displayState = StageDisplayState.NORMAL;
			var stat:String = "Очень интересная игра! Играю, не могу оторваться! " + APP_URL_GAME;
			
			Odnoklassniki.callRestApi("users.setStatus", onStatusAdded, {status: stat}, "JSON", "POST")
			ForticomAPI.removeEventListener(ApiCallbackEvent.CALL_BACK, onStatusPermissionGranted);
		}*/
		
		private function changeStatus():void
		{
			publishPost(onStatusAdded, 'Очень интересная игра! Играю, не могу оторваться!', {param: {url:'Icons/postImg.jpg', desc: 'Вам она понравится!'}});
		}
		
		private function onStatusAdded(event : Boolean):void
		{
			if (event)
			{
			//	ServiceHandler.instance.setPublishStatus(this, function (res:*):void{}, services().playerModel.playerId);
			//	services().playerModel.currencySoft = services().playerModel.currencySoft + services().statusPostModel.rewardCurrency;
				__onCompleteStatusPost.call();
			}
		}
		
		/**ДЛЯ ЛОКАЛЬНОГО СОЕДИНЕНИЯ*/
		protected function onErrorConnection(event:ApiServerEvent ):void
		{
			if(stanica.RELEASE)
			{
				ErrorConnection({status: 'error', response: {code: '0x111'}});
				return;	
			}
			
			initServer();
		}
	}
}