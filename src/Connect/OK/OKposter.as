package Connect.OK
{
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.utils.ByteArray;
	
	import Connect.Jsons.JSON;
	import Connect.OK.adobe.images.JPGEncoder;
	import Connect.OK.adobe.images.MultipartData;
	
	import Vars.MainVars;
	
	import api.com.odnoklassniki.Odnoklassniki;
	import api.com.odnoklassniki.events.ApiServerEvent;

	/**
	 * ...
	 * @author Alex
	 */
	public class OKposter
	{
		private var uid:String;
		private var helloCountryName:String;
		private var image:Bitmap;
		private var urlLoader:URLLoader = new URLLoader();
		private var albumDescription:String;
		private var albumTitle:String;
		private var photoDescription:String;
		
//		FOR CHANGE
		private var gameName:String = "''";
		private var gameUrl:String = " ";
		
		
		
		public function OKposter(_image:Bitmap, _uid:String, helloCountryName:String):void
		{
			image = _image;
			uid = _uid;
			
			albumDescription = "Альбом из игры " + gameName + gameUrl;
			albumTitle = "Альбом из игры " + gameName;
			photoDescription = helloCountryName + gameUrl; 
			
			getUserAlbums();
			
			Odnoklassniki.addEventListener(ApiServerEvent.CONNECTION_ERROR, onErrorConnection);
			Odnoklassniki.addEventListener(ApiServerEvent.PROXY_NOT_RESPONDING, onErrorConnection);
			Odnoklassniki.addEventListener(ApiServerEvent.NOT_YET_CONNECTED, onErrorConnection);
		}
		
		protected function onErrorConnection(event:Event):void
		{
			// TODO Auto-generated method stub
			
		}
		private function getUserAlbums():void 
		{
			//MonsterDebugger.trace(this, "getAlbumsCalled");
			Odnoklassniki.callRestApi("photos.getAlbums", onGetAlbumsHandler, { }, "JSON", "POST");
		}
		private function onGetAlbumsHandler(response:*):void 
		{
			//MonsterDebugger.trace(this, response, "", "getAlbumsResponse");
			for (var i:int = 0; i < response.albums.length; i++) 
			{
				if (response.albums[i].title== albumTitle) 
				{
					if(!uid) uid = response.albums[i].user_id;
					getUploadUrl(response.albums[i].aid);
					return;
				}
			}
			createAlbum();
		}
		private function createAlbum():void 
		{
			//MonsterDebugger.trace(this, "createAlbum");
			Odnoklassniki.callRestApi("photos.createAlbum", onCreateAlbumHandler, {title:albumTitle,description:albumDescription,type:"public" }, "JSON", "POST");
		}
		
		private function onCreateAlbumHandler(response:*):void 
		{
//			<?xml version="1.0" encoding="UTF-8" standalone="yes"?><ns2:error_response xmlns:ns2="http://api.forticom.com/1.0/"><error_code>104</error_code><error_msg>PARAM_SIGNATURE : Invalid signature c32604deecb332a2aa04abc003d409f0, calculated by string application_key=CBAPEIOFEBABABABAdescription=Альбом из игры 'Вуду слов'method=photos.createAlbumsession_key=50ad720fee.21bafbe32f2d7bf735970c3fa314f3424cb212f91beb0c6ctitle=Альбом из игры 'Вуду слов'type=public********SECRET KEY*******</error_msg></ns2:error_response>
			getUploadUrl(response);
		}
		private function getUploadUrl(aid:String):void 
		{
			//MonsterDebugger.trace(this, aid, "", "getUploadUrl for this aid");
//			Odnoklassniki.callRestApi("photosV2.getUploadUrl", onGetUploadUrlHandler, {uid:uid,aid:aid  }, "JSON", "POST");
			var data:Object = 	{ 	'aid'				:	aid,
				'method' 			:  	"photosV2.getUploadUrl"
			};
			var hash:String = signatureOK( data );
			var obj = MainVars.instance.mainVars.session_key
			var obj2 = OK_Connection.API_PUBLIC_KEY
			Odnoklassniki.callRestApi("photosV2.getUploadUrl", onGetUploadUrlHandler, {	
																						'aid'				:	aid, 
																						'session_key'		:	obj,
																						'application_key'	:	obj2/*,
																						'sig'				:	hash*/
																						}, "JSON", "POST");
		}
		
		private function onGetUploadUrlHandler(response:*):void 
		{
			var jpgEncoder:JPGEncoder = new JPGEncoder(80);
			var jpgStream:ByteArray = jpgEncoder.encode(image.bitmapData);
			var urlRequest:URLRequest = new URLRequest(response.upload_url);
			urlRequest.method = URLRequestMethod.POST;
			urlRequest.requestHeaders.push(new URLRequestHeader("Content-type", "multipart/form-data; boundary=" + MultipartData.BOUNDARY));
			var mdata:MultipartData = new MultipartData();
			mdata.addFile(jpgStream, "pic1");
			urlRequest.data = mdata.data;
			urlLoader.addEventListener(Event.COMPLETE, albumSavePhoto);
			urlLoader.load(urlRequest);
			//MonsterDebugger.trace(this, response, "", "getUploadUrl response");
}
		private function albumSavePhoto(e:Event):void 
		{
//			MonsterDebugger.trace(this, "try to save photo");
//			MonsterDebugger.trace(this, e.target.data);
			
			var data:Object = Connect.Jsons.JSON.decode(e.target.data as String);
			data = data.photos;
//			MonsterDebugger.trace(this, data, "", "decoded json data");
						
			var photo_id:String;
			var token:String;
			for (var key:String in data) 
			{
//				MonsterDebugger.trace(this, key, "", "key");
				photo_id = key;
				token = data[key].token;
				
			}
			
			Odnoklassniki.callRestApi("photosV2.commit", onEndPosting, { photo_id:photo_id,token:token,comment:photoDescription }, "JSON", "POST");
		}
		private function onEndPosting(response:*):void 
		{
//			MonsterDebugger.trace(this, response, "", "end posting response");
		}
	}
	
}