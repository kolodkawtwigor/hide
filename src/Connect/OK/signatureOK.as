package Connect.OK
{
	import Connect.OK.adobe.crypto.MD5;
	
	import Vars.MainVars;

	public function signatureOK(data:Object = null):String
	{
		var keysList:Array = new Array();
		var paramsUTF8:String = '';
		
		data['application_key'] = OK_Connection.API_PUBLIC_KEY;
		data['session_key'] = MainVars.instance.mainVars.session_key;

		for(var s:String in data)
		{
			keysList.push( s );	
		}
		
		keysList = keysList.sort();
		
		for each (var i:String in keysList) 
		{
			paramsUTF8 += i + "=" + data[i];
		}
		
		paramsUTF8 += /*services().playerModel.sessionSecretKey*/MainVars.instance.mainVars.session_secret_key 
		
		return MD5.hash( paramsUTF8 );	}
}