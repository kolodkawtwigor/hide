package Connect.OK
{
	import Connect.Connection;
	import Connect.ServiceHandler;
	import Connect.currentConnection;
	import Connect.service;
	import Connect.Jsons.JSON;
	
	import ServiceGame.Service;
	import ServiceGame.ServiceModels.FriendsInSocialModel;
	import ServiceGame.ServiceModels.PurchaseShopModel;
	import ServiceGame.ServiceModels.PurchasesBankModel;
	
	import api.com.odnoklassniki.Odnoklassniki;
	import api.com.odnoklassniki.events.ApiServerEvent;
	
	public class OK_Connection extends Connection
	{
		static private var _init:OK_Connection;
		static public function instance():OK_Connection { return _init; }
		private var flashVars:Object;
		private var __onPurchaseComplete:Function;
		private var __bonusItem:PurchaseShopModel;
		
//		FOR CHANGE
		static public var API_SECRET_KEY:String = "123123";
		public static var API_PUBLIC_KEY:Object = "1231231";
		
		
		
		public function OK_Connection()
		{
			super()
			_init = this;
			SOCIAL_NAME = "OK";
		}
		
		override public function initConnection():void
		{			
			flashVars = hide.flashVars;
			Odnoklassniki.initialize(hide.instance().stage, API_SECRET_KEY);
			
			Odnoklassniki.addEventListener(ApiServerEvent.CONNECTED, onConnect);
			Odnoklassniki.addEventListener(ApiServerEvent.CONNECTION_ERROR, onErrorConnection);
			Odnoklassniki.addEventListener(ApiServerEvent.PROXY_NOT_RESPONDING, onErrorConnection);
			Odnoklassniki.addEventListener(ApiServerEvent.NOT_YET_CONNECTED, onErrorConnection);
		}
		
		/**ЗАПРОС ИНФОРМАЦИИ О СЕБЕ В ОК*/
		private function onConnect( e: ApiServerEvent ): void
		{
			var uids:* = Odnoklassniki.session.uid;
			var fields:String = "uid, first_name, last_name, pic_5";
			Odnoklassniki.callRestApi( "users.getInfo" , _getUsersInfo, { uids:uids , fields:fields }, "JSON", "POST" );
			
			ForticomAPI.connection = hide.instance.loaderInfo.parameters["apiconnection"];
		}
		
		/**ИНФОРМАЦИЯ О СЕБЕ В ОК*/
		private function _getUsersInfo( data: Object ): void
		{	
			user_id = data[0].uid
			AVATAR = data[0].pic_5;
			_lastName = data[0].last_name;
			_firstName = data[0].first_name;
		
			var uids:* = Odnoklassniki.session.uid;
			Odnoklassniki.callRestApi( "friends.getAppUsers" , _getFriends, { uids:uids}, "JSON", "POST" );	
		}
		
		/**СПИСОК ДРУЗЕЙ В ОК*/
		private function _getFriends( data: Object ):void
		{		
			for (var i:int = 0; i < data.uids.length; i++) 
			{
				_friends_social_ids.push( data.uids[i] );
			}	
			var uids:* = Odnoklassniki.session.uid;
//			хуйня потом переделать что бы было уже инфа о каждом друге
			Odnoklassniki.callRestApi( "friends.get" , _initServer, { uids:uids,sort_type:"PRESENT"}, "JSON", "POST" );
		}
		
		private function _initServer(data: Object):void
		{
			for (var i:int = 0; i < data.length; i++) 
			{
				_friends_social_in.push( data[i] );
			}	
			createFriends();
			initServer();
		}
		
		private function createFriends():void
		{
			var len:int = _friends_social_in.length;
			
			for (var i:int = 0; i < len; i++) 
			{
				var uids:* = _friends_social_in[i];
				var fields:String = "uid, first_name, last_name, pic_5";
				Odnoklassniki.callRestApi( "users.getInfo" , _saveFriend, { uids:uids , fields:fields }, "JSON", "POST" );
			}
		}
		
		private function _saveFriend(data: Object):void
		{
			if(data.length == 0) return;
			var friend:FriendsInSocialModel = new FriendsInSocialModel();
			friend.user_id = data[0].uid
			friend.avatar = data[0].pic_5;
			friend.lastName = data[0].last_name;
			friend.firstName = data[0].first_name;
			service().friendInSocialModel.push(friend);
		}
		
		override public function setSocialName():void
		{
			SOCIAL_NAME = "OK";
		}
		
		/**ПОКУПКА ЗА СОЦ ВАЛЮТУ*/
		override public function purchase(bonusOrder:String, callBack:Function, type:String):void
		{
//			StatusStage.active();
			ForticomAPI.removeEventListener(ApiCallbackEvent.CALL_BACK, handleApiEvent);
			__onPurchaseComplete = callBack;
			var _pm:Vector.<PurchasesBankModel> = Service.instance.purchaseBankModel;
			for (var i:int = 0; i < _pm.length; i++) 
			{
				if(_pm[i].id == bonusOrder) 
				{
					makePurchase(i,type);
				}
			}
		}
		
		private function makePurchase(bonusOrder:int,type:String):void
		{
			var obj:Object = {
				"type" : "BANK",
				"id" : Service.instance.purchaseBankModel[bonusOrder].id
			};
			var purchase:String = Connect.Jsons.JSON.encode(obj);
			
			var serviceType:int = int(Service.instance.purchaseBankModel[bonusOrder].total);
			
			switch(type)
			{
				case "coin":
				{
					var tipeOfWord:String = "монет";
					break;
				}
				case "life":
				{
					if (serviceType == 1)  tipeOfWord = "жизнь";
					else tipeOfWord = "жизней";
					break;
				}
					
				default:
				{
					break;
				}
			}
			
			var tipeOfLife:String = Service.instance.purchaseBankModel[bonusOrder].total+ " " + tipeOfWord; 
			
			ForticomAPI.showPayment(tipeOfLife,
				Service.instance.purchaseBankModel[bonusOrder].typeId,
				purchase,
				int(Service.instance.purchaseBankModel[bonusOrder].priceHard), null, null, null, 'true');
			
			ForticomAPI.addEventListener(ApiCallbackEvent.CALL_BACK, handleApiEvent);
		}
		
		override public function purchaseBonus(callBack:Function, bonusItem:PurchaseShopModel):void
		{
//			StatusStage.active();
			ForticomAPI.removeEventListener(ApiCallbackEvent.CALL_BACK, handleApiEvent);
			__onPurchaseComplete = callBack;
			__bonusItem = bonusItem;
			
			var obj:Object = {
				"type" : "SHOP",
				"id" : bonusItem.id
			};
			
			var purchase:String = Connect.Jsons.JSON.encode(obj);
			
			ForticomAPI.showPayment(__bonusItem.count,
				__bonusItem.count,
				purchase,
				int(__bonusItem.priceHard), null, null, null, 'true');
			
			ForticomAPI.addEventListener(ApiCallbackEvent.CALL_BACK, handleApiEvent);
		}
		
		
		
		protected function handleApiEvent(event:*):void
		{
			trace(event.method)
			__onPurchaseComplete.call(null);
			currentConnection().purchaseComplete(handleApiEvent);
		}
		
		override public function purchaseComplete(callBack:Function):void
		{
			ForticomAPI.removeEventListener(ApiCallbackEvent.CALL_BACK, callBack);
		}
		
		/**УСТАНОВИТЬ СТАТУС ПРИЛОЖЕНИЯ*/
		override public function setStatusPost(callBack:Function, text:String, url:String):void
		{
			publishPost(callBack, text , {param: {url:url, desc: ''}});
			
//			if(callBack) __onCompleteStatusPost = callBack;
//			Odnoklassniki.callRestApi("users.hasAppPermission" , requestStatusPermission, { ext_perm:'SET STATUS' }, "JSON", "POST" )
		}
		
		/**ОПУБЛИКОВАТЬ Bitmap*/
		override public function wallPostBitmap(callBack:Function, bitmap:*, message:String = null, uid:String = null, helloCountryName:String = null):void
		{
			Odnoklassniki.callRestApi("users.hasAppPermission" , requestWallPost, { ext_perm:'PHOTO_CONTENT' }, "JSON", "POST" );
			
			function requestWallPost(response:*):void 
			{
				//				UITools.removeCursore();
				//				if(Pause.STATUS && !Pause.ON_GAME) Pause.instance.play(); 
				if(response){
					onWallPost();
				}else {
					Odnoklassniki.showPermissions("PHOTO_CONTENT");
					ForticomAPI.addEventListener(ApiCallbackEvent.CALL_BACK, resolutionWallPost);
				}
			}
			
			function resolutionWallPost(response:*):void 
			{
				//				Odnoklassniki.callRestApi("users.hasAppPermission" , requestWallPost, { ext_perm:'PHOTO_CONTENT' }, "JSON", "POST" );
				ForticomAPI.removeEventListener(ApiCallbackEvent.CALL_BACK, resolutionWallPost);
				if(response.result == 'ok') onWallPost();
			}
			
			function onWallPost():void 
			{
				var uids:* = Odnoklassniki.session.uid;
				var uid:* =  flashVars['viewer_id'];
				new OKposter(bitmap, uids, message);
				if( callBack)callBack();
			}
		}
		
		/**ПРИГЛАСИТЬ ВСЕХ ДРУГА*/
		override public function inviteFriends( message:String,params:String, selected_uids:String, callBack:Function = null):void
		{
			ForticomAPI.showInvite(message, params, selected_uids);
		}
		
		/**ОПУБЛИКОВАТЬ*/
		override public function wallPost(callBack:Function, message:String):void
		{
			var request : Object = {method : "stream.publish", uid : 0, message : message};
			request = SignUtil.signRequest(request, true);
			
			ForticomAPI.showConfirmation("stream.publish", message, request["sig"]);
			callBack.call();
		}
		
		private function requestStatusPermission(response:*):void 
		{
			if (response) changeStatus();
			
			else 
			{ 
				Odnoklassniki.showPermissions("SET STATUS");
				ForticomAPI.addEventListener(ApiCallbackEvent.CALL_BACK, onStatusPermissionGranted);
			}
		}
		
		public function publishPost(callBack:Function, msg:*, o:Object = null):void
		{
//			Odnoklassniki.callRestApi("users.hasAppPermission" , requestTapePost, { ext_perm:'PUBLISH_TO_STREAM' }, "JSON", "POST" );
			
			var request : Object = { "media": [] };
			
			textPost(msg, request['media']);
			
			function textPost(o:*, _request:Array):void {
				var param:Array = _request;
				
				if(o is Array || o is Vector.<String>){
					for (var i:int = 0; i < o.length; i++)
					{
						param.push({ "type": "text", "text": o[i] });
					}
				}else {
					param.push({ "type": "text", "text": o });
				}
			}
			
			if(o){
				switch(o['type'])
				{
					case 'photo':
						request['media'].push(photoPost(o['param']));
						break;
					
					case 'app':
						appPost(o['param'], request['media']);
						break;
					
					default: appPost(o['param'], request['media']); break;
				}
			}
			onTapePost();
			
			function appPost(o:*, _request:Array):Array{
				var param:Array = _request;
				
				for (var i:int = 0; i < o.length; i++) 
				{
					param.push({ "type": "app", "text": o[i]['desc'], "images": [ { "url": o[i]['url'], "mark": o[i]['id'], "title": o[i]['descPic'] } ] });
				}
				
				if(!(o is Array)) param.push({ "type": "app", "text": o['desc'], "images": [ { "url": o['url'], "mark": o['id'], "title": o['descPic'] } ] });
				
				return param;
			}
			
			function photoPost(o:Object):Object{
				var param:Object = {"type": "photo", "list": []};
				
				for (var names:String in o) 
				{
					param['list'].push({id: o['idOk']});
				}
				
				return param;
			}
			
			function requestTapePost(response:*):void 
			{
				if(response)   onTapePost();
				else {
					Odnoklassniki.showPermissions("PUBLISH_TO_STREAM");
					ForticomAPI.addEventListener(ApiCallbackEvent.CALL_BACK, resolutionTapePost);
				}
			}
			
			function resolutionTapePost(response:*):void 
			{
				ForticomAPI.removeEventListener(ApiCallbackEvent.CALL_BACK, resolutionTapePost);
				if(response.result == 'ok') onTapePost();
			}
			
			function onTapePost():void 
			{
				ForticomAPI.postMediatopic(Connect.Jsons.JSON.encode(request), false);
				ForticomAPI.addEventListener(ApiCallbackEvent.CALL_BACK, resolutionMediaPost);
			}
			
			 function resolutionMediaPost(response:*):void
			{
				 ForticomAPI.removeEventListener(ApiCallbackEvent.CALL_BACK, resolutionTapePost);
//				FOR CHANGE
//				 if(response.result == 'ok' && !ServerConnect.BONUS_PUBLISH) 
//				 {
//					
//					 ServiceHandler.instance.setPublishStatus(this, getResult, Service.instance.playerModel.id);
//					 ServerConnect.BONUS_PUBLISH = false;
//				 }
			}
			 
			  function getResult(res:*):void
			 {
//				  								TODO уточнить сколько добавлять
				  
			 }
		}
		
		private function onStatusPermissionGranted(response:*):void 
		{
			if (response.result=="ok" && response.method == "showPermissions") 
			{
				changeStatus();
			}
		}
		
		private function changeStatus():void 
		{			
			//to do
			var stat:String = "Друзья! Есть отличная игра Знаток Слов , вам она понравиться! http://www.odnoklassniki.ru/game/1090124288";
			
			Odnoklassniki.callRestApi("users.setStatus", onStatusAdded, {status: stat}, "JSON", "POST")
			ForticomAPI.removeEventListener(ApiCallbackEvent.CALL_BACK, onStatusPermissionGranted);
		}
		
		private function onStatusAdded(event : Boolean):void
		{
			if (event)
			{
				ServiceHandler.instance.setPublishStatus(this, function (res:*):void{}, Service.instance.playerModel.id);
//				Service.instance.playerModel.currency_soft = Service.instance.playerModel.currency_soft + Service.instance.statusPostModel.rewardContentCount;
				__onCompleteStatusPost.call();
			}
		}
		
		/**ДЛЯ ЛОКАЛЬНОГО СОЕДИНЕНИЯ*/
		protected function onErrorConnection(event:ApiServerEvent ):void
		{
			trace(String(event));
			initServer();
		}
	}
}