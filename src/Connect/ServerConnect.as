package Connect
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.net.URLRequest;
	
	import Connect.Jsons.JSONConnect;
	import Connect.Local.Local_Connection;
	import Connect.OK.OK_Connection;
	import Connect.VK.VK_Connection;
	
	import GameLogic.GameHelpingService.LifeModel;
	import GameLogic.GameHelpingService.ServerTime;
	
	import ServiceGame.ServiceControllers.BonusController;
	import ServiceGame.ServiceControllers.FriendsController;
	import ServiceGame.ServiceControllers.GameDataItemsController;
	import ServiceGame.ServiceControllers.LevelsContainersController;
	import ServiceGame.ServiceControllers.LevelsController;
	import ServiceGame.ServiceControllers.LifeController;
	import ServiceGame.ServiceControllers.LotteryController;
	import ServiceGame.ServiceControllers.PlayerController;
	import ServiceGame.ServiceControllers.PurchaseShopConroller;
	import ServiceGame.ServiceControllers.PurchasesBankController;
	import ServiceGame.ServiceControllers.StatusPostController;
	import ServiceGame.ServiceControllers.WordsController;
	
	import deng.fzip.FZip;
	import deng.fzip.FZipFile;
	
	import hash.Xor64;
	

	public class ServerConnect
	{
		static private var _init:ServerConnect;

		static public function get instance():ServerConnect { return _init; }
		
		static public var SOCIAL					:String = "OK";
//		static private const SOCIAL					:String = "LOCAL";
		static public var SOCIAL_ID					:String;
		static public var TUTORIAL					:Boolean = false;
		static public var IN_GROUP					:Boolean;
		static public var STATIC_PATH				:String;
		public static var REWARD_CONTENT_COUNT		:int;
		public static var TUTORIAL_STEP				:Object;
		public static var SOUND:Boolean;
		public static var DAILY_BONUS_CURRENCY		:int;
		
		public static var DAIYLY_BONUS				:Boolean = true;
		public static var FRIEND_GIFTS				:Boolean = true;
		public static var NEW_FRIEND				:Boolean = true;
		private const APP_URL_GAME_OK				:String  = "";
		private const APP_URL_GAME_VK				:String  = "";
		
		private var __default_sn:String = "OK";
		
		private var __connection					:Connection;
		private var __mainSprite					:Sprite;
		private var __onLoadComplete				:Function;
		private static var zip:FZip;
		private var file:FZipFile;
		
		public static var bonusLevels:Array ;
		private var cacheKey:String = "7458914632";
		private static var zipMedia:Object;
		
		public static var mediaSoundArray:Vector.<FZipFile> 	  = new Vector.<FZipFile>();
		public static var mediaGraphicsArray:Vector.<FZipFile> 	  = new Vector.<FZipFile>();
		public static var mediaGraphicsATFArray:Vector.<FZipFile> = new Vector.<FZipFile>();
		public static var mediaGraphicsXMLArray:Vector.<FZipFile> = new Vector.<FZipFile>();
		private var MEDIA_URL:String;
		private var CACHE_URL:String;
		private var TIME_TO_NEW_LIFE:Number;
		
		public function ServerConnect(onLoadComplete:Function)
		{
			_init = this;
			__onLoadComplete = onLoadComplete;
			setConnection();
		}
		
		private function setConnection():void
		{
			getSocialNet();
			
			switch(SOCIAL)
			{
				case "LOCAL" : 
					this.__connection = new Local_Connection();
					break;
				case "VK" :
					Connection.APP_URL_GAME = APP_URL_GAME_VK;
					this.__connection = new VK_Connection();
					break;
				case "OK" :
					Connection.APP_URL_GAME = APP_URL_GAME_OK;
					this.__connection = new OK_Connection();
					break;
				case "MM" :
					break;
			}
			
			__connection.initConnection();
			__connection.setSocialName();	
		}
		
		private function getSocialNet():void
		{
			// определяет сеть
//			var flash = hide.instance.stage.loaderInfo
//			if( hide.instance.stage.loaderInfo.hasOwnProperty( "parameters") )
//			{
//				if (hide.instance.stage.loaderInfo.parameters.hasOwnProperty("platformType"))
//				{
//					SOCIAL = hide.instance.stage.loaderInfo.parameters["platformType"];
//				} 
//				else SOCIAL = __default_sn;
//			}
			SOCIAL = __default_sn;
		}
		public function get connection():Connection
		{
			return __connection;
		}
		
		/**Информация о игроке*/
		public function userLoad(res:*):void
		{
//			Preloader.instance().loaderMask = 5;
			STATIC_PATH = res.response.appInfo.staticUrl;
			ServerTime.instance.set_time( res.response.appInfo.serverTime);
			CACHE_URL = res.response.appInfo.cacheUrl
			MEDIA_URL = res.response.appInfo.mediaUrl;
			
			loadJSONFile(CACHE_URL);
			
			BonusController.instance.init(res.response.lottery);
			
			LifeController.instance.init(res.response.energy);
			
			TUTORIAL =  res.response.player.isNew;
			TUTORIAL_STEP = res.response.player.tutorial;
			SOUND = res.response.player.options.music;
			
			StatusPostController.instance.init(res.response.bonuses);
			FriendsController.instance.init(res.response.friends);
			PlayerController.instance.init(res.response.player);
			
			var int:Number = res.response.energy.timeLeft;
			
			var int3:Number = ServerTime.TIME;
			if(int > 0 )
			{
				TIME_TO_NEW_LIFE = res.response.life.timeLeft;
				LifeModel.instance.start();
			}
			
//			if (Service.instance.bonusModel.isAvailable && ServerConnect.DAIYLY_BONUS/* && !ServerConnect.TUTORIAL*/) 
//			{
//				service().playerModel.currencySoft -= ServerConnect.DAILY_BONUS_CURRENCY;
//			}
		}
		
		private  function loadMediaJSON(url:String):void
		{
			Preloader.instance().loaderMask = 5;
			var req:URLRequest = new URLRequest(url);  
//			var req:URLRequest = new URLRequest(/*url*/"http://app1.greemlins.com/word-master/uploads/media/media.zip"+"?"+ num.toString() );  
			
			zipMedia = new FZip();
			zipMedia.addEventListener(Event.COMPLETE, loadCompleteMedia);
			zipMedia.load(req);
		}
		
		protected  function loadCompleteMedia(event:Event):void
		{
			Preloader.instance().loaderMask = 20;
			
			var len = zipMedia.getFileCount();
			var fileMedia:FZipFile;
			var index:int;
			checkForSound();
			
			
			function checkForSound():void
			{
				for (var i:int = 0; i < len; i++) 
				{
					fileMedia = zipMedia.getFileAt(i);
					index = fileMedia.filename.indexOf(".mp3");
					if( index != -1) mediaSoundArray.push(fileMedia);
				}
//				SoundGameManager.loadSoundsMedia();
			}
			
			loadJSONFile(CACHE_URL);
		}
		
		private function loadJSONFile(url:String):void
		{
//			Preloader.instance().loaderMask = 24;
			
			var num:int = Math.random()*int.MAX_VALUE;
			url += "?";
			url += String(num);
			var req:URLRequest = new URLRequest( url ); 
			zip = new FZip();
			zip.addEventListener(Event.COMPLETE, loadComplete);
			zip.load(req);
		}
		
		protected function loadComplete(event:Event):void
		{
			file = zip.getFileAt(0);
			var content = file.getContentAsString();     
			var decode = Xor64.decode(content, cacheKey);
			var res:Object = { "response" : JSONConnect.decode(decode)};
			
			GameDataItemsController.instance.init(res.response.items);
			
			LevelsController.instance.init(res.response.levels);
			WordsController.instance.init(res.response.words);
			
			LotteryController.instance.init(res.response.lottery.objects);
			
			PurchasesBankController.instance.init(res.response.purchases.bank);
			PurchaseShopConroller.instance.init(res.response.purchases.shop);
			
			__onLoadComplete.call();
		}
	}
}