/*	КЛАСС СОДЕРЖАЩИЙ В СЕБЕ ФУНКЦИИ ДЛЯ
	ВЫПОЛНЕНИЯ НА СТОРОНЕ СЕРВЕРА */

package Connect
{
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	
	import Connect.Jsons.JSON2;
	import Connect.Local.URLConnector;
	import Connect.OK.adobe.crypto.MD5;
	
	

	public class ServiceHandler
	{
		static private var _init:ServiceHandler;
		static public function get instance():ServiceHandler { return _init; }
		
		private var _gateway:String;
		private var _nc:URLConnector;
		private var isConnect:Boolean = false;
		private var __connectionStack:Array = new Array;
		
		public function ServiceHandler(netConnection:URLConnector, gateway:String)
		{
			_init = this;
			this._nc = netConnection;
			this._gateway = gateway;
		}
		
		/**НАЧАЛО НОВОГО УРОВНЯ*/
		public function startLevel(target:*, fun:Function, levelID:String):void
		{ 
			var params:Object = {
				"player":{
					"id":service().playerModel.id
				}, 
				"level":{
					"id":levelID
				}
			}; 
			netConnection(_gateway + "level/start", params, target, fun) 
		}
		
		/**КОНЕЦ УРОВНЯ*/
		public function endLevel(target:*, fun:Function, LEVEL_ID:String, isWin:Boolean):void
		{ 
			var params:Object = {
				"player":{
					"id": service().playerModel.id
				}, 
				"level":{
					"id":LEVEL_ID,
					"isWin" : isWin
				}
			};
			netConnection(_gateway + "level/end", params, target, fun) 
		}
		
		/**СОХРАНЕНИЕ УРОВНЯ*/
		public function saveLevel(target:*, fun:Function, levelID:String, progress:Array):void
		{ 
			var params:Object = {
				"player":{
					"id": service().playerModel.id
				}, 
				"level":{
					"id":levelID,
					"progress" : progress
				},
				"session":service().playerModel.session
			};
			netConnection(_gateway + "level/save", params, target, fun) 
		}
		
		/**СОХРАНЕНИЕ ТУТОРИАЛА*/
		public function tutorialSave(target:*, fun:Function, step:Object):void
		{ 
			var params:Object = {
				"player":{
					"id": service().playerModel.id
						
				},
				"tutorial":step
			};
			netConnection(_gateway + "tutorial/set", params, target, fun) 
		}
		
		
		/**TOP PAGE*/
		public function topPage(target:*, fun:Function, socialID:String, limit:int, offSet:int):void
		{
			var params:Object = {
				"player":{
					"id":service().playerModel.id
				}, 
				"limit":limit,
				"offset":offSet,
				"session":service().playerModel.session
			};
			netConnection(_gateway + "player/top", params, target, fun)
		}
		
		
		
		/**MY PLACE*/
		public function playerInfo(target:*, fun:Function):void
		{
			var params:Object = {
				"player":{
					"id":service().playerModel.id
				}, 
				"session":service().playerModel.session
			}
			netConnection(_gateway + "player/info", params, target, fun)
		}
		
		/**MY MUSIC*/
		public function music(target:*, music:Boolean):void
		{
			var params:Object = {
				"player":{
					"id":service().playerModel.id
				}, 
				"option" : {
						"name" : "music",
						"value" : music
				}, 	
				"session":service().playerModel.session
			}
			netConnection(_gateway + "option/set", params, target)
		}
		
		/**SET STATUS POST*/
		public function setPublishStatus(target:*, fun:Function, playerID:String):void
		{
			var params:Object = {
				"player":{
					"id":service().playerModel.id
				}, 
				"session":service().playerModel.session
			}
			netConnection(_gateway + "bonus/status", params, target, fun)
		}
		
		/**ITEM USE*/
		public function itemUse(target:*, fun:Function, item:String):void
		{
			var params:Object = {
				"player":{
					"id":service().playerModel.id
				}, 
				"item":{
					"id":item
				}
			}
			netConnection(_gateway + "item/use", params, target, fun)
		}
		/**Lottery USE*/
		public function lotterySpin(target:*, fun:Function):void
		{
			var params:Object = {
				"player":{
					"id":service().playerModel.id
				}
			}
			netConnection(_gateway + "lottery/spin", params, target, fun)
		}
		
		/**СДЕЛАТЬ ПОКУПКУ*/
		public function shopPurchase(target:*, fun:Function, item:String):void
		{
			var params:Object = {
				"player":{
					"id":service().playerModel.id
				}, 
				"purchase":{
					"id":item
				}, 
				"session":service().playerModel.session
			}
			
			netConnection(_gateway + "purchase/make-purchase-shop", params, target, fun)
		}	
		
		/**SEND PRIZE*/
		public function sendPrize(target:*, fun:Function, friends_ids:String):void
		{
			var params:Object = {
				"player":{
					"id":service().playerModel.id
				}, 
				"friend":{
					"id":friends_ids
				}, 
				"session":service().playerModel.session
			};
			netConnection(_gateway + "gift/send", params, target, fun)
		}
		
		/**SET STATUS GROUP*/
		public function setPublishGroup(target:*, fun:Function, playerID:String):void
		{
			var param:Object = { "player_id"    : playerID }
			_nc.send(_gateway + "player/SetStatusGroup", onResult, onFault , param); 
			function onResult(result:Object):void { fun.call(target, result); }
		}
		
		
		
		protected function onFault(fault:Object):void { trace(String(fault.description));	}
		
		public function netConnection(resource:String, params:*, target:* = null, fun:Function = null):void
		{
			if( !isConnect )
			{
				sendRequest(resource, params, target, fun);
			}
			else
			{
				var connectInfo:Object = new Object();
				connectInfo.resource = resource;
				connectInfo.params = params;
				connectInfo.target = target;
				connectInfo.fun = fun;
				
				__connectionStack.push( connectInfo );
			}  
		}
		
		private function sendRequest(resource:String, params:*, target:* = null, fun:Function = null):void
		{
			isConnect = true;
			
			var hdr:URLRequestHeader = new URLRequestHeader("Content-type", "application/json");
			var request:URLRequest = new URLRequest(resource);
			
			params["player"]["platformType"] = Connection.SOCIAL_NAME;
			params["session"] = service().playerModel.session;
			
			var str:String = JSON2.serialize(params);
//			trace(str);
			var salt:String = 'FCAYcd50f0Yt';
			var hash:String = MD5.hash(str + salt);
//			trace(hash);
			
//			params["hash"] = hash;
			
			str = JSON2.serialize(params);
			trace(str);
			request.data = str;
			request.method = URLRequestMethod.POST;
			request.requestHeaders.push(hdr);
			
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, onResult);
			loader.addEventListener(IOErrorEvent.IO_ERROR, onError);
			
			try 
			{
				loader.load(request);
			} 
			catch(e:Error)
			{
				trace("Unable to load requested document.");
			}
			
			function onError(e:Event):void 
			{
				trace("Unable to load requested document.");
//				WindowsManager.hide(function():void{WindowsManager.show( InetError)});
				//ErrorConnection({status: 'error', response: {code: '0x111'}})
			}
			
			function onResult(e:Event):void 
			{      
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var _data:* = JSON2.deserialize(e.currentTarget.data);
				
				if(fun) fun.call(target, _data);
				
				isConnect = false;
				
				if(__connectionStack.length)
				{
					var connectInfo:Object = __connectionStack[0];
					sendRequest(connectInfo.resource, connectInfo.params, connectInfo.target, connectInfo.fun);
					__connectionStack.shift();
				}
			}  
		}
	}			
}