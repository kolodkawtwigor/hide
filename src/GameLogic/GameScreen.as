package GameLogic
{
	
	import GameLogic.Controllers_$_Views.InGame.GameView;
	import GameLogic.Models.GameModel;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import GameLogic.Controllers_$_Views.InGame.BotGamePanel;
	import GameLogic.Controllers_$_Views.InGame.GameElements;
	import GameLogic.Controllers_$_Views.InGame.TopGamePanel;

	public class GameScreen extends Sprite
	{
		private var _gameView:GameView;
		private var _gameModel:GameModel;
		static private var _init:GameScreen;
		private var _botPanel:BotGamePanel;
		private var _gameElements:GameElements;
		private var _topPanel:TopGamePanel;
		static public function get instance():GameScreen { return _init; }
		
		public function GameScreen()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			_init = this;
			
			_gameModel = new GameModel();
			
			_gameElements = new GameElements();
			addChild(_gameElements); 
			
			_botPanel = new BotGamePanel();
			addChild(_botPanel); 
			
			_gameView = new GameView(_botPanel);
			addChild(_gameView); 
			
			
			_botPanel.parent.setChildIndex(_botPanel, parent.numChildren+1 );
			
			_topPanel = new TopGamePanel();
			addChild(_topPanel); 
		}
		
		private function destroy(e:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}