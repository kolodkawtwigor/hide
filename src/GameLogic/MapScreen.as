package GameLogic
{
	
	import GameLogic.Controllers_$_Views.InMap.LevelMap;
	import GameLogic.Controllers_$_Views.InMap.MapTopPanel;
	
	import GraphicsArchitecture.MImage;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	import GameLogic.Controllers_$_Views.InMap.FriendsPanel;

	public class MapScreen extends Sprite
	{
		private var _background:MImage;
		private var __levelMap:LevelMap;
		private var __mapTopPanel:MapTopPanel;
		private var __friendsPanel:FriendsPanel;
		
		public function MapScreen()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			var texture:Texture = MAssetsManager.assetManager.getTexture("game_sceen_BG");
			_background = new MImage(texture); 
			addChild(_background);
			
			__levelMap = new LevelMap();
			addChild(__levelMap);
			
			__mapTopPanel = new MapTopPanel();
			addChild(__mapTopPanel);
			
			__friendsPanel = new FriendsPanel();
			addChild(__friendsPanel); __friendsPanel.y = 600;
			
			onResize(null);
		}
		
		private function onResize(e:Event = null):void
		{
			
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
	}
}