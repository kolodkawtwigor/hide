package GameLogic.Timer
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import starling.events.Event;
	import starling.events.EventDispatcher;
	import Events.GameModelEvents;
	import GameLogic.GameHelpingService.TimeConverter;

	public class GameTimeModel extends EventDispatcher
	{
		private var _timeOut:int;
		public var _gameTimer:Timer;
		private var _timeConverter:TimeConverter;
		private var _gameTimeString:String;
		
		static private var _init:GameTimeModel;
		static public function get instance():GameTimeModel { return _init; }
		
		public function GameTimeModel()
		{
			_init = this;
			createGameTimer();
		}
		
		private function createGameTimer():void
		{
			_timeOut = 91;
			_gameTimer = new Timer(1000);
			_gameTimer.addEventListener(TimerEvent.TIMER, onTimerTick);
			_gameTimer.start();
			_timeConverter = new TimeConverter(_timeOut);
			onTimerTick(null);
		}
		
		protected function onTimerTick(e:TimerEvent):void
		{
			_timeOut --;
			if (_timeOut < 0)
			{
				_timeOut = 0;
				dispatchEvent(new Event(GameModelEvents.TIME_LEFT));
			} 
			dispatch();
		}
		
		private function dispatch():void
		{
			_timeConverter.value = _timeOut;
			_gameTimeString = _timeConverter.minute + ":" + _timeConverter.second;
			dispatchEvent(new Event(GameModelEvents.TICK_GAME_TIMER, false, _gameTimeString));
		}
	}
}