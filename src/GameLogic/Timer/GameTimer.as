package GameLogic.Timer
{
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import Events.GameModelEvents;
	import GraphicsArchitecture.MImage;
	
	public class GameTimer extends Sprite
	{
		private var _timerSubstrate:MImage;
		private var _timerTextField:TextField;
		
		public function GameTimer()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			var gameTimeModel:GameTimeModel = new GameTimeModel();
			
			gameTimeModel.addEventListener(GameModelEvents.TICK_GAME_TIMER, updateGameTimer);
			crateBackground();
			crateTimerTextField();
		}
		
		private function updateGameTimer(e:Event):void
		{
			_timerTextField.text = String(e.data);
		}
		
		private function crateTimerTextField():void
		{
			_timerTextField = new TextField(this.width, this.height, "01:30", "Verdana", 25, 0x0, true);
//			_timerTextField.border = true;
			_timerTextField.alignPivot();
			addChild(_timerTextField);
		}
		
		private function crateBackground():void
		{
//			_timerSubstrate = new MImage(MAssetsManager.TimeSubstrate);
//			_timerSubstrate.alignPivot();
//			addChild(_timerSubstrate);
		}
		
		private function destroy(e:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}