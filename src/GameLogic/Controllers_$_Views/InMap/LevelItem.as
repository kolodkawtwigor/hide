package GameLogic.Controllers_$_Views.InMap
{
	import Connect.ServerConnect;
	import Connect.ServiceHandler;
	import Connect.service;
	
	import GameLogic.GameScreen;
	import GameLogic.GameHelpingService.ImageLoader;
	import GameLogic.GameHelpingService.ScreenManager;
	import GameLogic.GameHelpingService.WindowsManager;
	
	import GraphicsArchitecture.MImage;
	
	import ServiceGame.ServiceModels.LevelsContainersModel;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	import GraphicsArchitecture.ButtonsElements.MapScreenButtons.LevelButton;
	
	public class LevelItem extends Sprite
	{
		private var __position:int;
		private var __level:LevelsContainersModel;
		private var __isOpenedLevel:Boolean;
		private var __podlogka:MImage;
		private var __backing:LevelButton;
		public var __mc_picture:Sprite;
		public function LevelItem( /*level:LevelsContainersModel,*/position:int, isOpenedLevel:Boolean)
		{
			__position = position;
//			__level = level;
			__isOpenedLevel = isOpenedLevel
				
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			createImage();
		}
		
		private function createImage():void
		{
			__backing = new LevelButton(onClickLevel, __position);
			addChild(__backing);
			if(__isOpenedLevel)
			{
				this.touchable = true;
				this.useHandCursor = true;
			}
			else 
			{
				var texture:Texture = MAssetsManager.assetManager.getTexture("");
				var __blockIco:MImage = new MImage(texture); 
				addChild(__blockIco); __blockIco.alignPivot();
				this.touchable = false;
			}
		}
		
		private function onClickLevel():void
		{
			ScreenManager.instance.show(GameScreen);
			return;
//			LevelMap.instance().curLevel = __level;
//			LevelMap.instance().curLevelInt = __position;
//			ScreenManager.show(GameScreen);
			
//			ScreenManager.instance.show(MapScreen);
//			return;
			
			StatusStage.deactive();
			if(service().playerModel.lifeCount == 0 )
			{
//				WindowsManager.show( NoLife );
				return;
			}
			ServiceHandler.instance.startLevel(this, startLevel, __level.id);
		}
		
		private function startLevel(res:*):void
		{
			LevelMap.instance().curLevel = __level;
			LevelMap.instance().curLevelInt = __position;
			
			
			for (var i:int = 0; i < 1; i++) 
			{
				ScreenManager.instance.show(GameScreen);
			}
			
			
			
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
			
		}
	}
}