package GameLogic.Controllers_$_Views.InMap
{
	import flash.text.TextFormatAlign;
	
	import Connect.ServerConnect;
	import Connect.ServiceHandler;
	import Connect.service;
	
	import Events.ServiceEvent;
	import Events.TimeEvents;
	
	import GameLogic.GameHelpingService.LifeModel;
	import GameLogic.GameHelpingService.MTextField;
	import GameLogic.GameHelpingService.TimeConverter;
	
	import GraphicsArchitecture.MImage;
	import GraphicsArchitecture.ButtonsElements.MapScreenButtons.BonusBtn;
	import GraphicsArchitecture.ButtonsElements.MapScreenButtons.TopBtn;
	import GraphicsArchitecture.ButtonsElements.SystemButtons.AddCoin;
	import GraphicsArchitecture.ButtonsElements.SystemButtons.HelpBtn;
	import GraphicsArchitecture.ButtonsElements.SystemButtons.SoundBtn;
	
	import ServiceGame.Service;
	import ServiceGame.ServiceModels.PlayerModel;
	
	import Supporting.WindowsManager;
	
	import Windows.RuleWindow;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class MapTopPanel extends Sprite
	{
		private var _lifeSubstrate:MImage;
		private var _coinSubstrate:MImage;
		private var _coinSubstrate_ITM:MImage;
//		private var life_btn:AddCoinBtn;
		private var help_btn:HelpBtn;
//		private var coin_btn:AddCoinBtn;
		private var _lifeSubstrate_ITM:MImage;
		private var __playerModel:PlayerModel = Service.instance.playerModel;
		private var _tcLife:TimeConverter;
		private var __timeTF:MTextField;
		private var __coinTF:MTextField;
		private var __curentCoins:int 		= service().playerModel.currencySoft;
		private var __lifeTF:MTextField;
		private var _energySubstrate:MImage;
		private var _energySubstrate_ITM:MImage;
		private var _timerSubstrate:MImage;
		private var enegy_btn:AddCoin;
		private var coin_btn:AddCoin;
		private var sound_btn_on:SoundBtn;
		private var sound_btn_off:SoundBtn;
		private var bonus_btn:BonusBtn;
		private var top_btn:TopBtn;
		
		public function MapTopPanel()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			
			
			createBackground();
			createButtons();
			checkForSound();
//			createBonusTimer();
//			createTF();
//			addListeners();
			
			onResize(null);
		}
		
		private function createTF():void
		{
//			__coinTF = new MTextField(120, 50, __curentCoins.toString(), 0,0,MAssetsManager.CANDARA_BOLD, 27, 0xFFFFFF);
//			addChild(__coinTF);  __coinTF.touchable = false;__coinTF.alignPivot();
//			__coinTF.hAlign = TextFormatAlign.CENTER;
//			
//			var text:String = String(__playerModel.lifeCount) + "/" + String(__playerModel.lifeMax)
//			__lifeTF = new MTextField(120, 50, text, 0,0,MAssetsManager.CANDARA_BOLD, 27, 0xFFFFFF);
//			addChild(__lifeTF);  __lifeTF.touchable = false;__lifeTF.alignPivot();
//			__lifeTF.hAlign = TextFormatAlign.CENTER;
		}
		
		private function createBonusTimer():void
		{
//			_tcLife = new TimeConverter( LifeModel.instance.timeLeft );
//			__timeTF = new MTextField(100, 30, "", 0,0,MAssetsManager.CANDARA_BOLD, 20, 0xFFFFFF);
//			
//			
//			__timeTF.alignPivot();
//			__timeTF.batchable = true; 
//			__timeTF.kerning = true; 
//			addChild(__timeTF);
		}
		
		private function createButtons():void
		{ 
			enegy_btn = new AddCoin(openShopShop);
			addChild(enegy_btn);   enegy_btn.alignPivot();
			
			coin_btn = new AddCoin(openBankShop);
			addChild(coin_btn);   coin_btn.alignPivot();
			
			sound_btn_on = new SoundBtn(controlSound, true);
			addChild(sound_btn_on); sound_btn_on.alignPivot();
			
			sound_btn_off = new SoundBtn(controlSound,false);
			addChild(sound_btn_off); sound_btn_off.alignPivot();
			
			help_btn = new HelpBtn(openHelpWindow);
			addChild(help_btn); 
			
			bonus_btn = new BonusBtn(openBonusWindow);
			addChild(bonus_btn); 
			
			top_btn = new TopBtn(openTopWindow);
			addChild(top_btn);  	
			
			
			
		}
		
		private function openTopWindow():void
		{
			// TODO Auto Generated method stub
			
		}
		
		private function openBonusWindow():void
		{
			// TODO Auto Generated method stub
			
		}
		
		private function onResize(e:Event = null):void
		{
			_energySubstrate.x = (InitializationStarling._stage.stageWidth>>1) -135; _energySubstrate.y = 63;
			_energySubstrate_ITM.x = _energySubstrate.x - (_energySubstrate.width>>1) + 5; _energySubstrate_ITM.y = 63;
			_timerSubstrate.x = _energySubstrate.x; _timerSubstrate.y = _energySubstrate.y + 42;
			_coinSubstrate_ITM.x = (InitializationStarling._stage.stageWidth>>1) + 130; _coinSubstrate_ITM.y = 58;
			
			enegy_btn.x = _energySubstrate.x + (_energySubstrate.width>>1) ; enegy_btn.y = _energySubstrate.y + 6;
			coin_btn.x = _coinSubstrate_ITM.x + (_energySubstrate.width>>1) ; coin_btn.y = _coinSubstrate_ITM.y + 10;
			
			help_btn.x = InitializationStarling._stage.stageWidth - 60; help_btn.y = 50;
			sound_btn_on.x = InitializationStarling._stage.stageWidth - 60; sound_btn_on.y = 120;
			sound_btn_off.x = InitializationStarling._stage.stageWidth - 60; sound_btn_off.y = 120;
			
			bonus_btn.x = 120; bonus_btn.y = 90;
			top_btn.x = 120; top_btn.y = 210;
		}
		
		private function controlSound():void
		{
			if (!ServerConnect.SOUND)
			{
				active();
				//				SoundGameManager.playMusic(SoundGameManager.MAP_SCREEN_SOUND, 1);
//				SoundGameManager.mute(SoundGameManager.FX, false);
//				SoundGameManager.mute(SoundGameManager.MUSIC, false);
				ServerConnect.SOUND = true;
//				ServiceHandler.instance.music(this, true);
			}
			else
			{
				deactive();
//				SoundGameManager.mute(SoundGameManager.FX, true);
//				SoundGameManager.mute(SoundGameManager.MUSIC, true);
				ServerConnect.SOUND = false;
//				ServiceHandler.instance.music(this, false);
			}
		}
		
		private function active():void
		{
			sound_btn_on.visible = true;
			sound_btn_off.visible = false;
		}
		
		private function deactive():void
		{
			sound_btn_on.visible = false;
			sound_btn_off.visible = true;
		}
		
		private function openBankShop():void
		{
//			WindowsManager.show( CoinBank );
		}
		
		private function openHelpWindow():void
		{
			WindowsManager.show( RuleWindow );
		}
		
		private function openShopShop():void
		{
//			WindowsManager.show( LifeBank );
		}
		
		private function createBackground():void
		{
			_energySubstrate = new MImage(MAssetsManager.assetManager.getTexture("coin_substrate_2"));
			addChild(_energySubstrate); _energySubstrate.alignPivot();
			
			_energySubstrate_ITM = new MImage(MAssetsManager.assetManager.getTexture("coin_ITM"));
			addChild(_energySubstrate_ITM); _energySubstrate_ITM.alignPivot();
			
			_timerSubstrate = new MImage(MAssetsManager.assetManager.getTexture("timer_substrate"));
			addChild(_timerSubstrate); _timerSubstrate.alignPivot();
		
			_coinSubstrate_ITM = new MImage(MAssetsManager.assetManager.getTexture("coin_substrate"));
			addChild(_coinSubstrate_ITM); _coinSubstrate_ITM.alignPivot();
		}
		
		private function addListeners():void
		{
			__playerModel.addEventListener(ServiceEvent.CURRENCY_SOFT, updateCoinBar);
			__playerModel.addEventListener(ServiceEvent.LIFE_COUNT, updateLifeCount);
			LifeModel.instance.addEventListener(TimeEvents.UPDATE_LIFE_TIME, updateLifeTimer);
		}
		
		private function updateLifeCount(event:Event):void
		{
			var text:String = String(__playerModel.lifeCount) + "/" + String(__playerModel.lifeMax)
			__lifeTF.text = text;
		}
		
		protected function updateCoinBar(event:Event):void
		{
			__coinTF.text = __playerModel.currencySoft.toString();
		}
		
		protected function updateLifeTimer(event:Event):void
		{
			//			TODO часы
			if(_tcLife)
			{
				_tcLife.value = LifeModel.instance.timeLeft;
				__timeTF.text = _tcLife.minute  + ":" + _tcLife.second;
			}
		}
		
		private function checkForSound():void
		{
			if(ServerConnect.SOUND)
			{
				sound_btn_on.visible = true;
				sound_btn_off.visible = false;
			}
			else
			{
				sound_btn_on.visible = false;
				sound_btn_off.visible = true;
			}
		}
		
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
			
		}
	}
}