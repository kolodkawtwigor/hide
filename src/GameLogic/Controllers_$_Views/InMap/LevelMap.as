package GameLogic.Controllers_$_Views.InMap
{
	import flash.geom.Point;
	
	import Connect.service;
	import ServiceGame.ServiceModels.LevelsContainersModel;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class LevelMap extends Sprite
	{
		static private var _init:LevelMap;
		static public function instance():LevelMap { return _init; }
		private var totalPage:int;
		public var offset:int = 8;
		public var countPage:int = 0;
		public var currentPage:int = 1;
		private var __pointsStorage:Vector.<Point>;
		public var levelsModelContainer:Vector.<LevelsContainersModel>;
		public var __levelItemStorage:Vector.<LevelItem> ;
//		private var btn_next:PageArrowBtn;
//		private var btn_previous:PageArrowBtn;
		private var currentLvl:int = service().playerModel.points;
		public var curLevel:LevelsContainersModel;
		public var curLevelInt:int;
		
		public function LevelMap()
		{
			super();
			_init = this;
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			levelsModelContainer = service().levelsContainerModelContainer;
			
			createBt();
			getTotalPage();
			
			createItems();
//			if (currentPage == 1) btn_previous.visible = false;
//			else btn_previous.visible = true;
//			if(currentPage == totalPage )btn_next.visible = false;
			
			
			
			
			
			onResize(null)
			stage.addEventListener(Event.RESIZE, onResize);
		}
		
		private function onResize(e:Event = null):void
		{
//			btn_previous.x = (InitializationStarling._stage.stage.stageWidth>>1) - 290;		 btn_previous.y = (InitializationStarling._stage.stage.stageHeight>>1);
//			btn_next.x = (InitializationStarling._stage.stage.stageWidth>>1) + 290; btn_next.y = (InitializationStarling._stage.stage.stageHeight>>1);
			
			
			createPointsStorage();
			for (var i:int = 0; i < __levelItemStorage.length; i++) 
			{
				__levelItemStorage[i].x = __pointsStorage[i].x;
				__levelItemStorage[i].y = __pointsStorage[i].y;
				addChild(__levelItemStorage[i]);
			}
		}
		
		private function previousPage():void
		{
//			countPage--;
//			currentPage--;
//			removeItems();
//			if (currentPage == 1) 
//			{
//				btn_previous.visible = false;
//			}
//			if (currentPage != totalPage) btn_next.visible = true;
//			createItems();
//			
//			addItems();
		}
		
		public function removeItems():void
		{
			for (var i:int = 0; i < __levelItemStorage.length; i++)  
			{
				removeChild(__levelItemStorage[i],true);
			}
		}
		
		private function nextPage():void
		{
//			countPage++;
//			currentPage++;
//			removeItems();
//			if (currentPage != 1) btn_previous.visible = true;
//			
//			if (currentPage == totalPage) btn_next.visible = false;
//			createItems();
//			addItems();
		}
		
		
		private function addItems():void
		{
			for (var i:int = 0; i < __levelItemStorage.length; i++) 
			{
				__levelItemStorage[i].x = __pointsStorage[i].x;
				__levelItemStorage[i].y = __pointsStorage[i].y;
				addChild(__levelItemStorage[i]);
			}	
		}
		
		private function createItems():void
		{
			__levelItemStorage = new Vector.<LevelItem>()
			var levelItem:LevelItem;
//			for (var j:int = countPage*offset; j < currentPage*offset && j < levelsModelContainer.length; j++) 
//			{
//				
//				if(j <= currentLvl ) levelItem = new LevelItem(levelsModelContainer[j], j+1 , true);
//				else levelItem = new LevelItem(levelsModelContainer[j], j+1, false);
//				levelItem.alignPivot();
//				__levelItemStorage.push(levelItem);
//			}
			
			for (var j:int = countPage*offset; j < 1; j++) 
			{
				
				levelItem = new LevelItem(/*levelsModelContainer[j],*/ j+1 , true);
				levelItem.alignPivot();
				__levelItemStorage.push(levelItem);
			}
		}
		
		private function createPointsStorage():void
		{
			__pointsStorage = new Vector.<Point>();
			var point:Point
			for (var i:int = 0; i < 2; i++) 
			{
				for (var j:int = 0; j < 4; j++) 
				{
					point = new Point();
					point.x = 300 + j*200;
					point.y = 255 + i*185;
					__pointsStorage.push(point);
				}
			}
			
			__pointsStorage.push(createPoint( ((InitializationStarling._stage.stageWidth>>1)-0), (InitializationStarling._stage.stageHeight>>1)));
		}
		
		private function getTotalPage():void
		{
			totalPage = Math.ceil(levelsModelContainer.length/offset);
			currentPage = Math.ceil((currentLvl + 1) / offset);
			if(currentPage > totalPage && totalPage != 0)  currentPage = totalPage
			countPage = currentPage  - 1;
		}
		
		private function createBt():void
		{
//			btn_next = new PageArrowBtn(nextPage);
//			addChild(btn_next); 
//			
//			
//			btn_next.rotation = deg2rad(180);
//			
//			btn_previous = new PageArrowBtn(previousPage);
//			addChild(btn_previous); 
		}
		
		public function createPoint(x:Number, y:Number):Point
		{
			var point:Point = new Point();
			point.x = x; point.y = y;
			return point;
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
			
		}
	}
}