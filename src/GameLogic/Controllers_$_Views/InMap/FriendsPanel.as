package GameLogic.Controllers_$_Views.InMap
{
	
	import GraphicsArchitecture.MImage;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class FriendsPanel extends Sprite
	{
		private var _background:MImage;
		private var addFriend_btn:AddFriendBtn;
		public function FriendsPanel()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			createButton();
		}
		
		private function createButton():void
		{
			var texture:Texture = MAssetsManager.assetManager.getTexture("add_friends_substrate");
			_background = new MImage(texture);  _background.alignPivot();
			addChild(_background); _background.x = InitializationStarling._stage.stageWidth - 100;
//			 _background.y = InitializationStarling._stage.stageHeight - 145;
			 _background.y = 25;
			 
			 addFriend_btn = new AddFriendBtn(inviteAllFriends);
			 addChild(addFriend_btn);   addFriend_btn.alignPivot();
			 addFriend_btn.x = _background.x;
			 addFriend_btn.y = _background.y;
		}
		
		private function inviteAllFriends():void
		{
//			var text:String = "Очень увлекательная игра! Заходи, будем играть вместе!"
//			var params:String = "";
//			var selected_uids:String = "";
//			currentConnection().inviteFriends(text, params, null, null );
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}