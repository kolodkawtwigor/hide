package GameLogic.Controllers_$_Views.InMap
{
	import Connect.ServerConnect;
	import Connect.ServiceHandler;
	
	import GraphicsArchitecture.ButtonsElements.MapScreenButtons.FriendsWindowBtn;
	import GraphicsArchitecture.ButtonsElements.MapScreenButtons.OwlCollectionBtn;
	import GraphicsArchitecture.ButtonsElements.MapScreenButtons.TopBtn;
	import GraphicsArchitecture.ButtonsElements.SystemButtons.SoundBtn;
	
	import GameLogic.GameHelpingService.WindowsManager;
	
	import Windows.FriendsList;
	import Windows.OwlClollectionWindow;
	import Windows.Toplist;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class MapBotPanel extends Sprite
	{
		private var owl_btn:OwlCollectionBtn;
		private var friend_btn:FriendsWindowBtn;
		private var top_btn:TopBtn;
		private var sound_btn:SoundBtn;
		private var sound_btn_on:SoundBtn;
		private var sound_btn_off:SoundBtn;
		public function MapBotPanel()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			createButtons();
			checkForSound();
			onResize(null);
			stage.addEventListener(Event.RESIZE, onResize);
		}
		
		private function checkForSound():void
		{
			if(ServerConnect.SOUND)
			{
				sound_btn_on.visible = true;
				sound_btn_off.visible = false;
			}
			else
			{
				sound_btn_on.visible = false;
				sound_btn_off.visible = true;
			}
		}
		
		private function onResize(e:Event = null):void
		{
//			_levelSubstrate.x = (InitializationStarling._stage.stageWidth>>1) -125; _levelSubstrate.y = 50;
			
			owl_btn.x = (InitializationStarling._stage.stageWidth>>1) + 70;
			owl_btn.y = InitializationStarling._stage.stageHeight - 30;
			
			top_btn.x = owl_btn.x + 90;
			top_btn.y = owl_btn.y;
			
			friend_btn.x = top_btn.x + 90;
			friend_btn.y = owl_btn.y;
			
			
			sound_btn_on.x = friend_btn.x + 69;
			sound_btn_on.y = owl_btn.y;
			sound_btn_off.x = friend_btn.x + 69;
			sound_btn_off.y = owl_btn.y;
		}
		
		private function createButtons():void
		{  
			sound_btn_on = new SoundBtn(controlSound, true);
			addChild(sound_btn_on); sound_btn_on.alignPivot();
			
			sound_btn_off = new SoundBtn(controlSound,false);
			addChild(sound_btn_off); sound_btn_off.alignPivot();
			
			owl_btn = new OwlCollectionBtn(openOwlCollectionWindow);
			addChild(owl_btn);  owl_btn.alignPivot();
			
			friend_btn = new FriendsWindowBtn(openFriendsWindow );
			addChild(friend_btn);  friend_btn.alignPivot();
			
			top_btn = new TopBtn(openTopWindow );
			addChild(top_btn);  top_btn.alignPivot();
		}
		
		private function controlSound():void
		{
			if (!ServerConnect.SOUND)
			{
				active();
//				SoundGameManager.playMusic(SoundGameManager.MAP_SCREEN_SOUND, 1);
//				SoundGameManager.mute(SoundGameManager.FX, false);
//				SoundGameManager.mute(SoundGameManager.MUSIC, false);
				ServerConnect.SOUND = true;
				ServiceHandler.instance.music(this, true);
			}
			else
			{
				deactive();
//				SoundGameManager.mute(SoundGameManager.FX, true);
//				SoundGameManager.mute(SoundGameManager.MUSIC, true);
				ServerConnect.SOUND = false;
				ServiceHandler.instance.music(this, false);
			}
		}
		
		private function active():void
		{
			sound_btn_on.visible = true;
			sound_btn_off.visible = false;
		}
		
		private function deactive():void
		{
			sound_btn_on.visible = false;
			sound_btn_off.visible = true;
		}
		
		private function openTopWindow():void
		{
			WindowsManager.show( Toplist );
		}
		
		private function openFriendsWindow():void
		{
			WindowsManager.show( FriendsList);
		}
		
		private function openOwlCollectionWindow():void
		{
			WindowsManager.show( OwlClollectionWindow );
		}
		
		private function openHelpWindow():void
		{
			// TODO Auto Generated method stub
			
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
			

		}
	}
}