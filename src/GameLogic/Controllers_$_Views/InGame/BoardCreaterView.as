package board 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;

	public class BoardCreaterView extends Sprite
	{
		static public var INTERVAL:Number = 60;
		public var ROW:int = 9;
		public var COL:int = 9;
		private const __diffrentCount:int = 6;
		public var grid_data:Vector.<Vector.<Piece>>;
		private var __data:Array;
		private var __boardCreaterModel:BoardCreaterModel;
		
		public function BoardCreaterView(boardCreaterModel:BoardCreaterModel)
		{
			__boardCreaterModel = boardCreaterModel;
			super();
			
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			createBoard();
		}
		
		public function createBoard():void
		{
			
		this.__data = __boardCreaterModel.__data;
		
			var __border:frame_GUI;
			var __cell:Cell_GUI;
			var __angle:frame2_GUI;
			
			for (var i:int = 0; i < __data.length; i++) 
			{
				for (var j:int = 0; j < __data[i].length; j++)
				{
					if(__data[i][j] == null) continue;
					if((__data[i][j]==1 || __data[i][j]==2)) createCell();
					
//					if( isNull(__data[i - 1]) || isNull(__data[i - 1][j]))createBorder(90);
//					/*bot*/ 
//					if( isNull(__data[i + 1]) || isNull(__data[i + 1][j]) ) createBorder(-90);
//					/*left*/
//					if(  isNull(__data[i][j - 1]))createBorder(0);
//					/*right*/
//					if(isNull(__data[i][j + 1]))createBorder(-180);
//					//Боковые рамки
					
//					if( (__data[i][j]==1 || __data[i][j]==2)   && isNull(__data[i][j - 1])  && isNull(__data[i - 1][j]))createAngle(0);
//					if( (__data[i][j]==1 || __data[i][j]==2), isNull(__data[i-1][j])  && isNull(__data[i][j + 1]))createAngle(90);
//					
//					if( (__data[i][j]==1 || __data[i][j]==2), isNull(__data[i+1][j])  && isNull(__data[i][j + 1]))createAngle(180);
//					
//					if ( (__data[i][j]==1 || __data[i][j]==2), isNull(__data[i][j-1])  && isNull(__data[i+1][j]))createAngle(-90);
					
//					function createAngle(rotation:int):void
//					{
//						__angle = new frame2_GUI();
//						addChild(__angle);
//						__angle.x = INTERVAL*j; __angle.y = INTERVAL*i;
//						__angle.rotation = rotation;
//					}
//					function createBorder(rotation:int):void
//					{
//						__border = new frame_GUI();
//						addChild(__border);
//						__border.x = INTERVAL*j; __border.y = INTERVAL*i;
//						__border.rotation = rotation;
//					}
					function createCell():void
					{
						__cell = new Cell_GUI();
						__cell.alpha = .2;
						addChild(__cell);
						__cell.x = INTERVAL*j; __cell.y = INTERVAL*i;
					}	
				}		
			}
		}
		
		private function isNull(data:*):Boolean
		{
			if(data == null) return true;
			else return false;
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}