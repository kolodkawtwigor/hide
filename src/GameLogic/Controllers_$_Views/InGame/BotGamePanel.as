package GameLogic.Controllers_$_Views.InGame
{
	
	import Events.GameModelEvents;
	
	import GraphicsArchitecture.ButtonsElements.GameScreenButtons.ClearBoard_btn;
	import GraphicsArchitecture.ButtonsElements.GameScreenButtons.OpenFirstLetter_btn;
	import GraphicsArchitecture.ButtonsElements.GameScreenButtons.OpenWordBtn;
	
	import starling.display.Sprite;
	import starling.events.Event;

	public class BotGamePanel extends Sprite
	{
		private var openWord_btn:OpenWordBtn;
		private var clearBoard_btn:ClearBoard_btn;
		private var openFirstLetter_btn:OpenFirstLetter_btn;
		public function BotGamePanel()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			createButtons();
			onResize();
			stage.addEventListener(Event.RESIZE, onResize);
		}
		
		private function createButtons():void
		{
			openWord_btn = new OpenWordBtn(openWord);
			addChild(openWord_btn);
			
			clearBoard_btn = new ClearBoard_btn(clearBoard);
			addChild(clearBoard_btn);
			
			openFirstLetter_btn = new OpenFirstLetter_btn(openLetter);
			addChild(openFirstLetter_btn);
		}
		
		private function openLetter():void
		{
			dispatchEvent(new Event(GameModelEvents.OPEN_BONUS_FIRST_LETTER));
		}
		
		private function clearBoard():void
		{
//			dispatchEvent(new Event(GameModelEvents.CLEAR_BOARD));
		}
		
		private function openWord():void
		{
			dispatchEvent(new Event(GameModelEvents.OPEN_WORD));
		}
		
		private function onResize(e:Event = null):void
		{
			this.alignPivot();
			this.x = 220;
			this.y = (InitializationStarling._stage.stageHeight>>1) + 80 ;
			
			openWord_btn.y -= 165; 
			clearBoard_btn.y += 000;
			openFirstLetter_btn.y = 165;
		}
		
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}