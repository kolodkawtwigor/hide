package GameLogic.Controllers_$_Views.InGame
{
	import Connect.ServerConnect;
	
	import GameLogic.GameScreen;
	import GameLogic.MapScreen;
	import GameLogic.GameHelpingService.ScreenManager;
	
	import GraphicsArchitecture.ButtonsElements.GameScreenButtons.ToMapBtn;
	import GraphicsArchitecture.ButtonsElements.SystemButtons.AddCoin;
	import GraphicsArchitecture.ButtonsElements.SystemButtons.HelpBtn;
	import GraphicsArchitecture.ButtonsElements.SystemButtons.SoundBtn;
	
	import starling.display.Sprite;
	import starling.events.Event;

	public class TopGamePanel extends Sprite
	{
		private var toMap_btn:ToMapBtn;
		private var sound_btn_on:SoundBtn;
		private var sound_btn_off:SoundBtn;
		private var addCoin_btn:AddCoin;
		private var help_btn:HelpBtn;
		public function TopGamePanel()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			createButtons();
			checkForSound();
			onResize();
		}
		
		private function createButtons():void
		{
			addCoin_btn = new AddCoin(openBankShop);
			addChild(addCoin_btn); 
			
			sound_btn_on = new SoundBtn(controlSound, true);
			addChild(sound_btn_on); sound_btn_on.alignPivot();
			
			sound_btn_off = new SoundBtn(controlSound,false);
			addChild(sound_btn_off); sound_btn_off.alignPivot();
			
			help_btn = new HelpBtn(openHelpWindow);
			addChild(help_btn);  
			
			toMap_btn = new ToMapBtn(toMap);
			addChild(toMap_btn);  
		}
		
		
		
		private function onResize(e:Event = null):void
		{
			addCoin_btn.x = 160;
			addCoin_btn.y = 93;
			
			sound_btn_on.x = 205;
			sound_btn_on.y = 45;
			
			sound_btn_off.x = 205;
			sound_btn_off.y = 45;
			
			help_btn.x = sound_btn_off.x - 70;
			help_btn.y = sound_btn_off.y;
			
			toMap_btn.x = sound_btn_off.x - 140;
			toMap_btn.y = sound_btn_off.y;
		}
		
		private function openHelpWindow():void
		{
//			WindowsManager.show( RuleWindow);
		}
		
		private function controlSound():void
		{
			if (!ServerConnect.SOUND)
			{
				active();
				//				SoundGameManager.playMusic(SoundGameManager.MAP_SCREEN_SOUND, 1);
//				SoundGameManager.mute(SoundGameManager.FX, false);
//				SoundGameManager.mute(SoundGameManager.MUSIC, false);
				ServerConnect.SOUND = true;
//				ServiceHandler.instance.music(this, true);
			}
			else
			{
				deactive();
//				SoundGameManager.mute(SoundGameManager.FX, true);
//				SoundGameManager.mute(SoundGameManager.MUSIC, true);
				ServerConnect.SOUND = false;
//				ServiceHandler.instance.music(this, false);
			}
		}
		
		private function openBankShop():void
		{
			//			WindowsManager.show( CoinBank);
		}
		
		private function checkForSound():void
		{
			if(ServerConnect.SOUND)
			{
				sound_btn_on.visible = true;
				sound_btn_off.visible = false;
			}
			else
			{
				sound_btn_on.visible = false;
				sound_btn_off.visible = true;
			}
		}
		
		private function active():void
		{
			sound_btn_on.visible = true;
			sound_btn_off.visible = false;
		}
		
		private function deactive():void
		{
			sound_btn_on.visible = false;
			sound_btn_off.visible = true;
		}
		
		
		
		
		private function toMap():void
		{
			ScreenManager.instance.show(MapScreen);
//			WindowsManager.show( OutGame );
			//			ServiceHandler.instance.endLevel(this, showloseWindow, GameModel.INSTANCE._currentQuestionBlock.id, false);
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}