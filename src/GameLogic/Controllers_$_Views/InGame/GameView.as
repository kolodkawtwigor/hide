package GameLogic.Controllers_$_Views.InGame
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import Events.GameModelEvents;
	
	import GameLogic.GameHelpingService.MTextField;
	import GameLogic.Models.GameModel;
	
	import GraphicsArchitecture.MImage;
	
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.geom.Polygon;
	import starling.text.TextField;

	public class GameView extends Sprite
	{
		static private var _init:GameView;
		private var _gameModel:GameModel;
		private var grid_data:Vector.<Vector.<Piece>>;
		static public function instance():GameView { return _init; }
		static	public  const ROW:int = 12;
		static	public  const COL:int = 12;
		private var boardActive:Boolean = true;
		private var _botPanel:BotGamePanel;
		private var _background:MImage;
		private var _backgroundGameSubstrate:MImage;
		private var _clockSubstrate:MImage;
		private var _pictureSubstrate:MImage;
		private var _coinSubstrate:MImage;
		private var _gameMainContainer:Sprite = new Sprite();
		
		public function GameView(botPanel:BotGamePanel)
		{
			_gameModel = GameModel.INSTANCE;
			_botPanel = botPanel;
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			_init = this;
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			grid_data = _gameModel.grid_data;

			addBoardToStage();
			
			addListeners();
			onResize();
			StatusStage.active();
			
			
//			console();
		}
		
		private function addBoardToStage():void
		{
			var gridPiece:Piece;
			for (var i:int = 0; i < grid_data.length; i++) 
			{
				for (var j:int = 0; j < grid_data[i].length; j++) 
				{
					gridPiece = grid_data[i][j];
					addChild(gridPiece);
					gridPiece.x  = gridPiece.pos_grid.x * (gridPiece.width);
					gridPiece.y  = gridPiece.pos_grid.y * (gridPiece.height);
				}
			}
		}
		
		private function addListeners():void
		{
			stage.addEventListener(Event.RESIZE, onResize);
			_gameModel.addEventListener(GameModelEvents.CLEAR_BOARD,deactiveGameBoard);
			addEventListener(TouchEvent.TOUCH , onGameKeyClick);
			_botPanel.addEventListener(GameModelEvents.OPEN_WORD, openWord);
			_botPanel.addEventListener(GameModelEvents.CLEAR_BOARD, clearBoardForBonus);
			_botPanel.addEventListener(GameModelEvents.OPEN_BONUS_FIRST_LETTER, openBonusFirstLetter);
		}
		
		private function openBonusFirstLetter():void
		{
			_gameModel.openBonusFirstLetter();
		}
		
		private function clearBoardForBonus():void
		{
			_gameModel.clearBoardForBonus();
		}
		
		private function openWord():void
		{
			_gameModel.activateBonusRightWord();
		}
		
		public function activeBoardInGame():void
		{
			boardActive = true;
			addEventListener(TouchEvent.TOUCH , onGameKeyClickInClick);
		}
		
		public  function deactiveGameBoard(e:Event = null):void
		{
			removeEventListener(TouchEvent.TOUCH , onGameKeyClickInClick);
			boardActive = false;
			_gameModel.updateBoard();
			_gameModel.clearActiveWordsContainer();
		}
		
		private function onGameKeyClickInClick(e:TouchEvent):void
		{
			if(!boardActive) return;
			
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			var mousePoint:Point;
			var inPolygon:Boolean;
			var globalPoint:Point;
			var localPoint:Point;
			
			for each (var touch:Touch in touches) 
			{
				globalPoint = new  Point(touches[0].globalX,touches[0].globalY);
				localPoint = new Point();
				localPoint = globalToLocal(globalPoint);
				_gameModel.checkPiece( localPoint )
			}
		}
		
		protected function onGameKeyClick(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			
			var rect:Rectangle = this.bounds;
			var polygon:Polygon = new Polygon();
			var topRightX:int = rect.topLeft.x + rect.width
			var topRight:Point = new Point(topRightX , rect.topLeft.y);
			var bottomLeftX:int = rect.bottomRight.x - rect.width
			var bottomLeft:Point = new Point(bottomLeftX , rect.bottomRight.y);
			polygon.addVertices(rect.topLeft, topRight, rect.bottomRight, bottomLeft);
			
			var mousePoint:Point;
			var inPolygon:Boolean;
			var globalPoint:Point;
			var localPoint:Point;
			
			if( touches.length == 0) deactiveGameBoard();
			for each (var touch:Touch in touches) 
			{
				 mousePoint = new Point ( touches[0].globalX,touches[0].globalY ) 
				 inPolygon = polygon.containsPoint(mousePoint);
				if(!inPolygon) deactiveGameBoard();
				if (touch.phase == TouchPhase.BEGAN) activeBoardInGame();
				if (touch.phase == TouchPhase.ENDED) 
				{
					_gameModel.checkRightWords();
					deactiveGameBoard();
				}
			}
		}
		
		private function onResize(e:Event = null):void
		{
			this.alignPivot();
			this.x = (MainApplication.instance.stage.stageWidth>>1) + 45;
			this.y = (MainApplication.instance.stage.stageHeight>>1)+ 20;
		}
		
		private function console():void
		{
			var data:String = "";
			for (var i:int = 0; i < ROW; i++) 
			{
				for (var j:int = 0; j < COL; j++) 
				{
					if(grid_data[j][i] != null) 
					{
						data += grid_data[j][i].active +  ';' + grid_data[j][i].y  + " ";
					}
					else data += ". ";
					
					//					if(grid_data[j][i] != null) data += grid_data[j][i].pos_grid.toString() + " ";
					//					else data += ". ";
				}
				data += "\n";
			}
			trace(data);
		}
		
		private function destroy(e:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);
			removeEventListener(TouchEvent.TOUCH , onGameKeyClick);
		}
		
		
	}
}