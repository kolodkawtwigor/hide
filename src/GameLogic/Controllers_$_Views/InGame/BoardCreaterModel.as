package GameLogic.Controllers_$_Views.InGame
{
	
	import flash.geom.Point;
	
	import starling.events.Event;
	import starling.events.EventDispatcher;
	
	public class BoardCreaterModel extends EventDispatcher
	{
		public var __data:Array;
		public var grid_dataMax:Vector.<Vector.<Piece>>;
		public var grid_data:Vector.<Vector.<Piece>>;
		
		static	public  const ROW:int = 10;
		static	public  const COL:int = 15;
		
		public  var playerMove:Boolean = true;
		
		public function BoardCreaterModel()
		{
			
		}
		
		public function initModel():void
		{
			createBoardMain();
		}
		
		public function createBoardMain():void
		{
			__data = new Array(	  
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,1,1,1,1,1,1,1,1,1,1,0],
				[0,0,0,0,1,1,1,1,1,1,1,1,1,0],
				[0,0,0,0,1,1,1,1,1,1,1,1,1,0],
				[0,0,0,0,1,1,1,1,1,1,1,1,1,0],
				[0,0,0,0,1,1,1,1,1,1,1,1,1,0],
				[0,0,0,0,1,1,1,1,1,1,1,1,1,0],
				[0,0,0,0,0,0,1,1,1,1,1,1,1,0],
				[0,0,0,0,0,0,1,1,1,1,1,1,1,0],
				[0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
			
			toNull();
			toPiece();
		}
		
		public function toPiece():void
		{
			grid_dataMax = new Vector.<Vector.<Piece>>();
			grid_data = new Vector.<Vector.<Piece>>();
			
			for (var i3:int = 0; i3 < COL; i3++) 
			{
				grid_data.push( new Vector.<Piece>());
			}
			
			
			for (var i:int = 0; i < __data.length; i++) 
			{
				grid_dataMax.push( new Vector.<Piece>());
				
				
				
				//				grid_data.push( new Vector.<Piece>());
				
				for (var j:int = 0; j < __data[i].length; j++)
				{
					var piece:Piece;
					var pieceStone:PieceStone;
					
					if(__data[i][j]==null)
					{
						pieceStone = new PieceStone();
						pieceStone.pos_grid = new Point(j,i);
						pieceStone.type = 10;
						grid_dataMax[i].push( pieceStone );
					}	
					else 
					{
						piece = new Piece(registrationCklick, this);
						piece.pos_grid = new Point(j,i);
						if ( __data[i][j] == 2 ) 
						{
							//							piece.withSubstrate = true;
						}
						grid_dataMax[i].push( piece );
					} 
				}
			}
			
			for (var k:int = 0; k < grid_dataMax.length; k++) for (var i2:int = 0; i2 < grid_dataMax[k].length; i2++)  grid_data[i2][k] = grid_dataMax[k][i2]
		}
		
		public function registrationCklick(piece:Piece):void
		{
			//			 dispatchEvent(new Event(GamePlayEvent.CLICK_TO_PIECE , true, piece));
			if(piece.type == 2 ) dispatchEvent(new Event(GamePlayEvent.CLICK_TO_PIECE , true, piece));
			else dispatchEvent(new Event(GamePlayEvent.CLICK_TO_ENEMY_PIECE , true, piece));
		}		
		
		private function toNull():void
		{
			for (var i:int = 0; i < __data.length; i++)  for(var j:int = 0; j < __data[i].length; j++)   if(__data[i][j] == 0)__data[i][j] = null;
		}
	}
}