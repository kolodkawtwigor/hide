package GameLogic.Controllers_$_Views.InGame
{
	import flash.geom.Point;
	
	import GameLogic.GameHelpingService.MTextField;
	import GameLogic.Models.GameModel;
	
	import GraphicsArchitecture.MImage;
	
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.filters.BlurFilter;

	public class Piece extends Sprite
	{
		private var __pos_grid:Point;
		private var __text:MTextField;
		private var __rect:MImage;
		private var __active:Boolean;
		private var __rightLetter:Boolean;
		private var __workWord:Boolean;
		private var __bonusFirstLetter:Boolean;
		
		public function Piece()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			createPiece();
		}
		
		private function createPiece():void
		{
			createSimpleGraphics();
			
			addEventListener(TouchEvent.TOUCH , onGameKeyClick);
		}
		
		private function createSimpleGraphics():void
		{
			__rect = new MImage(MAssetsManager.assetManager.getTexture("letter_substrate"));
			addChild(__rect);
			__rect.width = 44;
			__rect.height = 44;
			
			if( name != null && name.length != 0 )
			{
				 text = new MTextField(40, 40, name , MAssetsManager.CAMPUS, 32, 0xFFFFFF, true);
				addChild(text);text.x = width>>1; text.y = height>>1;text.alignPivot();
				text.filter = BlurFilter.createDropShadow(2,0.785,0x0,.8,1,1.5);
			}
		}
		
		protected function onGameKeyClick(e:TouchEvent):void
		{
			if(rightLetter && !bonusFirstLetter) return;
			
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			if( touches.length == 0) 
			{
				if(!bonusFirstLetter) text.color = 0xFFFFFF
				rect.alpha = 1;
			}
			for each (var touch:Touch in touches) 
			{
				if (touch.phase == TouchPhase.ENDED)
				{
					if(!bonusFirstLetter) text.color = 0xFFFFFF
					rect.alpha = 1;
				}
				
				if (touch.phase == TouchPhase.BEGAN)
				{
					active = true;
					if(!bonusFirstLetter) text.color = 0xFF0000;
					rect.alpha = 1; 
					GameModel.INSTANCE.activeWordsContainer.push(this);
				}
				
				if (touch.phase == TouchPhase.HOVER)
				{
					if(!bonusFirstLetter)text.color = 0xFFFFFF;
					rect.alpha = .2 
				}
				if (touch.phase == TouchPhase.MOVED)
				{
//					trace(name)
				}
			}
		}
		
		public function get pos_grid():Point
		{
			return __pos_grid;
		}
		
		public function set pos_grid(value:Point):void
		{
			__pos_grid = value;
		}
		
		public function get text():MTextField
		{
			return __text;
		}
		
		public function set text(value:MTextField):void
		{
			
			__text = value;
		}
		
		public function get rect():MImage
		{
			return __rect;
		}
		
		public function set rect(value:MImage):void
		{
			__rect = value;
		}
		
		public function get active():Boolean
		{
			return __active;
		}
		
		public function set active(value:Boolean):void
		{
			__active = value;
		}
		public function get rightLetter():Boolean
		{
			return __rightLetter;
		}
		
		public function set rightLetter(value:Boolean):void
		{
			__rightLetter = value;
		}
		
		public function get workWord():Boolean
		{
			return __workWord;
		}
		
		public function set workWord(value:Boolean):void
		{
			__workWord = value;
		}
		
		public function get bonusFirstLetter():Boolean
		{
			return __bonusFirstLetter;
		}
		
		public function set bonusFirstLetter(value:Boolean):void
		{
			__bonusFirstLetter = value;
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
			removeEventListener(TouchEvent.TOUCH , onGameKeyClick);
		}
	}
}