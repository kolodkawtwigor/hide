package GameLogic.Controllers_$_Views.InGame
{
	
	import GraphicsArchitecture.MImage;
	
	import starling.display.Sprite;
	import starling.events.Event;

	public class GameElements extends Sprite
	{
		private var _background:MImage;
		private var _backgroundGameSubstrate:MImage;
		private var _clockSubstrate:MImage;
		private var _pictureSubstrate:MImage;
		private var _coinSubstrate:MImage;
		public function GameElements()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			createGameElementsSubstrate();
			onResize();
		}
		
		private function createGameElementsSubstrate():void
		{
			_background = new MImage(MAssetsManager.assetManager.getTexture("bg_game_screen"));
			addChild(_background); _background.pivotX = 0; _background.pivotY = 0; 
			
			_backgroundGameSubstrate = new MImage(MAssetsManager.assetManager.getTexture("main_bg_letter_substrate"));
			addChild(_backgroundGameSubstrate);_backgroundGameSubstrate.alignPivot();
			
			_clockSubstrate = new MImage(MAssetsManager.assetManager.getTexture("clock_substrate"));
			addChild(_clockSubstrate);_clockSubstrate.alignPivot();
			
			_pictureSubstrate = new MImage(MAssetsManager.assetManager.getTexture("picture_substrate"));
			addChild(_pictureSubstrate);_pictureSubstrate.alignPivot();
			
			_coinSubstrate = new MImage(MAssetsManager.assetManager.getTexture("coin_substrate"));
			addChild(_coinSubstrate);_coinSubstrate.alignPivot();
		}
		
		private function onResize(e:Event = null):void
		{
			_background.y = -1;
			
			_backgroundGameSubstrate.x = (MainApplication.instance.stage.stageWidth>>1) + 45;
			_backgroundGameSubstrate.y = (MainApplication.instance.stage.stageHeight>>1) + 20;
			
			_clockSubstrate.x = _backgroundGameSubstrate.x;
			_clockSubstrate.y = _backgroundGameSubstrate.y - (_backgroundGameSubstrate.height>>1) - (_clockSubstrate.height>>1) + 3;
			
			_pictureSubstrate.x = MainApplication.instance.stage.stageWidth - 95;
			_pictureSubstrate.y = (MainApplication.instance.stage.stageHeight>>1) + 20;
			
			_coinSubstrate.x = 130;
			_coinSubstrate.y = 120;
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}