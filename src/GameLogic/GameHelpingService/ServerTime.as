package GameLogic.GameHelpingService
{
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import Events.TimeEvents;


	public class ServerTime extends EventDispatcher
	{
		static public var TIME:int;
		static public var _timer:Timer;
		
		static private var __init:ServerTime
		
		static public function get instance():ServerTime
		{
			if(!__init) __init = new ServerTime();
			return __init;
		}
		
		public function ServerTime()
		{
		}
		
		public function set_time(value:int):void
		{
			TIME = value;
			_timer = new Timer(1000);
			_timer.addEventListener(TimerEvent.TIMER, onTimer);
			_timer.start();
		}
		
		public function onTimer(event:TimerEvent):void
		{
			TIME ++;
			dispatchEvent(new Event( TimeEvents.SERVER_TIME ));
		}
		
		
	}
}