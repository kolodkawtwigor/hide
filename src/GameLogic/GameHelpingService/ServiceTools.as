
package GameLogic.GameHelpingService
{
	import Connect.service;
	
	import ServiceGame.ServiceModels.BonusLevelModel;
	import ServiceGame.ServiceModels.GameDataItemsModel;
	import ServiceGame.ServiceModels.LevelModel;
	import ServiceGame.ServiceModels.Levels;
	import ServiceGame.ServiceModels.PlayerBonusLevels;
	import ServiceGame.ServiceModels.PlayerModel;
	import ServiceGame.ServiceModels.PurchaseShopModel;
	import ServiceGame.ServiceModels.PurchasesBankModel;
	
	public class ServiceTools
	{
		public function ServiceTools()
		{
		}
		//левел по айди
		static public function levelById(id:String):LevelModel
		{
			for each (var i:LevelModel in service().levelsModelContainer) 
			{
				if(i.id == id) return i;
			}
			
			throw new Error("Такого уровня не существет");
			return null;
		}
		//левел по айди
		static public function helpById(id:String):Levels
		{
			for each (var i:Levels in service().playerModel.levels) 
			{
				if(i.id == id) return i;
			}
			//			throw new Error("Такого уровня не существет");
			return null;
		}
		
		//1 покупку по типу
		static public function purchaseModelById(purchaseType:String):PurchasesBankModel
		{	
			for each (var purchaseBankModel:PurchasesBankModel in service().purchaseBankModel) 
			{
				if (purchaseBankModel.id == purchaseType) 
				{
					if(purchaseBankModel.id == purchaseType) return purchaseBankModel;
				}
			}
			
			throw new Error("Такого типа покупок не существует");
			return null;
		}
		
		static public function itemById(purchaseType:String):GameDataItemsModel
		{	
			for each (var item:GameDataItemsModel in service().gameDataItems) 
			{
				if (item.id == purchaseType) 
				{
					return item;
				}
			}
			
			throw new Error("Такого типа покупок не существует");
			return null;
		}
		
		
		
		//все покупики по типу
		static public function purchaseModelsStorageByPurchaseType(purchaseConstant:String):Vector.<PurchasesBankModel>
		{
			var purchaseModelStorage:Vector.<PurchasesBankModel> = new Vector.<PurchasesBankModel>();
			for each (var purchaseBankModel:PurchasesBankModel in service().purchaseBankModel) 
			{
				if (purchaseBankModel.typeId == purchaseConstant) 
				{
					purchaseModelStorage.push(purchaseBankModel);
				}
			}
			return purchaseModelStorage;
			
			throw new Error("Такого типа покупок не существует");
			return null;
		}
		
		public static function itemShopById(id:String):PurchaseShopModel
		{
			for each (var item:PurchaseShopModel in service().purchasesShopModel) 
			{
				if (item.itemId == id) 
				{
					return item;
				}
			}
			
			throw new Error("Такого типа покупок не существует");
			return null;
		}
		
		public static function itemShopByPriceSoft(soft:int):PurchaseShopModel
		{
			for each (var item:PurchaseShopModel in service().purchasesShopModel) 
			{
				if (item.priceSoft == String(soft) ) 
				{
					return item;
				}
			}
			
			throw new Error("Такого типа покупок не существует");
			return null;
		}
		
		public static function checkBonusLvlTake(lvl:BonusLevelModel):Boolean
		{
			var obj = service().playerModel.playerBonusLevels
			for each (var item:PlayerBonusLevels in service().playerModel.playerBonusLevels) 
			{
				if (item.id == lvl.id) 
				{
					if(!item.completed)return true;
					else return false;
				}
			}
			throw new Error("Такого типа покупок не существует");
			return false;
		}
		
		public static function createCurLvlPogress(id:String):Array
		{
			var len:int = service().playerModel.levels.length
			var playerModel:PlayerModel = service().playerModel;
			for (var i:int = 0; i < len; i++) 
			{
				if(playerModel.levels[i].id == id) 
				{
					return playerModel.levels[i].progress
				}
			}
			return null;
		}
		
		public static function createCurLvl(id:String):LevelModel
		{
			var obj:Vector.<LevelModel> = service().levelsModelContainer;
			var len:int = obj.length;
			
			for (var i:int = 0; i < len; i++) 
			{
				if(obj[i].id == id)
				{
					return obj[i];
				}
			}
			
			return null;
		}
	}
}