package GameLogic.GameHelpingService
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import Events.TimeEvents;
	
	import ServiceGame.Service;
	
	import starling.events.Event;
	import starling.events.EventDispatcher;

	public class LifeModel extends EventDispatcher
	{
		private var __respawnTimer:Timer;
		private var __period:int;
		private var __timeLeft:int;
		private var __countRespawn:int;
		
		static private var __init:LifeModel
		
		public function get timeLeft():int
		{
			return __timeLeft;
		}

		static public function get instance():LifeModel
		{
			if(!__init) __init = new LifeModel(new singletone);
			return __init;
		}
		
		public function LifeModel(s:singletone)
		{
			__period = Service.instance.lifeModel.resp_period;
			__timeLeft = __period - (ServerTime.TIME -  Service.instance.playerModel.lifeTime);
			if (__timeLeft < -1000) __timeLeft = __period
			__countRespawn = Service.instance.lifeModel.resp_count;
			if(__timeLeft > __period) __timeLeft = __period;
			
			__respawnTimer = new Timer( 1000 );
		}
		
		public function start():void
		{
			__respawnTimer.addEventListener(TimerEvent.TIMER, addEnergy);
			__respawnTimer.start();
		}
		
//		public function update(o:Object):void
//		{
//			Service.instance.playerModel.lifeTime = o.time.sec;		
//			__timeLeft = __period - ( ServerTime.TIME -  Service.instance.playerModel.lifeTime);
//			if(__timeLeft > __period) __timeLeft = __period;
//		}
		
		protected function addEnergy(event:TimerEvent):void
		{
			if(Service.instance.playerModel.lifeCount >= Service.instance.lifeModel.max_count)  return;
		
			__timeLeft--;

			if(__timeLeft <= 0 )
			{
				__timeLeft = __period;
				Service.instance.playerModel.lifeCount += __countRespawn; 	
//				trace("My energy ", Service.instance.playerModel.lifeCount);
			}
//			trace(__timeLeft);
			dispatchEvent( new Event( TimeEvents.UPDATE_LIFE_TIME) );
		}
	}
}

class singletone{}