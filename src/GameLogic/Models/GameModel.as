package GameLogic.Models
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import Events.GameModelEvents;
	
	import GameLogic.Controllers_$_Views.InGame.Piece;
	
	import starling.events.Event;
	import starling.events.EventDispatcher;
	import starling.geom.Polygon;

	public class GameModel extends EventDispatcher
	{
		static public var INSTANCE:GameModel;
		static	public  const ROW:int = 12;
		static	public  const COL:int = 12;
		public var grid_data:Vector.<Vector.<Piece>>;
		private var grid_dataMax:Vector.<Vector.<Piece>>;
		private var letterContainer:Vector.<String> = new Vector.<String>();
		private var workWordsContainer:Vector.<String> = new Vector.<String>();
		private var workWordsPieceContainer:Vector.<Vector.<Piece>> = new Vector.<Vector.<Piece>>();
		
		public var activeWordsContainer:Vector.<Piece> = new Vector.<Piece>();
		private var previousPiece:Piece = new Piece();
		
		public function GameModel()
		{
			createLetterContainer();
			
			createWorkWordContainer();
			createGameBoard();
			
			createBoardWithWorkWords();
			createRestPieceName();
			
			initModel();
//			console();
		}
		
		private function createRestPieceName():void
		{
			var letterArray:Vector.<String>;
			var piece:Piece;
			newArray();
			namedRestPieces();
			function namedRestPieces():void
			{
				for (var i:int = 0; i < grid_data.length; i++) 
				{
					for (var j:int = 0; j < grid_data[i].length; j++) 
					{
						piece = grid_data[i][j];
						if(piece.name == null) piece.name = createName();
					}
				}
			}	
			
			function createName():String
			{
				if(letterArray.length == 0) newArray();
				var rand:int = Math.floor(Math.random()*letterArray.length);
				if(rand == letterArray.length) rand -= 1;
				var letter:String = letterArray[rand]
				letterArray.splice(rand,1);
				return letter;
			}	
			
			function newArray():void
			{
				letterArray = new Vector.<String>();
				for (var i:int = 0; i < letterContainer.length; i++) 
				{
					letterArray.push(letterContainer[i])
				}
			}
		}
		
		private function createWorkWordContainer():void
		{
			workWordsContainer.push("OOOO","КККК","ШШШШ","PPPP");
		}
		
		private function createBoardWithWorkWords():void
		{
			for (var i:int = 0; i < workWordsContainer.length; i++) 
			{
				createPath(workWordsContainer[i]);
			}
			namedWords();
			
			function namedWords():void
			{
				for (var j:int = 0; j < workWordsPieceContainer.length; j++) 
				{
					for (var k:int = 0; k < workWordsPieceContainer[j].length; k++) 
					{
						workWordsPieceContainer[j][k].workWord = true;
					}
				}
			}
			
			function createPath(word:String):void
			{
				var wordDone:Boolean = checkForWordPath(word);
				if(wordDone == false ) createPath(word);
			}	
		}		
		
		private function initModel():void
		{
			INSTANCE = this;
		}
		
		private function createGameBoard():void
		{
			grid_dataMax = new Vector.<Vector.<Piece>>();
			grid_data = new Vector.<Vector.<Piece>>();
			var letterContainerForWorking:Vector.<String> = new Vector.<String>();
			
			createBoardPiece();
			saveGridData();
			var piece:Piece;
			function createBoardPiece():void
			{
				for (var i:int = 0; i < ROW; i++) 
				{
					grid_data.push( new Vector.<Piece>());
					grid_dataMax.push( new Vector.<Piece>());
					
					for (var j:int = 0; j < COL; j++)
					{
						piece = new Piece();
						piece.pos_grid = new Point(j,i);
						grid_dataMax[i].push( piece );
					}
				}
			}
			
			function createNewLetterContainerForWork():void
			{
				for (var k:int = 0; k < letterContainer.length; k++) 
				{
					letterContainerForWorking.push(letterContainer[k])
				}
			}
			function saveGridData():void
			{
				for (var k:int = 0; k < grid_dataMax.length; k++) 
				{
					for (var i2:int = 0; i2 < grid_dataMax[k].length; i2++)   
					{
						grid_data[i2][k] = grid_dataMax[k][i2]
					}
				}
			}
		}
		
		private function createLetterContainer():void
		{
			letterContainer.push("А","Б","В","Г","Д","Е","Ё","Ж","З","И","К","Л","М",
				"Н","О","П","Р","С","Т","У","Ф","Х","Ц","Ч"
				,"Ш","Щ","Ь","Ы","Ъ","Э","Ю","Я")
		}
		
		private function checkForWordPath(word:String):Boolean
		{
			var letterLen:int = word.length ;
			var startPathPoint:Point = new Point();
			var path:int;
			var wordContainer:Vector.<Piece>;
			var mainX:int;
			var mainY:int;
			startPathPoint.x = Math.round(Math.random()*ROW);
			startPathPoint.y = Math.round(Math.random()*COL);
			path = Math.round(Math.random()*6);
			
			if(startPathPoint.x  == ROW) startPathPoint.x  -= 1;
			if(startPathPoint.y == COL)  startPathPoint.y -= 1;
			if(path == 6)				 path -= 1;
			mainX = startPathPoint.x;
			mainY = startPathPoint.y;
			
			if(grid_data[mainX][mainY].name != null ) return false;
			var x:int;
			var y:int;

			switch(path)
			{
				case 0:
				{
					for (var i:int = 1; i < letterLen; i++) 
					{
						 y = mainY - i;
						if( y <= 0 || y >= COL || grid_data[mainX][y].name != null ) return false;
					}
					grid_data[mainX][mainY].name = word.slice(0,1);
					
					wordContainer = new Vector.<Piece>();
					wordContainer.push(grid_data[mainX][mainY]);
					
					for (var i:int = 1,  k:int = 1; i < letterLen; i++,k++) 
					{
						grid_data[mainX][mainY - k].name = word.slice(i,i+1);
						wordContainer.push(grid_data[mainX][mainY - k]);
					}
					workWordsPieceContainer.push(wordContainer);
					return true;
					break;
				}
				case 1:
				{
					for (var i:int = 1,  k:int = 1; i < letterLen; i++,k++) 
					{
						 x = mainX + i;
						 y = mainY - k;
						if(  x >= ROW ||  y < 0 || grid_data[x][y].name != null) return false;
					}
					
					grid_data[mainX][mainY].name = word.slice(0,1);
					wordContainer = new Vector.<Piece>();
					wordContainer.push(grid_data[mainX][mainY]);
					for (var i:int = 1,  k:int = 1; i < letterLen; i++,k++) 
					{
						x = mainX + i;
						y = mainY - k;
						grid_data[x][y].name = word.slice(i,i+1);
						wordContainer.push(grid_data[x][y]);
					}
					workWordsPieceContainer.push(wordContainer);
					return true;
					break;
				}
				case 2:
				{
					for (var i:int = 1; i < letterLen; i++) 
					{
						 x = startPathPoint.x + i;
						if(  x >= ROW || grid_data[x][mainY].name != null) return false;
					}
					grid_data[mainX][mainY].name = word.slice(0,1);
					wordContainer = new Vector.<Piece>();
					wordContainer.push(grid_data[mainX][mainY]);
					for (var i:int = 1,  k:int = 1; i < letterLen; i++,k++) 
					{
						grid_data[mainX + i][mainY].name = word.slice(i,i+1);
						wordContainer.push(grid_data[mainX + i][mainY]);
					}
					workWordsPieceContainer.push(wordContainer);
					return true;
					break;
				}
				case 3:
				{
					for (var i:int = 1,  k:int = 1; i < letterLen; i++,k++) 
					{
						 x = mainX + i;
						 y = mainY + k;
						if(  x >= ROW ||  y >= COL || grid_data[x][y].name != null) return false;
					}
					grid_data[mainX][mainY].name = word.slice(0,1);
					wordContainer = new Vector.<Piece>();
					wordContainer.push(grid_data[mainX][mainY]);
					for (var i:int = 1,  k:int = 1; i < letterLen; i++,k++) 
					{
						 x = mainX + i;
						 y = mainY + k;
						grid_data[x][y].name = word.slice(i,i+1);
						wordContainer.push(grid_data[x][y]);
					}
					workWordsPieceContainer.push(wordContainer);
					return true;
					break;
				}
				case 4:
				{
					
					for (var i:int = 1; i < letterLen; i++) 
					{
						 y = mainY + i
						if( y >= COL || grid_data[mainX][y].name != null) return false;
					}
					grid_data[mainX][mainY].name = word.slice(0,1);
					wordContainer = new Vector.<Piece>();
					wordContainer.push(grid_data[mainX][mainY]);
					for (var i:int = 1,  k:int = 1; i < letterLen; i++,k++) 
					{
						grid_data[mainX][mainY + k].name = word.slice(i,i+1);
						wordContainer.push(grid_data[mainX][mainY + k]);
					}
					workWordsPieceContainer.push(wordContainer);
					return true;
					break;
				}
				case 5:
				{
					for (var i:int = 1,  k:int = 1; i < letterLen; i++,k++) 
					{
						x = mainX - i;
						y = mainY + k;
						if(  x < 0 ||  y >= COL || grid_data[x][y].name != null) return false;
					}
					
					grid_data[mainX][mainY].name = word.slice(0,1);
					wordContainer = new Vector.<Piece>();
					wordContainer.push(grid_data[mainX][mainY]);
					for (var i:int = 1,  k:int = 1; i < letterLen; i++,k++) 
					{
						x = mainX - i;
						y = mainY + k;
						grid_data[x][y].name = word.slice(i,i+1);
						wordContainer.push(grid_data[x][y]);
					}
					workWordsPieceContainer.push(wordContainer);
					return true;
					break;
				}	
				default:
				{
					break;
				}
			}
			return false;
		}
		
		public function updateBoard():void
		{
			var piece:Piece;
			for (var i:int = 0; i < grid_data.length; i++) 
			{
				for (var j:int = 0; j < grid_data[i].length; j++) 
				{
					piece = grid_data[i][j];
					piece.rect.alpha = 1;
					piece.active = false;
					if(!piece.rightLetter && !piece.bonusFirstLetter) 
					{
						piece.text.color = 0xFFFFFF;
					}
				}
			}
		}
		
		public function checkPiece(checkPoint:Point):void
		{
			if(activeWordsContainer.length == 0) return;
			var firstPiece:Piece = activeWordsContainer[0];
			var x:int = Math.floor( checkPoint.x / 44);
			var y:int = Math.floor( checkPoint.y / 44);
			if( x == 12)x -= 1;
			if( y == 12)y -= 1;
			var secondPiece:Piece = grid_data[x][y];
			
			if(previousPiece.pos_grid == secondPiece.pos_grid )  return;
			
			previousPiece = secondPiece;
			
			if(previousPiece.bonusFirstLetter && activeWordsContainer[0].pos_grid == previousPiece.pos_grid) return;
			
			if( secondPiece.rightLetter )
			{
				dispatchEvent(new Event(GameModelEvents.CLEAR_BOARD));
				return;
			}
			
			activeWordsContainer[0].rect.alpha = .2;
			
			if(firstPiece.pos_grid.x == secondPiece.pos_grid.x )
			{
				pushToPathByX();
				convertContainer();
				return;
			}
			if(firstPiece.pos_grid.y == secondPiece.pos_grid.y )
			{
				pushToPathByY();
				convertContainer();
				return;
			}
			
			var xLine:uint = Math.abs( firstPiece.pos_grid.x - secondPiece.pos_grid.x);
			var yLine:uint = Math.abs( firstPiece.pos_grid.y - secondPiece.pos_grid.y);
			if(xLine  == yLine)
			{
				pushToPathByXY();
				convertContainer();
				return;
			}
			
			function pushToPathByXY():void
			{
				var x:int;
				var y:int;
				var piece:Piece;
				
				updateBoardInGame();
				
				var lenX:int = firstPiece.pos_grid.x - secondPiece.pos_grid.x;
				var len2Model:int = Math.abs(firstPiece.pos_grid.x - secondPiece.pos_grid.x);
				var lenY:int = firstPiece.pos_grid.y - secondPiece.pos_grid.y;
					
				for (var i:int = 0;  len2Model != 0; i++,len2Model--) 
				{
					x = firstPiece.pos_grid.x - lenX;
					y = firstPiece.pos_grid.y - lenY
					
					piece = grid_data[x][y];
					
					if(piece.rightLetter) 
					{
						dispatchEvent(new Event(GameModelEvents.CLEAR_BOARD));
						return;
					}
					
					piece.text.color = 0xFF0000;
					piece.active = true;
					activeWordsContainer.push(piece);
					
					if(lenX > 0 && lenY > 0) 
					{
						lenX--;
						lenY--;
					}
					else if(lenX < 0 && lenY < 0)
					{
						lenX++;
						lenY++;
					}
					else if(lenX < 0 && lenY > 0)
					{
						lenX++;
						lenY--;
					}
					else if(lenX > 0 && lenY < 0)
					{
						lenX--;
						lenY++;
					}
				}
			}
			
			function pushToPathByY():void
			{
				var x:int;
				var piece:Piece;
				
				updateBoardInGame();
				
				var len:int = firstPiece.pos_grid.x - secondPiece.pos_grid.x;
				var len2:int = Math.abs(firstPiece.pos_grid.x - secondPiece.pos_grid.x)
				
				for (var i:int = 0;  len2 != 0; i++,len2--) 
				{
					x = firstPiece.pos_grid.x - len;
					piece = grid_data[x][firstPiece.pos_grid.y];
					
					if(piece.rightLetter) 
					{
						dispatchEvent(new Event(GameModelEvents.CLEAR_BOARD));
						return;
					}
					
					piece.text.color = 0xFF0000;
					piece.active = true;
					
					activeWordsContainer.push(piece);
					if(len > 0) len--;
					else len++;
				}
			}
			
			function convertContainer():void
			{
				if(activeWordsContainer.length < 1) return;
				
				var len:int = activeWordsContainer.length -1;
				for (var i:int = len; i > 0; i--) 
				{
					activeWordsContainer.push( activeWordsContainer[i]);
				}
				activeWordsContainer.splice(1,len);
			}
			
			function pushToPathByX():void
			{
				var y:int;
				var piece:Piece;
				
				updateBoardInGame();
				
				var len:int = firstPiece.pos_grid.y - secondPiece.pos_grid.y;
				var len2:int = Math.abs(firstPiece.pos_grid.y - secondPiece.pos_grid.y)
				
				for (var i:int = 0;  len2 != 0; i++,len2--) 
				{
					y = firstPiece.pos_grid.y - len;
					piece = grid_data[firstPiece.pos_grid.x][y];
					
					if(piece.rightLetter) 
					{
						dispatchEvent(new Event(GameModelEvents.CLEAR_BOARD));
						return;
					}
					
					piece.text.color = 0xFF0000;
					piece.active = true;
					activeWordsContainer.push(piece);
					
					if(len > 0) len--;
					else len++;
				}
			}
		}
		
		private function updateBoardInGame():void
		{
			if( activeWordsContainer.length > 1 ) activeWordsContainer = activeWordsContainer.splice(0, 1);
			
			var piece:Piece;
			for (var i:int = 0; i < grid_data.length; i++) 
			{
				round :for (var j:int = 0; j < grid_data[i].length; j++) 
				{
					piece = grid_data[i][j];
					if( activeWordsContainer[0] != piece) 
					{
						piece.rect.alpha = 1;
						piece.active = false;
						if(!piece.rightLetter) piece.text.color = 0xFFFFFF;
					}
				}
			}
		}
		 
		public function clearActiveWordsContainer():void
		{
			activeWordsContainer = new Vector.<Piece>();
		}
		
		public function checkRightWords():void
		{
			if(activeWordsContainer.length == 0 ) return;
			
//			var string:String = new String();
//			for (var k:int = 0; k < activeWordsContainer.length; k++) 
//			{
//				string += activeWordsContainer[k].name
//			}
//			trace(string)
			var rightWord:Boolean;
			for (var i:int = 0; i < workWordsPieceContainer.length; i++) 
			{
				rightWord = true;
				if(activeWordsContainer.length > workWordsPieceContainer[i].length) rightWord = false;
				circle2 : for (var j:int = 0; j < workWordsPieceContainer[i].length ; j++) 
				{
					if(workWordsPieceContainer[i][j].pos_grid != activeWordsContainer[j].pos_grid )
					{
						rightWord = false;
						break circle2;
					}
				}
				if(rightWord) activateRightWord();
			}
		}
		
		private function activateRightWord():void
		{
			var wordsName:String = new String();
			var activeWord:String = new String();
			
			for (var b:int = 0; b < activeWordsContainer.length; b++) 
			{
				activeWord += activeWordsContainer[b].name;
			}
			
			circle :for (var j:int = 0; j < workWordsPieceContainer.length; j++) 
			{
				for (var k:int = 0; k < workWordsPieceContainer[j].length; k++) 
				{
					wordsName += workWordsPieceContainer[j][k].name;
				}
				if(activeWord == wordsName)
				{
					workWordsPieceContainer.splice(j,1);
					break circle;
				}
				else wordsName = new String();
			}
			
			for (var i:int = 0; i < activeWordsContainer.length; i++) 
			{
				activeWordsContainer[i].rightLetter = true;
				activeWordsContainer[i].text.color = 0x00FF00;
				activeWordsContainer[i].active = false;
			}
		}
		
		public function activateBonusRightWord():void
		{
			var res:Vector.<Vector.<Piece>> = workWordsPieceContainer;
			if(res.length == 0 ) return;
			for (var i:int = 0; i < res[0].length; i++) 
			{
				res[0][i].rightLetter = true;
				res[0][i].text.color = 0x00FF00;
			}
				workWordsPieceContainer.splice(0,1);
		}
		
		public function clearBoardForBonus():void
		{
			var piece:Piece;
			for (var i:int = 0; i < grid_data.length; i++) 
			{
				for (var j:int = 0; j < grid_data[i].length; j++) 
				{
					if(!grid_data[i][j].workWord)
					{
						piece = grid_data[i][j];
						piece.text.alpha = 0;
						piece.touchable = false;
					}
				}
			}
		}
		
		public function openBonusFirstLetter():void
		{
			var res:Vector.<Vector.<Piece>> = workWordsPieceContainer;
			if(res.length == 0 ) return;
			var piece:Piece;
			
			for (var i:int = 0; i < res.length; i++) 
			{
				piece = res[i][0];
				if(!piece.rightLetter )
				{
					piece.rightLetter = true;
					piece.text.color = 0x00FF00;
					piece.bonusFirstLetter = true;
					return;
				}
			}
		}
		
		private function console():void
		{
			var data:String = "";
			for (var i:int = 0; i < ROW; i++) 
			{
				for (var j:int = 0; j < COL; j++) 
				{
					if(grid_data[j][i] != null) 
					{
						data += grid_data[j][i].x + " ";
					}
					else data += ". ";
//					if(grid_data[j][i] != null) data += grid_data[j][i].pos_grid.toString() + " ";
//					else data += ". ";
				}
				data += "\n";
			}
			trace(data);
		}
	}
}