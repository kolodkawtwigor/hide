package Windows
{
	import flash.text.TextFormatAlign;
	
	import GraphicsArchitecture.MImage;
	import GraphicsArchitecture.ButtonsElements.SystemButtons.CloseWindowButton;
	import GraphicsArchitecture.ButtonsElements.SystemButtons.NextMoveButton;
	import GraphicsArchitecture.ButtonsElements.SystemButtons.NextMoveButton_2;
	
	import Supporting.MTextField;
	import Supporting.WindowsManager;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.HAlign;
	
	public class RuleWindow extends Sprite
	{
		public function RuleWindow()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			addImg();
			addBtn();
			
			StatusStage.active();
			stage.addEventListener(Event.RESIZE , resize)
		}
		
		private function resize(e:Event = null):void
		{
			this.x = InitializationStarling._stage.stageWidth >> 1;
			this.y = InitializationStarling._stage.stageHeight >> 1;
		}
		
		private function addImg():void
		{
			var __backing:MImage = new MImage(MAssetsManager.assetManager.getTexture("help_window"));
			addChild(__backing);__backing.alignPivot();
			
			
		}
		
		private function addBtn():void
		{
			var btn_closeWindow:CloseWindowButton = new CloseWindowButton(closeWindow);
			addChild(btn_closeWindow);  btn_closeWindow.x = (this.width>>1) -10; btn_closeWindow.y = -180; 
			
			var coinBank:NextMoveButton = new NextMoveButton(closeWindow);
			addChild(coinBank);  coinBank.alignPivot();
			coinBank.x = 0; coinBank.y = 300;
		}
		
		private function closeWindow():void{WindowsManager.hide(); }
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}