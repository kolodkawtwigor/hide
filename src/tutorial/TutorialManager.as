package tutorial
{
	import Connect.ServerConnect;
	import Connect.ServiceHandler;
	
	import Events.GameModelEvents;
	
	import GameLogic.MapScreen;
	import GameLogic.Controllers_$_Views.InGame.AnswerInstance;
	
	import starling.events.Event;
	import starling.events.EventDispatcher;
	
	
	public class TutorialManager extends EventDispatcher
	{
		static public function get instance():TutorialManager
		{
			if(!__init) __init = new TutorialManager();
			return __init;
		}
		static private var __init:TutorialManager
		
		
		static private var LOCATION_MAP:String = "LOCATION_MAP";
		static private var GAME_SCREEN_MAIN_WORD:String = "GAME_SCREEN_MAIN_WORD";
		static private var GAME_SCREEN_BONUS_PANEL:String = "GAME_SCREEN_BONUS_PANEL";
		private var GAME_SCREEN_END_STEP:String = "GAME_SCREEN_END_STEP";
		static private var END_HELPING:String = "END_HELPING";
		static private var WINDOW_END:String = "WINDOW_END";
		
		
		private var __currentStep:String = LOCATION_MAP;
		private var stepObject:Object;
		
		public var tutorialStepObject:TutorialStep;
		
		public function TutorialManager()
		{
			addEventListener(GameModelEvents.TUTORIAL_STEP, tutorialStep);
		}
		
		public function tutorialStepClose():void
		{
			if( tutorialStepObject ) 
			{
				MainApplication.instance.removeChild(tutorialStepObject);
			}
		}
		
		private function tutorialStep(e:Event):void
		{
			if( e.data != null)
			{
				__currentStep =  e.data as String
			}
			tutorialStepClose();
			
			createTutorialStep();
			changeStep();
		}		
		
		private function createTutorialStep():void
		{
			stepObject = TutorialStepInformation.stepObject(__currentStep);
			
			if(stepObject)
			{
				tutorialStepObject = new TutorialStep(stepObject);
				MainApplication.instance.addChild(tutorialStepObject);
			}
		}
		
		private function changeStep():void
		{
			switch(__currentStep)
			{
				case LOCATION_MAP:
				{
					__currentStep = GAME_SCREEN_MAIN_WORD;
					break;
				}
				case GAME_SCREEN_MAIN_WORD:
				{
					ServerConnect.TUTORIAL_STEP = 2;
					ServiceHandler.instance.tutorialSave(this, null, 2);
					__currentStep = GAME_SCREEN_BONUS_PANEL;
					break;
				}
				case GAME_SCREEN_BONUS_PANEL:
				{
					__currentStep = GAME_SCREEN_END_STEP;
					break;
				}
				case GAME_SCREEN_END_STEP:
				{
					__currentStep = END_HELPING;
					break;
				}
				case END_HELPING:
				{
					__currentStep = WINDOW_END;
					tutorialStepClose();
					AnswerInstance.instance().activeBoard();
					break;
				}
					
				case WINDOW_END:
				{
					tutorialStepClose();
//					WindowsManager.show( EndTutorial );
					MapScreen.instance().tutorialStart = false;
					ServerConnect.TUTORIAL_STEP = false;
					ServiceHandler.instance.tutorialSave(this, null, false);
					AnswerInstance.instance().activeBoard();
					break;
				}
					
				default:
				{
					break;
				}
			}
		}
	}
}