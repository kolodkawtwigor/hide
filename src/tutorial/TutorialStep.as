package tutorial
{
	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Point;
	
	import Events.GameModelEvents;
	
	import GameLogic.Controllers_$_Views.InGame.AnswerInstance;
	
	import GraphicsArchitecture.MImage;
	import GraphicsArchitecture.ButtonsElements.SystemButtons.NextMoveButton;
	
	import Supporting.InvertedMask;
	import Supporting.MTextField;
	
	import starling.display.Canvas;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.deg2rad;
	
	public class TutorialStep extends Sprite
	{
		//background
		private var _background:flash.display.Sprite;
		private var _backgroundColor:uint = 0x000000;
		private var _backgroundAlpha:Number;
		private var _backgroundVisibility:Boolean;
		
		//Ellipse
		private var _ellipseFigure:String = "ellipse";
		private var _ellipse:flash.display.Sprite;
		private var _ellipseColor:uint = 0xFF0000;
		private var _ellipseRadius:Number;
		private var _ellipseX:Number;
		private var _ellipseY:Number;
		private var _ellipseWidth:Number;
		private var _ellipseHeight:Number;
		
		//rectengle
		private var _rectengleFigure:String = "rectengle";
		
		private var _rectangle:flash.display.Sprite;
		private var _rectangle_2:flash.display.Sprite;
		private var _rectangleColor:uint = 0xFF0000;
		private var _rectengleSize:Point;
		private var _rectangleWidth:Number;
		private var _rectangleHeight:Number;
		private var _rectangleWidth_2:Number;
		private var _rectangleHeight_2:Number;
		
		//mask
//		private var _invertedMask:InvertedMask;
		private var _globalPoint:Point;
		private var _globalPoint_2:Point;
		private var _distance:Number;
		private var _location:Point;
		
		private var _figureName:String;
		private var _figure:flash.display.Sprite;
		private var _figure_2:flash.display.Sprite;
		private var _figurePosition:Point;
		
//		private var _starlingImage:StarlingImage;
		private var _canvas:Canvas;
		//текст
		private var _messageText:String;
		private var _messageButton:Boolean;
		private var _messagePosition:Point;
		private var _step:int;
		private var _voodooPosition:Point;
		
		private var _messageObject:Object;
		private var _invertedMask:InvertedMask;
		private var _invertedMask_2:InvertedMask;
		private var _starlingImage:Image;
		private var _starlingImage_2:Image;
		private var _touchOnlyButton:Boolean;
		private var publish:NextMoveButton;
		private var _video:Boolean;
		private var _mouse_ITM:MImage;
		private var _maskForm:String;
		private var _strelaPoint:Point;
		private var _strelaGradus:Number;
		private var _touchMask:Boolean;
		private var _messageSubstrateScaleWidth:Number;
		private var _messageSubstrateScaleHeight:Number;
		private var bitmapData:BitmapData;
		private var bitmapImage:Image;
//		private var blurFilter:BlurFilter;
		
		public function TutorialStep(stepObject:Object)
		{
			_globalPoint = stepObject["globalPoint"]
		
			_voodooPosition = stepObject["voodooPosition"];
			_messageText = stepObject["message"];
			_messageButton = stepObject["button"];
			_touchOnlyButton = stepObject["touchOnlyButton"];
			_backgroundVisibility = stepObject["backgroundVisibility"];
			_video = stepObject["video"];
			_step = stepObject["step"];
			_backgroundAlpha = stepObject["backgroundAlpha"];
			_strelaPoint = stepObject["strelaPoint"];
			_strelaGradus = stepObject["strelaGradus"];
			_touchMask = stepObject["touchMask"];
			
			_messageSubstrateScaleWidth = stepObject["messageSubstrateScaleWidth"];
			_messageSubstrateScaleHeight = stepObject["messageSubstrateScaleHeight"];
			
			_maskForm = stepObject["maskForm"];
			switch(_maskForm)
			{
				case "rectangle":
				{
					_rectangleWidth = stepObject["rectangleWidth"];
					_rectangleHeight = stepObject["rectangleHeight"];
					break;
				}
				case "ellipse":
				{
					_ellipseWidth = stepObject["ellipseWidth"];
					_ellipseHeight = stepObject["ellipseHeight"];
					break;
				}
				case "rectangle_2":
				{
					_rectangleWidth = stepObject["rectangleWidth"];
					_rectangleHeight = stepObject["rectangleHeight"];
					_globalPoint_2 = stepObject["globalPoint_2"]
					_rectangleWidth_2 = stepObject["rectangleWidth_2"];
					_rectangleHeight_2 = stepObject["rectangleHeight_2"];
					break;
				}
					
				default:
				{
					break;
				}
			}
			
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			addListeners();
			
			 createSimpleTutorial();
		}
		
		private function createSimpleTutorial():void
		{
			drawAssets();
			drawVodoo();
			tuning();
		}
		
		private function tuning():void
		{
			if(_touchOnlyButton) 
			{
				AnswerInstance.instance().deActiveBoard();
			}
			if(_touchMask)
			{
				MainApplication.instance.stage.addEventListener(TouchEvent.TOUCH, onTouchMask);
			}
		}
		
		
		
		private function drawVodoo():void
		{
			if(_strelaPoint)
			{
				var strela:MImage = new MImage(MAssetsManager.assetManager.getTexture("btn_pageArrow"));
				addChild(strela);strela.alignPivot(); strela.x = _strelaPoint.x; strela.y = _strelaPoint.y;
				strela.rotation = deg2rad(_strelaGradus);
				
				if(_strelaGradus == 0 )TweenMax.to(strela, .6 ,{ x:(strela.x - 40) ,repeat:4000, yoyo:true}) ;
				else TweenMax.to(strela, .6 ,{ y:(strela.y + 40) ,repeat:4000, yoyo:true}) ;
			}
			
			if( _messageButton )
			{
				var voodoo:MImage = new MImage(MAssetsManager.assetManager.getTexture("tutorial_substrate"));
				addChild(voodoo);  voodoo.x = _voodooPosition.x; voodoo.y = _voodooPosition.y;
				voodoo.alignPivot();
				voodoo.scaleX = _messageSubstrateScaleWidth;
				voodoo.scaleY = _messageSubstrateScaleHeight;
				
				var voodooText:MTextField  = new MTextField(700, 130, _messageText, MAssetsManager.CANDARA_BOLD, 20, 0x633B12)
				addChild(voodooText);  voodooText.alignPivot();  
				voodooText.x =  _voodooPosition.x; voodooText.y = _voodooPosition.y;
				
				publish = new NextMoveButton(closeWindow);
				addChild(publish);  publish.x = voodoo.x ; publish.y = voodoo.y + (voodoo.height >>1) ;
				publish.scaleX = .7;
				publish.scaleY = .7;
			}
		}
		
		private function closeWindow():void
		{
			TutorialManager.instance.dispatchEvent(new Event(GameModelEvents.TUTORIAL_STEP));
		}
		
		private function drawAssets():void
		{
			drawBackGround();
			drawFigure();
			
			
			if(_maskForm == "rectangle_2")
			{
				drawDoubleMask();
			}
			else
			{
				drawMask();
			}
		}
		
		private function drawDoubleMask():void
		{
			_invertedMask = new InvertedMask(_background, _figure, _globalPoint) ;
			var invertedBitmap:BitmapData = new BitmapData(_background.width, _background.height, true, 0x00);
			invertedBitmap.draw(_invertedMask.inverted);
			_starlingImage = new Image(Texture.fromBitmapData(invertedBitmap)); 
			addChild(_starlingImage); 
			
			_invertedMask_2 = new InvertedMask(_background, _figure_2, _globalPoint_2) ;
			var invertedBitmap_2:BitmapData = new BitmapData(_background.width, _background.height, true, 0x00);
			invertedBitmap_2.draw(_invertedMask_2.inverted);
			_starlingImage_2 = new Image(Texture.fromBitmapData(invertedBitmap_2)); 
			addChild(_starlingImage_2); _starlingImage_2.y = MainApplication.instance.stage.stageHeight>>1;
		}
		
		private function drawMask():void
		{
			_invertedMask = new InvertedMask(_background, _figure, _globalPoint) ;
			var invertedBitmap:BitmapData = new BitmapData(_background.width, _background.height, true, 0x00);

			invertedBitmap.draw(_invertedMask.inverted);
			_starlingImage = new Image(Texture.fromBitmapData(invertedBitmap)); 
			addChild(_starlingImage);
//			
//			var blurFilter:BlurFilter = new BlurFilter(8, 8, 1);
//			_starlingImage.filter = blurFilter;
			
		}
		
		private function drawFigure():void
		{
			if(_maskForm == "ellipse") createEllipse();
			else createRectangle();
			
			function createRectangle():void
			{
				_rectangle = new flash.display.Sprite();
				_rectangle.graphics.beginFill(_ellipseColor);
				_rectangle.graphics.drawRect(0, 0, _rectangleWidth, _rectangleHeight);
				_figure = _rectangle;
				
				if(_maskForm == "rectangle_2")
				{
					_rectangle_2 = new flash.display.Sprite();
					_rectangle_2.graphics.beginFill(_ellipseColor);
					_rectangle_2.graphics.drawRect(0, 0, _rectangleWidth_2, _rectangleHeight_2);
					_figure_2 = _rectangle_2;
				}
			}
			
			function createEllipse():void
			{
				_ellipse = new flash.display.Sprite();
				_ellipse.graphics.beginFill(_ellipseColor);
				_ellipse.graphics.drawEllipse(0, 0, _ellipseWidth, _ellipseHeight);
				
				var btm:Bitmap = new Bitmap();
				_ellipse.addChild(btm);
				btm.width = btm.height = _ellipseWidth;
				_figure = _ellipse;
			}
		}		
		 
		
		private function drawBackGround():void
		{
			if(_maskForm == "rectangle_2")
			{
				_background = new flash.display.Sprite();
				_background.graphics.beginFill(_backgroundColor, _backgroundAlpha);
				_background.graphics.drawRect(0, 0, MainApplication.instance.stage.stageWidth, (MainApplication.instance.stage.stageHeight>>1) );
			}
			else
			{
				_background = new flash.display.Sprite();
				_background.graphics.beginFill(_backgroundColor, _backgroundAlpha);
				_background.graphics.drawRect(0, 0, MainApplication.instance.stage.stageWidth, MainApplication.instance.stage.stageHeight);
			}
			
			

//			var blur:BlurFilter = new BlurFilter(); 
//			blur.blurX = 10; 
//			blur.blurY = 10; 
//			blur.quality = BitmapFilterQuality.MEDIUM; 
//			_background.filters = [blur];
//			trace(_background.filters)
		}	
		
		private function onTouch(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			for each (var touch:Touch in touches)
			{
				_location = new Point(_globalPoint.x + _rectangleWidth, _globalPoint.y + _rectangleWidth); 
				_distance = Point.distance( touch.getLocation( this ), _location);
				
				if(_distance > _rectangleWidth) _starlingImage.touchable = true;
				else _starlingImage.touchable = false;
			}
		}
		
		private function onTouchMask(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			for each (var touch:Touch in touches)
			{
				_location = new Point(_globalPoint.x + (_rectangleWidth>>1), _globalPoint.y + (_rectangleWidth>>1)); 
				_distance = Point.distance( touch.getLocation( this ), _location);
				
				if(_distance > (_rectangleWidth>>1)) _starlingImage.touchable = true;
				else _starlingImage.touchable = false;
			}
		}
		
		private function addListeners():void
		{
			if(!_touchOnlyButton) MainApplication.instance.stage.addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
			MainApplication.instance.stage.removeEventListener(TouchEvent.TOUCH, onTouch);
		}
	}
}