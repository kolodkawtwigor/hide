package tutorial
{
	import flash.geom.Point;

	public class TutorialStepInformation
	{
		private static var stepObjectContainer:Object;
		
		public function TutorialStepInformation()
		{
		}
		
		public static function stepObject(currentStep:String):Object
		{
			switch(currentStep)
			{
				case "LOCATION_MAP":
				{
					stepObjectContainer = locationMap();
					break;
				}
				case "GAME_SCREEN_MAIN_WORD":
				{
					stepObjectContainer = gameScreenMainWord();
					break;
				}
				
				case "GAME_SCREEN_BONUS_PANEL":
				{
					stepObjectContainer = gameScreenBonusPanel();
					break;
				}
				case "GAME_SCREEN_END_STEP":
				{
					stepObjectContainer = gameScreenEndStep();
					break;
				}
				case "WINDOW_END":
				{
					 windowEnd();
					break;
				}
					
				default:
				{
					break;
				}
			}
			return stepObjectContainer;
		}
		
		private static function gameScreenEndStep():Object
		{
			var obj:Object = new Object();
			obj["step"] = 4;
			obj["voodooPosition"] = new Point(375, 300);
			
			obj["maskForm"] = "ellipse";
			obj["globalPoint"] = new Point(1, 1);
			obj["ellipseWidth"] = 1;
			obj["ellipseHeight"] = 1;
			
			obj["message"] = " Попробуй угадать слово и впиши его \n в поле с помощью клавиатуры.";
			obj["messageSubstrateScaleWidth"] = .4;
			obj["messageSubstrateScaleHeight"] = .3;
			obj["button"] = true;
			obj["touchOnlyButton"] = true;
			obj["backgroundAlpha"] = 0.8;
			return obj;
		}
		
		private static function windowEnd():void
		{
			// TODO Auto Generated method stub
			
		}		
		
		
		private static function gameScreenBonusPanel():Object
		{
			var obj:Object = new Object();
			obj["step"] = 3;
			obj["maskForm"] = "rectangle";
			obj["globalPoint"] = new Point(25, 210);
			obj["rectangleWidth"] = 710;
			obj["rectangleHeight"] = 240;
			obj["voodooPosition"] = new Point(375, 100);
			obj["message"] = "Используй подсказки, чтоб угадать \n задуманное слово.  Одна картинка - это \n прилагательное  характерное \n для слова-ответа.";
			obj["messageSubstrateScaleWidth"] = .4;
			obj["messageSubstrateScaleHeight"] = .4;
			obj["button"] = true;
			obj["touchOnlyButton"] = true;
			obj["backgroundAlpha"] = 0.8;
			return obj;
		}
		
		
		private static function gameScreenMainWord():Object
		{
			var obj:Object = new Object();
			obj["step"] = 2;
			
			obj["maskForm"] = "rectangle_2";
			obj["globalPoint"] = new Point(140, 100);
			obj["rectangleWidth"] = 480;
			obj["rectangleHeight"] = 120;
			obj["rectangleWidth_2"] = 400;
			obj["rectangleHeight_2"] = 150;
			obj["globalPoint_2"] = new Point(180, 100);
			
			obj["voodooPosition"] = new Point(220, 330);
			obj["message"] = "Надо угадать \n загаданное слово.";
			obj["messageSubstrateScaleWidth"] = .25;
			obj["messageSubstrateScaleHeight"] = .25;
			
			obj["button"] = true;
			obj["touchOnlyButton"] = true;
			obj["backgroundAlpha"] = 0.8;
			obj["strelaPoint"] = new Point(380, 310);;
			obj["strelaGradus"] = 270;
			return obj;
		}
		
		private static function locationMap():Object
		{
			var obj:Object = new Object();
			obj["step"] = 1;
			obj["globalPoint"] = new Point(137, 225);
			obj["maskForm"] = "rectangle";
			obj["rectangleWidth"] = 223;
			obj["rectangleHeight"] = 247;
			obj["backgroundAlpha"] = .8;
			
			obj["strelaPoint"] = new Point(430, 340);;
			obj["strelaGradus"] = 0;
			obj["touchMask"] = true;
			
			return obj;
		}	
	}
}