package Events
{

	public class GameModelEvents
	{
		public static var TIME_LEFT:String = "TIME_LEFT";
		public static var TICK_GAME_TIMER:String = "TICK_GAME_TIMER";
		public static var CORRECT_ANSWER:String = "CORRECT_ANSWER";
		public static var INCORRECT_ANSWER:String = "INCORRECT_ANSWER";
		public static var LOSE:String = "LOSE";
		public static var WIN:String = "WIN";
		public static var UPDATE_CURRENSY_SOFT_PICTURE:String = "UPDATE_CURRENSY_SOFT_PICTURE";
		public static var WALL_UPLOAD_POST:String = "WALL_UPLOAD_POST";
		public static var CLEAR_BOARD:String = "CLEAR_BOARD";
		public static var OPEN_WORD:String = "OPEN_WORD";
		public static var OPEN_BONUS_FIRST_LETTER:String = "OPEN_BONUS_FIRST_LETTER";
		
		public function GameModelEvents()
		{
		}
	}
}