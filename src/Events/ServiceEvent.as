package Events
{
	public class ServiceEvent 
	{
		//all
		static public const APPIINFO_SERVISE							:String = "APPIINFO_SERVISE";
		static public const BONUS_SERVISE								:String = "BONUS_SERVISE";
		static public const FRIENDGIFTS_SERVISE							:String = "FRIENDGIFTS_SERVISE";
		static public const NEWFRIENDS_SERVISE							:String = "NEWFRIENDS_SERVISE";
		static public const LIFE_SERVISE								:String = "LIFE_SERVISE";
		static public const PLAYER_SERVISE								:String = "USER_SERVISE";
		static public const PURCHASE_SERVICE							:String = "PURCHASE_SERVICE";
		static public const LEVELS_SERVICE								:String = "LEVELS_SERVICE";
		static public const FRIENDS_SERVICE								:String = "FRIENDS_SERVICE";
		
		
		static public const ENTRY_COUNT 								:String = "ENTRY_COUNT";
		static public const POINTS										:String = "POINTS";
		
		
		static public const LIFE_COUNT									:String = "LIFE_COUNT";
		static public const CURRENT_LEVEL								:String = "CURRENT_LEVEL";
		static public const CURRENCY_SOFT								:String = "CURRENCY_COUNT";
		static public const LEVEL_COUNT									:String = "LEVEL_COUNT";
		
		public function ServiceEvent()
		{
		}
	}
}