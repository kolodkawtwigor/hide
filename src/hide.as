package
{
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageQuality;
	import flash.display.StageScaleMode;
	
	import Connect.Connection;
	
	import Vars.MainVars;
	import Vars.MainVarsController;
	
	import starling.events.Event;
	
	[SWF(width="1025", height="700", frameRate="60", wmode="direct")]
	public class hide extends Sprite
	{
		static private var _init:hide;
		private const  GAME_NAME:String = "word-in-image";
		static public var flashVars:Object;
		static public var PRELOADER:Preloader;
		static public function instance():hide { return _init; }
		
		public function hide()
		{
			_init = this;
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			stage.quality = StageQuality.BEST;
			
			var paramsters:Object = this.root.loaderInfo.parameters;
			MainVarsController.instance.init(paramsters);
			
			var __gateWay:String = MainVars.instance.mainVars.appUrl;
			
			if(__gateWay == null)
			{
				__gateWay = "http://dev.purpurgames.com/"
			}
			__gateWay += GAME_NAME;
			__gateWay += "/social/index.php?r="
			
			flashVars = LoaderInfo(this.root.loaderInfo).parameters as Object;
			Connection.gateWayPath = __gateWay;
			//			PRELOADER = new Preloader(stage);
			//			addChild(PRELOADER);
			var initializationStarling:InitializationStarling = new InitializationStarling(stage);
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}