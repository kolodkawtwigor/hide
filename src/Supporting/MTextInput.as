package Supporting
{
	import flash.events.TimerEvent;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.utils.Timer;
	
	import Events.GameModelEvents;
	
	import GameLogic.Controllers_$_Views.InGame.Answer;
	import GameLogic.Controllers_$_Views.InGame.GameView;
	import GameLogic.Controllers_$_Views.InGame.InfoText;
	import GameLogic.Models.GameModel;
	
	import GraphicsArchitecture.FontsManager;
	import GraphicsArchitecture.MImage;
	import GraphicsArchitecture.ButtonsElements.GameScreenButtons.AnswerBTN;
	
	import feathers.controls.TextInput;
	import feathers.controls.text.TextFieldTextEditor;
	import feathers.core.ITextEditor;
	import feathers.events.FeathersEventType;
	
	import starling.display.Canvas;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class MTextInput extends Sprite
	{
		private var __findTI:TextInput;
		private var __inputTextFieldHint:Image;
		private var __answer_btn:AnswerBTN;
		private var background:MImage;
		private var correctAnswe:Boolean;
		private var __correct_answer:String;
		private var canvasArray:Vector.<Canvas> = new Vector.<Canvas>();
		private var lvlText:int = 0;
		public function MTextInput(correct_answer:String)
		{
			__correct_answer = correct_answer;
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		public function get findTI():TextInput
		{
			return __findTI;
		}

		public function set findTI(value:TextInput):void
		{
			__findTI = value;
		}

		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			createBackground();
			
			createInputTextField();
			createInputTextFieldHint();
			createButton();
			
			createWordsHelping();
		}
		
		public function createWordsHelping():void
		{
			if(this.getChildByName("canvasContainer" ))
			{
				removeChild(this.getChildByName("canvasContainer" ));
			}
			
			
			
			var sprite:Sprite = new Sprite();
			canvasArray = new Vector.<Canvas>();
			sprite.name = "canvasContainer" 
			for (var i:int = 0; i < __correct_answer.length; i++) 
			{
				var __circ:Canvas = new Canvas();
				__circ.beginFill(0xFFFFFF);
				__circ.drawRectangle(0,0,22,3);
				sprite.addChild(__circ); __circ.alpha = 1;
				__circ.x = i*28  - 100/*- ( (width>>1) - 90)*/;
//				__circ.y = 83;
				canvasArray.push(__circ)
			}
			addChild(sprite);
			sprite.y = 83;
		}
		
		private function createBackground():void
		{
			background = new MImage(MAssetsManager.assetManager.getTexture("substrate_answer"));
			background.alignPivot();	background.y = 70
			addChild(background);
		}
		
		private function createInputTextFieldHint():void
		{
			__inputTextFieldHint = FontsManager.dropShadowTextField("Введите ответ сюда", MAssetsManager.CANDARA_BOLD,40,0xFFFFFF, 0x6D3828,2,7);
			addChild(__inputTextFieldHint); 
			__inputTextFieldHint.alignPivot(); 
		}
		
		private function createButton():void
		{
			__answer_btn = new AnswerBTN(checkForDoneAnswer);
			addChild(__answer_btn); __answer_btn.y = 70; __answer_btn.x = background.width>>1;
		}
		
		private function checkForDoneAnswer():void
		{
//			correctAnswer();
			if(__correct_answer == __findTI.text) correctAnswer();
			else incorrectAnswer();
		}
		
		private function incorrectAnswer():void
		{
			correctAnswe = false;
			timer();
		}
		
		private function correctAnswer():void
		{
			if(GameView.LEVEL_BLOCK <= 9)InfoText.instance.dispatchEvent(new Event(GameModelEvents.CORRECT_ANSWER_ANIMATION));
			
			GameView.LEVEL_BLOCK ++;
			correctAnswe = true;
			timer();
		}
		
		private function timer():void
		{
			var tmr:Timer = new Timer(1000, 1);
			tmr.start();
			tmr.addEventListener(TimerEvent.TIMER_COMPLETE, restart);
		}
		
		protected function restart(event:TimerEvent):void
		{
			Answer.CURRENCY_SOFT_PICTURE = 10;
			
			if (correctAnswe) GameModel.INSTANCE.update();
			else GameModel.INSTANCE.uptadeToStart();
		}
		
		private function createInputTextField():void
		{
			__findTI = new TextInput();
			__findTI.maxChars = __correct_answer.length;
			__findTI.setSize(250,40)
			__findTI.useHandCursor = true;
			
//			var tf:TextField = new TextField(100, 100, "a");
//			addChild(tf);
			
			__findTI.textEditorFactory = function():ITextEditor
			{
				var renderer:TextFieldTextEditor= new TextFieldTextEditor();
				renderer.textFormat = new TextFormat ();
				renderer.textFormat.kerning = true;
				renderer.textFormat.letterSpacing = 10;
				
				renderer.embedFonts = true;
				renderer.textFormat.font = MAssetsManager.CANDARA_BOLD;
				renderer.textFormat.color = 0xFFFFFF;
				renderer.textFormat.bold = true;
				renderer.textFormat.size = 30;
				
				renderer.textFormat.align = TextFormatAlign.LEFT
				return renderer;
			}
			
			addChild( (__findTI as Sprite) );
		
			__findTI.addEventListener( starling.events.Event.CHANGE, enterInput);
			__findTI.y = 50; 
			__findTI.x = -100 ;
		}
		
		private function enterInput(e:Event):void
		{
			var len:uint;
			if(lvlText > __findTI.text.length) 
			{
				len =  __findTI.text.length 
			}
			else 
			{
				len =  __findTI.text.length - 1
			}
			
			lvlText = __findTI.text.length
			
			if(canvasArray[len].visible)   
			{
				canvasArray[len].visible = false;
			}
			else
			{
				canvasArray[len].visible = true;
			}
			
			
			
		}
		
		public function get correct_answer():String
		{
			return __correct_answer;
		}
		
		public function set correct_answer(value:String):void
		{
			__correct_answer = value;
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
			__inputTextFieldHint.texture.dispose();
			__inputTextFieldHint.dispose();
		}
	}
}