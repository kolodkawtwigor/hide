package Supporting
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Point;
	
	public class InvertedMask extends Sprite{
		
		public var inverted:Sprite;
		
		public function InvertedMask(pattern:Sprite, maskTemplate:Sprite, position:Point):void
		{
			createInvertedMask(pattern, maskTemplate, position);
		}
		
		private function createInvertedMask(pattern:Sprite, maskTemplate:Sprite, position:Point):void{
			//create a container for the render of your object with an inverted mask
			inverted = new Sprite();
			inverted.x = pattern.x;
			inverted.y = pattern.y;
			
			// first we have to duplicate the mask we wish to invert as a bitmap
			var dup:BitmapData = new BitmapData(maskTemplate.width, maskTemplate.height, true, 0x00000000);
			dup.draw(maskTemplate); 
			
			var newAlpha:Bitmap = new Bitmap(dup, "auto", true);
			newAlpha.x = position.x;
			newAlpha.y = position.y;
			
			//set the blendmode of the new alpha to erase so that it will erase whatever is behind it in its container movieclip
			newAlpha.blendMode = BlendMode.ERASE;
			
			//add the object you wish to apply the mask to to the inverted sprite
			inverted.addChild(pattern);
			pattern.x = pattern.y = 0;
			
			//add the newAlpha bitmap to the sprite
			inverted.addChild(newAlpha);
			
			//set the inverted sprite's blendmode to layer so that the interior erase blend mode will show through in the container display object
			inverted.blendMode = BlendMode.LAYER;
		}
	}
}