package Supporting
{
	
	import flash.text.TextFormatAlign;
	
	import starling.events.Event;
	import starling.text.TextField;
	
	public class MTextField extends TextField
	{
		private var _width:Number;
		private var _height:Number;
		private var _fontSize:Number;
		private var _text:String;
		private var _posX:Number;
		private var _posY:Number;
		private var _fontName:String;
		private var _touchable:Boolean;
		private var _border:Boolean;
		private var _hAlive:String;
		private var _vAlive:String;
		
		
		public function MTextField(width:Number, height:Number, text:String, fontName:String="Verdana", fontSize:Number=12, color:uint=0, touchable:Boolean = false,border:Boolean = false,hAlive:String = TextFormatAlign.CENTER,vAlive:String = TextFormatAlign.CENTER)
		{
			_width = width;
			_height = height;
			
			_fontSize = fontSize;
			_fontName = fontName;
			_text = text;
			_touchable = touchable;
			_border = border;
			_hAlive = hAlive;
			_vAlive = vAlive;
			if(text.length < 16) batchable = true;
			
			super(_width, _height, _text, fontName, _fontSize, color);
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}

		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			this.hAlign = _hAlive;
			this.vAlign = _vAlive;
//			this.leading = 2*Engine.GlobalSize;
//			this.filter = BlurFilter.createDropShadow();
			this.touchable = _touchable;
			this.border = _border;
		}
		
		private function destroy(e:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);
			this.dispose();
		}
	}
}