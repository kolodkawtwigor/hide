package Supporting
{
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class Mask extends Sprite
	{
		private var __target:*;
		private var __mask:*;
		private var maskedDisplayObject:PixelMask = new PixelMask();
		private var __fixSize:Boolean;
		
		public function Mask(target:*, mask:*, fixSize:Boolean = false)
		{
			__target = target;
			__mask = mask;
			__fixSize = fixSize;
			
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			var sprt:Sprite = new Sprite();	
			
			
			maskedDisplayObject.addChild(__target);
			
			maskedDisplayObject.mask = __mask;
			
			if(__fixSize)
			{
				maskedDisplayObject.maskWidth = __mask.width;
				maskedDisplayObject.maskHeight = __mask.height;
			}
			
			addChild(maskedDisplayObject);
		}
		
		private function destroy(event:Event):void
		{
			for (var i:int = 0; i < numChildren; i++) 
			{
				getChildAt(i).dispose();
			}		
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}