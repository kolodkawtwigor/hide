package Supporting
{
	import flash.media.Sound;
	
	import Connect.ServerConnect;
	
	import ServiceGame.Preloader;
	
	import deng.fzip.FZip;
	import deng.fzip.FZipFile;
	
	import treefortress.sound.SoundAS;
	import treefortress.sound.SoundManager;

	public class SoundGameManager
	{
		static public const MAP_SCREEN_SOUND:String = "MAP_SCREEN_SOUND";
		static public const GAME_SCREEN_SOUND:String = "GAME_SCREEN_SOUND";
		
		static public const BONUS_DONE_LVL:String = "BONUS_DONE_LVL";
		static public const BUY:String = "BUY";
		static public const DELETE_LETTER:String = "DELETE_LETTER";
		static public const ENTER_LETTER:String = "ENTER_LETTER";
		static public const ENTER_WORD_BTN:String = "ENTER_WORD_BTN";
		static public const HINT_OPEN_LETTER:String = "HINT_OPEN_LETTER";
		static public const HINT_open_picture:String = "HINT_open_picture";
		static public const LOSE:String = "LOSE";
		static public const OTHER:String = "OTHER";
		static public const PRAISE_RIGHT_WORD:String = "PRAISE_RIGHT_WORD";
		static public const WIN:String = "WIN";
		static public const WINDOW_OPEN_CLOSE:String = "WINDOW_OPEN_CLOSE";
		
		
		static public var FX:String = "fx";
		static public var MUSIC:String = "music";
		
		static private var music_group:SoundManager;
		static private var fx_group:SoundManager;
		private static var zip:FZip;
		private static var file:FZipFile;
		
		
		public static function loadSoundsMedia():void
		{
//			Preloader.instance().loaderMask = 5;
			var mediaArray = ServerConnect.mediaSoundArray;
			music_group = new SoundManager();
			fx_group = new SoundManager();
			
			var len = mediaArray.length;
			
			for (var i:int = 0; i < len; i++) 
			{
				file = mediaArray[i];
				var sound:Sound = new Sound();
				sound.loadCompressedDataFromByteArray( file.content, file.content.length);
				
				var name:String = file.filename.substring(13, file.filename.length);
				var name1 = name.slice( 0, name.length -4);
				
				
				switch(name1)
				{
					case MAP_SCREEN_SOUND:
					{
						music_group.addSound(name1 , sound);
						break;
					}
					case GAME_SCREEN_SOUND:
					{
						music_group.addSound(name1 , sound);
						break;
					}
						
					default:
					{
						fx_group.addSound(name1 , sound);
						break;
					}
				}
			}
			
		}
		
		protected static function sdsd(file:FZipFile):void
		{
			music_group.loadSound(file.filename , file.filename );
		}
		
		static public function loadSounds():void
		{
			music_group = new SoundManager();		
			fx_group = new SoundManager();
			
//			addMusic("sounds/MAP_SCREEN_SOUND.mp3", MAP_SCREEN_SOUND);
//			addMusic("sounds/GAME_SCREEN_SOUND.mp3", GAME_SCREEN_SOUND);
//			
//			addFx("sounds/BONUS_1_WORD.mp3", BONUS_1_WORD);
//			addFx("sounds/BONUS_3_WORD.mp3", BONUS_3_WORD);
//			addFx("sounds/BONUS_ALL_WORD.mp3", BONUS_ALL_WORD);
//			addFx("sounds/BUY.mp3", BUY);
//			addFx("sounds/CLICK_WORD_1.mp3", CLICK_WORD_1);
//			addFx("sounds/CLICK_WORD_2.mp3", CLICK_WORD_2);
//			addFx("sounds/CLICK_WORD_3.mp3", CLICK_WORD_3);
//			addFx("sounds/CLICK_WORD_4.mp3", CLICK_WORD_4);
//			addFx("sounds/DELETE_LETTER.mp3", DELETE_LETTER);
//			addFx("sounds/GAME_SCREEN_SOUND.mp3", GAME_SCREEN_SOUND);
//			addFx("sounds/GO_TO_LVL.mp3", GO_TO_LVL);
//			addFx("sounds/MAP_SCREEN_SOUND.mp3", MAP_SCREEN_SOUND);
//			addFx("sounds/OTHER.mp3", OTHER);
//			addFx("sounds/WIN.mp3", WIN);
//			addFx("sounds/WINDOW_OPEN_CLOSE.mp3", WINDOW_OPEN_CLOSE);
				
				
				
//			setSoundOnStart();
		}
		
		private static function setSoundOnStart():void
		{
//			mute( MUSIC, mainData.instance.settings.sound[MainDataType.MUSIC_MUTE] );
//			mute( FX, mainData.instance.settings.sound[MainDataType.FX_MUTE] );
		}
		
		// add sound
		static private function addFx(url:String, name:String):void
		{
//			trace(ServerConnect.STATIC_PATH)
			fx_group.loadSound( ServerConnect.STATIC_PATH + url+ "?" + int(Math.random() * int.MAX_VALUE), name );
		}
		
		static private function addMusic(url:String, name:String):void
		{
			music_group.loadSound( ServerConnect.STATIC_PATH + url+ "?" + int(Math.random() * 1000), name );
		}
		
		//mute
		static public function off(type:String):void
		{
			mute(type, true);
		}
		
		static public function on(type:String):void
		{
			mute(type, false);
		}
		
		static public function mute(type:String, mute:Boolean):void
		{
			//if(stanica.DEV_VERSION == 0) mute = true;
			switch(type)
			{
				case FX:
					fx_group.mute = mute;
					break;
				case MUSIC:
					music_group.mute = mute;
					break;
			}
		}
		
		//play
		static public function playMusic(name:String, volume:Number):void
		{
			music_group.playLoop( name, volume );
		}
		
		static public function playFX(name:String, volume:Number):void
		{
			fx_group.playFx( name, volume );
		}
		
		//pause
		static public function stopMusic(name:String):void
		{
			music_group.getSound( name ).stop();
		}
		
		static public function stopFx(name:String):void
		{
			fx_group.getSound( name ).stop();
		}
		
		static public function getFxMute():Boolean
		{
			return fx_group.mute;
		}
		
		static public function getMusicMute():Boolean
		{
			return music_group.mute;
		}
		
		
	}	
}