package Vars 
{
	public class MainVarsModel
	{
		static private var _init:MainVarsModel;
		static public function get instance():MainVarsModel { return _init; }
		
		public var apiconnection:String;
		public var api_server:String;
		public var application_key:String;
		public var appUrl:String;
		public var authorized:String;
		public var auth_sig:String;
		public var clientLog:String;
		public var custom_args:String;
		public var first_start:String;
		public var ip_geo_location:String;
		public var logged_user_id:String;
		public var new_sig:String;
		public var platformType:String;
		public var referrer:String;
		public var session_key:String;
		public var session_secret_key:String;
		public var sig:String;
		public var userAgent:String;
		public var web_server:String;
		
		public function MainVarsModel()
		{
		}
	}
}