package Vars 
{
	public class MainVarsController
	{
		static private var _init:MainVarsController
		
		static public function get instance():MainVarsController
		{
			if(!_init) _init = new MainVarsController();
			return _init;
		}
		
		private var mainVarsModel:MainVarsModel = MainVars.instance.mainVars;
		
		public function init(o:*):void
		{
			mainVarsModel.apiconnection = o.apiconnection;
			mainVarsModel.api_server = o.api_server;
			mainVarsModel.application_key = o.application_key;
			mainVarsModel.appUrl = o.appUrl;
			mainVarsModel.authorized = o.authorized;
			mainVarsModel.auth_sig = o.auth_sig;
			mainVarsModel.clientLog = o.clientLog;
			mainVarsModel.custom_args = o.custom_args;
			mainVarsModel.first_start = o.first_start;
			mainVarsModel.ip_geo_location = o.ip_geo_location;
			mainVarsModel.logged_user_id = o.logged_user_id;
			mainVarsModel.new_sig = o.new_sig;
			mainVarsModel.platformType = o.platformType;
			mainVarsModel.referrer = o.referrer;
			mainVarsModel.session_key = o.session_key;
			mainVarsModel.session_secret_key = o.session_secret_key;
			mainVarsModel.sig = o.sig;
			mainVarsModel.userAgent = o.userAgent;
			mainVarsModel.web_server = o.web_server;
		}
	}
}	














