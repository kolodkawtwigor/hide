package Vars  
{
	public class MainVars
	{
		static private var _init:MainVars
		static public function get instance():MainVars
		{
			if(!_init) _init = new MainVars();
			return _init;
		}
		
		private var _mainVars:MainVarsModel;
		
		public function get mainVars():MainVarsModel
		{
			if(!_mainVars) _mainVars = new MainVarsModel();
			return _mainVars;
		}
	}
}