package
{
	import flash.system.Security;
	
	import Connect.ServerConnect;
	
	import GameLogic.GameScreen;
	
	import Tools.UI.WindowsManager;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class MainApplication extends Sprite
	{
		private var _mAssetsManager:MAssetsManager;
		private var _gameScreen:GameScreen;
		public static var BLOCK:int;
		private var __screenHolst:Sprite = new Sprite();
		static private var _init:MainApplication;
		static public function get instance():MainApplication { return _init; }
		private var __serverConnect:ServerConnect;
		
		public function MainApplication()
		{
			_init = this
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}

		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			WindowsManager.mainSprite = this;
			Security.allowDomain('*');
//			GameInit.instance.start();
			
//			initServer();
			__serverConnect = new ServerConnect(initServer);
			Security.allowDomain('*');
//			
		}
		
		private function initServer():void
		{
			_mAssetsManager = new MAssetsManager(initGame);
		}
		
		private function initGame():void
		{
			GameInit.instance.start();
		}
		
		private function destroy(e:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}